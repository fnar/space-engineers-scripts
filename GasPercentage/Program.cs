﻿using System.Collections.Generic;
using System.Linq;

using Sandbox.ModAPI.Ingame;

namespace IngameScript
{
    partial class Program : MyGridProgram
    {
        public Program()
        {
            Runtime.UpdateFrequency = UpdateFrequency.Update10;
        }

        public void Main(string argument, UpdateType updateSource)
        {
            var tanks = new List<IMyGasTank>();
            GridTerminalSystem.GetBlocksOfType(tanks, b => b.CubeGrid == Me.CubeGrid);

            double TankCurrentStorage = tanks.Sum(t => t.FilledRatio * t.Capacity);
            double TankMaxStorage = tanks.Sum(t => t.Capacity);
            double TankStoragePercentage = ((TankCurrentStorage) / (TankMaxStorage)) * 100.0f;
        }
    }
}
