﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using SpaceEngineers.Game.ModAPI.Ingame;
using VRage.Game.ModAPI.Ingame;
using VRageMath;

namespace IngameScript
{
    partial class Program : MyGridProgram
    {
        // Usage:
        // Battery/Gas/TurretAmmo information will emit to `DisplaySurfaceCubeName` constant
        // To add an AirlockPair, go to `if (RegisterOnce)` block and add your pair
        // To trigger a sound and red-pulsing light, invoke this script using:
        //    - `warningon light:LargeShip Interior Light sound:LargeShip SoundBlock`
        //    - `warningoff sound:LargeShip SoundBlock light:LargeShip Interior Light`
        public const string DisplaySurfaceCubeName = "Large Ship Status Display";

        public Program()
        {
            Runtime.UpdateFrequency = UpdateFrequency.Update1;
        }

        public static bool RegisterOnce = true;

        public void Main(string argument, UpdateType updateSource)
        {
            BatteryManagement.Tick(GridTerminalSystem, "clear", Me);
            GasManagement.Tick(GridTerminalSystem, "", Me);
            TurretAmmoManagement.Tick(GridTerminalSystem, "", Me);
            WarningManager.Tick(GridTerminalSystem, argument, Me);
            AirlockManager.Tick(GridTerminalSystem, "", Me);
            ThrustToWeightManager.Tick(GridTerminalSystem, "clear", Me);

            if (RegisterOnce)
            {
                AirlockManager.Register(GridTerminalSystem, "Large Ship Door (Bottom Interior)", "Large Ship Door (Bottom Exterior)");
                AirlockManager.Register(GridTerminalSystem, "Large Ship Hangar Airlock Inner", "Large Ship Hangar Airlock Outer");
                AirlockManager.Register(GridTerminalSystem, "Large Ship Bridge Airlock Inner", "Large Ship Bridge Airlock Outer");
                RegisterOnce = false;
            }
        }

        public static class BatteryManagement
        {
            #region Constants
            private const string BatteryManagementSurfaceCubeName = DisplaySurfaceCubeName;
            #endregion

            public static float CurrStoredPower = 0.0f;

            private static List<IMyTextSurface> BatteryManagementLogSurfaces = new List<IMyTextSurface>();
            private static List<IMyBatteryBlock> Batteries = new List<IMyBatteryBlock>();

            private static bool IsInitialized = false;
            private static void Init(IMyGridTerminalSystem GridTerminalSystem, IMyProgrammableBlock Me)
            {
                if (!IsInitialized)
                {
                    if (!string.IsNullOrEmpty(BatteryManagementSurfaceCubeName))
                    {
                        List<IMyTerminalBlock> terminalBlocks = new List<IMyTerminalBlock>();
                        GridTerminalSystem.SearchBlocksOfName(BatteryManagementSurfaceCubeName, terminalBlocks, b => b.CubeGrid == Me.CubeGrid);
                        BatteryManagementLogSurfaces = terminalBlocks.Where(b => b is IMyTextSurface).Select(b => (IMyTextSurface)b).ToList();
                    }
                    
                    GridTerminalSystem.GetBlocksOfType(Batteries, b => b.CubeGrid == Me.CubeGrid);
                    IsInitialized = true;
                }
            }

            public static void Tick(IMyGridTerminalSystem GridTerminalSystem, string argument, IMyProgrammableBlock Me)
            {
                Init(GridTerminalSystem, Me);

                if (argument == "clear")
                {
                    BatteryManagementLogSurfaces.ForEach(s => s.WriteText(""));
                }

                if (Batteries.Count > 0)
                {
                    CurrStoredPower = Batteries.Sum(b => b.CurrentStoredPower);
                    float MaxStoredPower = Batteries.Sum(b => b.MaxStoredPower);
                    float PowerPercentage = (CurrStoredPower / MaxStoredPower) * 100.0f;

                    bool AnyCharging = Batteries.Any(b => b.IsCharging);
                    float CurrentInput = Batteries.Sum(b => b.CurrentInput);
                    float CurrentOutput = Batteries.Sum(b => b.CurrentOutput);
                    float NetInput = CurrentInput - CurrentOutput;

                    StringBuilder sb = new StringBuilder();
                    sb.Append($"Battery: {PowerPercentage:N2}%");
                    if (NetInput > 0.0f)
                    {
                        float NetInputPerMinute = NetInput / 60.0f;
                        float RemainingStorage = MaxStoredPower - CurrStoredPower;
                        float MinutesToFullyCharged = RemainingStorage / NetInputPerMinute;

                        if (MinutesToFullyCharged > 0.1f)
                        {
                            sb.Append($" ({MinutesToFullyCharged:N1}m)");
                        }
                    }

                    BatteryManagementLogSurfaces.ForEach(s => s.WriteText($"{sb}\n", true));
                }
            }
        }

        public static class GasManagement
        {
            #region Constants
            private const string GasManagementSurfaceCubeName = DisplaySurfaceCubeName;
            #endregion

            private static List<IMyTextSurface> GasManagementLogSurfaces = new List<IMyTextSurface>();
            private static List<IMyGasTank> OxygenTanks = new List<IMyGasTank>();
            private static List<IMyGasTank> HydrogenTanks = new List<IMyGasTank>();

            private static bool IsInitialized = false;
            private static void Init(IMyGridTerminalSystem GridTerminalSystem, IMyProgrammableBlock Me)
            {
                if (!IsInitialized)
                {
                    if (!string.IsNullOrEmpty(GasManagementSurfaceCubeName))
                    {
                        List<IMyTerminalBlock> terminalBlocks = new List<IMyTerminalBlock>();
                        GridTerminalSystem.SearchBlocksOfName(GasManagementSurfaceCubeName, terminalBlocks, b => b.CubeGrid == Me.CubeGrid);
                        GasManagementLogSurfaces = terminalBlocks.Where(b => b is IMyTextSurface).Select(b => (IMyTextSurface)b).ToList();
                    }

                    var tanks = new List<IMyGasTank>();
                    GridTerminalSystem.GetBlocksOfType(tanks, b => b.CubeGrid == Me.CubeGrid);

                    OxygenTanks = tanks.Where(t => t.BlockDefinition.TypeIdString.ToLower().Contains("oxygen")).ToList();
                    HydrogenTanks = tanks.Where(t => t.BlockDefinition.SubtypeId.ToLower().Contains("hydrogen")).ToList();

                    IsInitialized = true;
                }
            }

            public static void Tick(IMyGridTerminalSystem GridTerminalSystem, string argument, IMyProgrammableBlock Me)
            {
                Init(GridTerminalSystem, Me);

                if (argument == "clear")
                {
                    GasManagementLogSurfaces.ForEach(s => s.WriteText(""));
                }

                if (OxygenTanks.Count > 0 || HydrogenTanks.Count > 0)
                {
                    if (OxygenTanks.Count > 0)
                    {
                        GasManagementLogSurfaces.ForEach(s => s.WriteText($"Oxygen: {GetTankStoragePercentage(OxygenTanks)*100.0:N2}%\n", true));
                    }
                    if (HydrogenTanks.Count > 0)
                    {
                        GasManagementLogSurfaces.ForEach(s => s.WriteText($"Hydrogen: {GetTankStoragePercentage(HydrogenTanks) * 100.0:N2}%\n", true));
                    }
                }
            }

            private static double GetTankStoragePercentage(List<IMyGasTank> tanks)
            {
                double TankCurrentStorage = tanks.Sum(t => t.FilledRatio * t.Capacity);
                double TankMaxStorage = tanks.Sum(t => t.Capacity);
                return TankCurrentStorage / TankMaxStorage;
            }
        }

        public static class TurretAmmoManagement
        {
            #region Constants
            private const string TurretAmmoManagementSurfaceCubeName = DisplaySurfaceCubeName;
            #endregion

            private static List<IMyTextSurface> TurretAmmoManagementLogSurfaces = new List<IMyTextSurface>();
            private static List<IMyCargoContainer> Cargos = new List<IMyCargoContainer>();

            private static bool IsInitialized = false;
            private static void Init(IMyGridTerminalSystem GridTerminalSystem, IMyProgrammableBlock Me)
            {
                if (!IsInitialized)
                {
                    if (!string.IsNullOrEmpty(TurretAmmoManagementSurfaceCubeName))
                    {
                        List<IMyTerminalBlock> terminalBlocks = new List<IMyTerminalBlock>();
                        GridTerminalSystem.SearchBlocksOfName(TurretAmmoManagementSurfaceCubeName, terminalBlocks, b => b.CubeGrid == Me.CubeGrid);
                        TurretAmmoManagementLogSurfaces = terminalBlocks.Where(b => b is IMyTextSurface).Select(b => (IMyTextSurface)b).ToList();
                    }

                    GridTerminalSystem.GetBlocksOfType(Cargos, c => c.CubeGrid == Me.CubeGrid);

                    IsInitialized = true;
                }
            }
            
            public static void Tick(IMyGridTerminalSystem GridTerminalSystem, string argument, IMyProgrammableBlock Me)
            {
                Init(GridTerminalSystem, Me);

                if (argument == "clear")
                {
                    TurretAmmoManagementLogSurfaces.ForEach(s => s.WriteText(""));
                }

                var AmmoTypeToCount = new Dictionary<string, int>();

                var inventories = Cargos.Select(c => c.GetInventory()).ToList();
                foreach (var inventory in inventories)
                {
                    var items = new List<MyInventoryItem>();
                    inventory.GetItems(items);
                    foreach (var item in items)
                    {
                        if (item.Type.SubtypeId.ToLower().Contains("ammo"))
                        {
                            if (!AmmoTypeToCount.ContainsKey(item.Type.SubtypeId))
                            {
                                AmmoTypeToCount.Add(item.Type.SubtypeId, (int)item.Amount);
                            }
                            else
                            {
                                AmmoTypeToCount[item.Type.SubtypeId] += (int)item.Amount;
                            }
                        }
                    }
                }

                if (AmmoTypeToCount.Count > 0)
                {
                    foreach (var kvp in AmmoTypeToCount)
                    {
                        string AmmoType = kvp.Key;
                        if (AmmoType == "MediumCalibreAmmo")
                        {
                            AmmoType = "Turret Ammo";
                        }
                        TurretAmmoManagementLogSurfaces.ForEach(s => s.WriteText($"{AmmoType}: {kvp.Value}\n", true));
                    }
                }
            }
        }

        public static class WarningManager
        {
            private const float Speed = 0.5f;

            struct LightBlockState
            {
                public float Intensity;
                public float Radius;
                public Color Color;
                public bool On;
            }

            private static Dictionary<string, LightBlockState> InitialState = new Dictionary<string, LightBlockState>();
            private static Dictionary<string, IMyLightingBlock> NameToLight = new Dictionary<string, IMyLightingBlock>();
            private static List<IMyLightingBlock> ActiveLights = new List<IMyLightingBlock>();
            private static Dictionary<IMyLightingBlock, bool> ActiveLightToGoingUp = new Dictionary<IMyLightingBlock, bool>();

            private static Dictionary<string, IMySoundBlock> NameToSound = new Dictionary<string, IMySoundBlock>();
            private static List<IMySoundBlock> ActiveSounds = new List<IMySoundBlock>();

            public static void TurnOn(IMyGridTerminalSystem GridTerminalSystem, string LightName = null, string SoundName = null)
            {
                if (!string.IsNullOrEmpty(LightName))
                {
                    if (!NameToLight.ContainsKey(LightName))
                    {
                        var block = GridTerminalSystem.GetBlockWithName(LightName);
                        if (block is IMyLightingBlock)
                        {
                            var lightingBlock = (IMyLightingBlock)block;
                            NameToLight.Add(LightName, lightingBlock);
                            InitialState.Add(LightName, new LightBlockState { Intensity = lightingBlock.Intensity, Radius = lightingBlock.Radius, Color = lightingBlock.Color, On = lightingBlock.Enabled });
                        }
                    }

                    if (NameToLight.ContainsKey(LightName))
                    {
                        NameToLight[LightName].Color = Color.Red;
                        NameToLight[LightName].Intensity = 4.0f;
                        NameToLight[LightName].ApplyAction("OnOff_On");
                        ActiveLightToGoingUp[NameToLight[LightName]] = true;
                        ActiveLights.Add(NameToLight[LightName]);
                    }
                }
                
                if (!string.IsNullOrEmpty(SoundName))
                {
                    if (!NameToSound.ContainsKey(SoundName))
                    {
                        var block = GridTerminalSystem.GetBlockWithName(SoundName);
                        if (block is IMySoundBlock)
                        {
                            var soundBlock = (IMySoundBlock)block;
                            if (soundBlock.IsSoundSelected)
                            {
                                NameToSound.Add(SoundName, soundBlock);
                            }
                        }
                    }

                    if (NameToSound.ContainsKey(SoundName))
                    {
                        NameToSound[SoundName].LoopPeriod = 60.0f * 30.0f;
                        NameToSound[SoundName].Play();
                        ActiveSounds.Add(NameToSound[SoundName]);
                    }
                }
            }

            public static void TurnOff(string LightName = null, string SoundName = null)
            {
                if (!string.IsNullOrEmpty(LightName))
                {
                    IMyLightingBlock light;
                    if (NameToLight.TryGetValue(LightName, out light))
                    {
                        light.Intensity = InitialState[LightName].Intensity;
                        light.Radius = InitialState[LightName].Radius;
                        light.Color = InitialState[LightName].Color;
                        if (!InitialState[LightName].On)
                        {
                            light.ApplyAction("OnOff_Off");
                        }
                        
                        ActiveLights.Remove(light);
                        ActiveLightToGoingUp.Remove(NameToLight[LightName]);
                    }
                }
                
                if (!string.IsNullOrEmpty(SoundName))
                {
                    IMySoundBlock sound;
                    if (NameToSound.TryGetValue(SoundName, out sound))
                    {
                        sound.Stop();
                        ActiveSounds.Remove(sound);
                    }
                }
            }

            public static void Tick(IMyGridTerminalSystem GridTerminalSystem, string argument, IMyProgrammableBlock Me)
            {
                argument = argument.ToLower();

                var arguments = argument.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                if (arguments.Length >= 2)
                {
                    string command = arguments[0];
                    bool TurnOn = command == "warningon";
                    bool TurnOff = command == "warningoff";
                    if (TurnOn || TurnOff)
                    {
                        string light = "";
                        string sound = "";
                        bool parsingLight = false;
                        bool parsingSound = false;
                        foreach (var arg in arguments.Skip(1))
                        {
                            if (arg.StartsWith("light:"))
                            {
                                if (parsingSound)
                                {
                                    parsingSound = false;
                                }

                                parsingLight = true;
                                string strippedArg = arg.Substring(6);
                                light += $"{strippedArg} ";
                            }

                            if (arg.StartsWith("sound:"))
                            {
                                if (parsingLight)
                                {
                                    parsingLight = false;
                                }

                                parsingSound = true;
                                string strippedArg = arg.Substring(6);
                                sound += $"{strippedArg} ";
                            }
                        }

                        light = light.Trim();
                        sound = sound.Trim();

                        if (TurnOn)
                        {
                            WarningManager.TurnOn(GridTerminalSystem, light, sound);
                        }
                        else
                        {
                            WarningManager.TurnOff(light, sound);
                        }
                    }
                }

                foreach (var ActiveLight in ActiveLights)
                {
                    bool GoingUp = ActiveLightToGoingUp[ActiveLight];
                    float NewIntensity = GoingUp ? ActiveLight.Intensity + Speed : ActiveLight.Intensity - Speed;
                    if (NewIntensity > 10.0f) NewIntensity = 10.0f;
                    if (NewIntensity < 0.5f) NewIntensity = 0.5f;

                    float IntensityPercentage = (NewIntensity - 0.5f) / 9.5f;
                    ActiveLight.Intensity = NewIntensity;

                    if (ActiveLight.Intensity <= 0.51f)
                    {
                        ActiveLightToGoingUp[ActiveLight] = true;
                    }

                    if (ActiveLight.Intensity >= 9.9001f)
                    {
                        ActiveLightToGoingUp[ActiveLight] = false;
                    }
                }
            }
        }

        public static class AirlockManager
        {
            class DoorSet
            {
                public IMyDoor DoorOne;
                public IMyDoor DoorTwo;

                public bool DoorOnePendingOpen;
                public bool DoorTwoPendingOpen;

                public DoorStatus DoorOneStatus;
                public DoorStatus DoorTwoStatus;

                public int CloseTickCounter = -1;
            }

            private static List<DoorSet> DoorSets = new List<DoorSet>();

            public static void Register(IMyGridTerminalSystem GridTerminalSystem, string DoorOneName, string DoorTwoName)
            {
                DoorSet DoorSet = new DoorSet
                {
                    DoorOne = GridTerminalSystem.GetBlockWithName(DoorOneName) as IMyDoor,
                    DoorTwo = GridTerminalSystem.GetBlockWithName(DoorTwoName) as IMyDoor,
                };

                if (DoorSet.DoorOne != null && DoorSet.DoorTwo != null)
                {
                    DoorSet.DoorOneStatus = DoorSet.DoorOne.Status;
                    DoorSet.DoorTwoStatus = DoorSet.DoorTwo.Status;

                    DoorSets.Add(DoorSet);
                }
            }

            // Anything lower than this appears to cause airlock failures in general
            private const int NumTicksToWait = 150;

            private static void CloseDoorOne(DoorSet DoorSet)
            {
                if (DoorSet.DoorOne.Status != DoorStatus.Closed && DoorSet.DoorOne.Status != DoorStatus.Closing)
                {
                    DoorSet.DoorOne.CloseDoor();
                }
            }

            private static void CloseDoorTwo(DoorSet DoorSet)
            {
                if (DoorSet.DoorTwo.Status != DoorStatus.Closed && DoorSet.DoorTwo.Status != DoorStatus.Closing)
                {
                    DoorSet.DoorTwo.CloseDoor();
                }
            }

            public static void Tick(IMyGridTerminalSystem GridTerminalSystem, string argument, IMyProgrammableBlock Me)
            {
                for (int i = 0; i < DoorSets.Count; i++)
                {
                    var DoorSet = DoorSets[i];
                    var DoorOne = DoorSet.DoorOne;
                    var DoorTwo = DoorSet.DoorTwo;

                    bool DoorOneChanged = DoorSet.DoorOneStatus != DoorOne.Status;
                    bool DoorTwoChanged = DoorSet.DoorTwoStatus != DoorTwo.Status;

                    bool DoorOneOpen = DoorSet.DoorOneStatus != DoorStatus.Closed;
                    bool DoorTwoOpen = DoorSet.DoorTwoStatus != DoorStatus.Closed;

                    bool DoorOneInFlight = DoorSet.DoorOneStatus == DoorStatus.Opening || DoorSet.DoorOneStatus == DoorStatus.Closing;
                    bool DoorTwoInFlight = DoorSet.DoorTwoStatus == DoorStatus.Opening || DoorSet.DoorTwoStatus == DoorStatus.Closing;

                    if (DoorSet.DoorOnePendingOpen)
                    {
                        // Keep Forcing door two to close
                        if (DoorSet.DoorTwoStatus != DoorStatus.Closed)
                        {
                            CloseDoorTwo(DoorSet);
                        }
                        else if (DoorSet.DoorOneStatus != DoorStatus.Open)
                        {
                            CloseDoorTwo(DoorSet);

                            if (DoorSet.CloseTickCounter == -1)
                            {
                                // We just finished closing, so set the CloseTickCounter
                                DoorSet.CloseTickCounter = NumTicksToWait;
                            }
                            else if (DoorSet.CloseTickCounter == 0)
                            {
                                DoorOne.OpenDoor();
                                DoorSet.CloseTickCounter = -1;
                            }
                            else
                            {
                                DoorSet.CloseTickCounter--;
                            }
                        }
                        else
                        {
                            // DoorOne is open, remove pending open
                            DoorSet.DoorOnePendingOpen = false;
                        }
                    }
                    else if (DoorSet.DoorTwoPendingOpen)
                    {
                        // Keep Forcing door one to close
                        if (DoorSet.DoorOneStatus != DoorStatus.Closed)
                        {
                            CloseDoorOne(DoorSet);
                        }
                        else if (DoorSet.DoorTwoStatus != DoorStatus.Open)
                        {
                            CloseDoorOne(DoorSet);

                            if (DoorSet.CloseTickCounter == -1)
                            {
                                // We just finished closing, so set the CloseTickCounter
                                DoorSet.CloseTickCounter = NumTicksToWait;
                            }
                            else if (DoorSet.CloseTickCounter == 0)
                            {
                                DoorTwo.OpenDoor();
                                DoorSet.CloseTickCounter = -1;
                            }
                            else
                            {
                                DoorSet.CloseTickCounter--;
                            }
                        }
                        else
                        {
                            // DoorOne is open, remove pending open
                            DoorSet.DoorTwoPendingOpen = false;
                        }
                    }
                    else if (DoorOneOpen && DoorTwoOpen)
                    {
                        if (DoorOneInFlight)
                        {
                            CloseDoorOne(DoorSet);
                            DoorSet.DoorOnePendingOpen = true;
                        }
                        else if (DoorTwoInFlight)
                        {
                            CloseDoorTwo(DoorSet);
                            DoorSet.DoorTwoPendingOpen = true;
                        }
                        else
                        {
                            // They're both fully open
                            CloseDoorOne(DoorSet);
                            CloseDoorTwo(DoorSet);
                        }
                    }
                    

                    // Update status
                    DoorSet.DoorOneStatus = DoorOne.Status;
                    DoorSet.DoorTwoStatus = DoorTwo.Status;

                    DoorSets[i] = DoorSet;
                }
            }
        }

        public static class ThrustToWeightManager
        {
            private const string ThrustToWeightDisplayBlockName = "Large Ship ThrustToWeight Display";

            class ThrustValues
            {
                public double Up = 0.0;
                public double Down = 0.0;
                public double Forward = 0.0;
                public double Backward = 0.0;
                public double Left = 0.0;
                public double Right = 0.0;

                public double this[int i]
                {
                    get
                    {
                        switch (i)
                        {
                            case 0: return Up;
                            case 1: return Down;
                            case 2: return Forward;
                            case 3: return Backward;
                            case 4: return Left;
                            case 5: return Right;
                        }

                        return -1;
                    }
                }

                public static string GetNameFromIndex(int index)
                {
                    switch (index)
                    {
                        case 0: return "Up:       ";
                        case 1: return "Down:     ";
                        case 2: return "Forward:  ";
                        case 3: return "Backward: ";
                        case 4: return "Left:     ";
                        case 5: return "Right:    ";
                    }

                    return "ERROR-UNKNOWN";
                }
            }

            private static List<IMyTextSurface> LogSurfaces = new List<IMyTextSurface>();

            private static IMyCockpit Cockpit = null;
            private static List<IMyThrust> AtmoThrusters = new List<IMyThrust>();
            private static List<IMyThrust> HydroThrusters = new List<IMyThrust>();
            private static List<IMyThrust> IonThrusters = new List<IMyThrust>();

            private static ThrustValues AtmoThrustValues = new ThrustValues();
            private static ThrustValues HydroThrustValues = new ThrustValues();
            private static ThrustValues IonThrustValues = new ThrustValues();

            private static bool Initialized = false;
            private static void Init(IMyGridTerminalSystem GridTerminalSystem, IMyProgrammableBlock Me)
            {
                if (!Initialized)
                {
                    var terminalBlocks = new List<IMyTerminalBlock>();
                    GridTerminalSystem.SearchBlocksOfName(ThrustToWeightDisplayBlockName, terminalBlocks, b => b.CubeGrid == Me.CubeGrid);
                    LogSurfaces = terminalBlocks.Where(b => b is IMyTextSurface).Select(b => (IMyTextSurface)b).ToList();

                    Initialized = true;
                }
            }

            private static void GetThrustDirectionNewtonValues(
            List<IMyThrust> thrusters, out ThrustValues thrustValues)
            {
                thrustValues = new ThrustValues();
                thrustValues.Up = 0.0;
                thrustValues.Down = 0.0;
                thrustValues.Forward = 0.0;
                thrustValues.Backward = 0.0;
                thrustValues.Left = 0.0;
                thrustValues.Right = 0.0;

                // Sum up all the thruster values
                foreach (var thruster in thrusters)
                {
                    if (thruster.GridThrustDirection == Vector3I.Up)
                    {
                        thrustValues.Up += thruster.MaxThrust;
                    }
                    else if (thruster.GridThrustDirection == Vector3I.Down)
                    {
                        thrustValues.Down += thruster.MaxThrust;
                    }
                    else if (thruster.GridThrustDirection == Vector3I.Forward)
                    {
                        thrustValues.Forward = thruster.MaxThrust;
                    }
                    else if (thruster.GridThrustDirection == Vector3I.Backward)
                    {
                        thrustValues.Backward += thruster.MaxThrust;
                    }
                    else if (thruster.GridThrustDirection == Vector3I.Left)
                    {
                        thrustValues.Left += thruster.MaxThrust;
                    }
                    else if (thruster.GridThrustDirection == Vector3I.Right)
                    {
                        thrustValues.Right += thruster.MaxThrust;
                    }
                }
            }

            public static void Tick(IMyGridTerminalSystem GridTerminalSystem, string argument, IMyProgrammableBlock Me)
            {
                Init(GridTerminalSystem, Me);

                if (argument == "clear")
                {
                    LogSurfaces.ForEach(l => l.WriteText(""));
                }

                var cockpits = new List<IMyCockpit>();
                GridTerminalSystem.GetBlocksOfType(cockpits, c => c.CubeGrid == Me.CubeGrid && c.IsUnderControl);
                Cockpit = cockpits.FirstOrDefault();

                if (Cockpit == null)
                {
                    LogSurfaces.ForEach(l => l.WriteText("Information only available when actively piloting"));
                    return;
                }

                var thrusters = new List<IMyThrust>();
                GridTerminalSystem.GetBlocksOfType(thrusters, t => t.CubeGrid == Me.CubeGrid);

                AtmoThrusters = thrusters.Where(t => t.BlockDefinition.SubtypeId.Contains("AtmosphericThrust")).ToList();
                HydroThrusters = thrusters.Where(t => t.BlockDefinition.SubtypeId.Contains("HydrogenThrust")).ToList();
                IonThrusters = thrusters.Where(t => t.BlockDefinition.SubtypeId.Contains("SmallThrust") || t.BlockDefinition.SubtypeId.Contains("LargeThurst")).ToList();

                GetThrustDirectionNewtonValues(AtmoThrusters, out AtmoThrustValues);
                GetThrustDirectionNewtonValues(HydroThrusters, out HydroThrustValues);
                GetThrustDirectionNewtonValues(IonThrusters, out IonThrustValues);

                double Elevation;
                bool IsUnderPlanetGravityEffects = Cockpit.TryGetPlanetElevation(MyPlanetElevation.Surface, out Elevation);
                double ShipMass = Cockpit.CalculateShipMass().PhysicalMass;
                double Gravity = IsUnderPlanetGravityEffects ? Math.Abs(Cockpit.GetNaturalGravity().Length()) : 9.81;

                var sb = new StringBuilder();
                if (AtmoThrusters.Count > 0)
                {   
                    double ElevationFactor = -0.00009 * Elevation + 0.9;
                    sb.AppendLine($"=== {AtmoThrusters.Count} Atmospheric Thrusters ===");
                    for (int i = 0; i < 6; ++i)
                    {
                        double ForceWeight = AtmoThrustValues[i] * ElevationFactor / Gravity - ShipMass;
                        string PositiveOrNegative = ForceWeight > 0.0 ? "+" : "-";
                        ForceWeight = Math.Abs(ForceWeight);

                        double ForceWeightKNewtons = ForceWeight / 1000.0;

                        string EarthSuffix = IsUnderPlanetGravityEffects ? "" : " (Earth)";
                        sb.AppendLine($"  {ThrustValues.GetNameFromIndex(i)}{PositiveOrNegative}{ForceWeightKNewtons:N2}kN @ {-Gravity:N2}m/s2{EarthSuffix}");
                    }
                    sb.AppendLine();
                }

                if (HydroThrusters.Count > 0)
                {
                    sb.AppendLine($"=== {HydroThrusters.Count} Hydrogen Thrusters ===");
                    for (int i = 0; i < 6; ++i)
                    {
                        double ForceWeight = AtmoThrustValues[i] / Gravity - ShipMass;
                        string PositiveOrNegative = ForceWeight > 0.0 ? "+" : "-";
                        ForceWeight = Math.Abs(ForceWeight);

                        double ForceWeightKNewtons = ForceWeight / 1000.0;
                        sb.AppendLine($"  {ThrustValues.GetNameFromIndex(i)}{PositiveOrNegative}{ForceWeightKNewtons:N2}kN @ {-Gravity:N2}m/s2");
                    }
                    sb.AppendLine();
                }

                if (IonThrusters.Count > 0)
                {
                    if (!IsUnderPlanetGravityEffects)
                    {
                        // Assume sea level
                        Elevation = 0.0;
                    }

                    double ElevationFactor = 0.005 * Elevation + 30;
                    sb.AppendLine($"=== {IonThrusters.Count} Ion Thrusters ===");
                    for (int i = 0; i < 6; ++i)
                    {
                        double ForceWeight = AtmoThrustValues[i] * ElevationFactor / Gravity - ShipMass;
                        string PositiveOrNegative = ForceWeight > 0.0 ? "+" : "-";
                        ForceWeight = Math.Abs(ForceWeight);

                        double ForceWeightKNewtons = ForceWeight / 1000.0;

                        if (IsUnderPlanetGravityEffects)
                        {
                            sb.AppendLine($"  {ThrustValues.GetNameFromIndex(i)}{PositiveOrNegative}{ForceWeightKNewtons:N2}kN @ Sea Level");
                        }
                        else
                        {
                            sb.AppendLine($"  {ThrustValues.GetNameFromIndex(i)}{PositiveOrNegative}{ForceWeightKNewtons:N2}kN @ {Elevation:N0}m");
                        }
                    }
                    sb.AppendLine();
                }

                LogSurfaces.ForEach(s => s.WriteText(sb.ToString(), true));
            }
        }
    }
}
