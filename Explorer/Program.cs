﻿using Sandbox.ModAPI.Ingame;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VRageMath;

namespace IngameScript
{
    partial class Program : MyGridProgram
    {
        public Program()
        {
            
        }

        private static List<IMyTextSurface> Surfaces = new List<IMyTextSurface>();

        private static bool Init = false;
        public void Main(string argument, UpdateType updateSource)
        {
            if (!Init)
            {
                List<IMyCockpit> Cockpits = new List<IMyCockpit>();
                GridTerminalSystem.GetBlocksOfType(Cockpits, b => b.CubeGrid == Me.CubeGrid);
                Surfaces = Cockpits.Select(c => c.GetSurface(0)).ToList();
                Surfaces.Add(GridTerminalSystem.GetBlockWithName("[Explorer] Front LCD") as IMyTextSurface);
                Init = true;
            }

#if false
            ThrustToWeightManager.Tick(GridTerminalSystem, "clear", Me, LogSurfaces);
#endif
            BatteryManagement.Tick(GridTerminalSystem, "clear", Me, Surfaces);
            DistanceToStopManager.Tick(GridTerminalSystem, "", Me, Surfaces);
            RaycastManager.Tick(GridTerminalSystem, "", Me, Surfaces);
        }

#if false
        public static class ThrustToWeightManager
        {
            class ThrustValues
            {
                public double Up = 0.0;
                public double Down = 0.0;
                public double Forward = 0.0;
                public double Backward = 0.0;
                public double Left = 0.0;
                public double Right = 0.0;

                public double this[int i]
                {
                    get
                    {
                        switch (i)
                        {
                            case 0: return Up;
                            case 1: return Down;
                            case 2: return Forward;
                            case 3: return Backward;
                            case 4: return Left;
                            case 5: return Right;
                        }

                        return -1;
                    }
                }

                public static string GetNameFromIndex(int index)
                {
                    switch (index)
                    {
                        case 0: return "Up:       ";
                        case 1: return "Down:     ";
                        case 2: return "Forward:  ";
                        case 3: return "Backward: ";
                        case 4: return "Left:     ";
                        case 5: return "Right:    ";
                    }

                    return "ERROR-UNKNOWN";
                }
            }

            private static List<IMyTextSurface> LogSurfaces = new List<IMyTextSurface>();

            private static IMyCockpit Cockpit = null;
            private static List<IMyThrust> AtmoThrusters = new List<IMyThrust>();
            private static List<IMyThrust> HydroThrusters = new List<IMyThrust>();
            private static List<IMyThrust> IonThrusters = new List<IMyThrust>();

            private static ThrustValues AtmoThrustValues = new ThrustValues();
            private static ThrustValues HydroThrustValues = new ThrustValues();
            private static ThrustValues IonThrustValues = new ThrustValues();

            private static bool Initialized = false;
            private static void Init(IMyGridTerminalSystem GridTerminalSystem, IMyProgrammableBlock Me, List<IMyTextSurface> LogSurfaces)
            {
                if (!Initialized)
                {
                    this.LogSurfaces = LogSurfaces;
                    Initialized = true;
                }
            }

            private static void GetThrustDirectionNewtonValues(
            List<IMyThrust> thrusters, out ThrustValues thrustValues)
            {
                thrustValues = new ThrustValues();
                thrustValues.Up = 0.0;
                thrustValues.Down = 0.0;
                thrustValues.Forward = 0.0;
                thrustValues.Backward = 0.0;
                thrustValues.Left = 0.0;
                thrustValues.Right = 0.0;

                // Sum up all the thruster values
                foreach (var thruster in thrusters)
                {
                    if (thruster.GridThrustDirection == Vector3I.Up)
                    {
                        thrustValues.Up += thruster.MaxThrust;
                    }
                    else if (thruster.GridThrustDirection == Vector3I.Down)
                    {
                        thrustValues.Down += thruster.MaxThrust;
                    }
                    else if (thruster.GridThrustDirection == Vector3I.Forward)
                    {
                        thrustValues.Forward = thruster.MaxThrust;
                    }
                    else if (thruster.GridThrustDirection == Vector3I.Backward)
                    {
                        thrustValues.Backward += thruster.MaxThrust;
                    }
                    else if (thruster.GridThrustDirection == Vector3I.Left)
                    {
                        thrustValues.Left += thruster.MaxThrust;
                    }
                    else if (thruster.GridThrustDirection == Vector3I.Right)
                    {
                        thrustValues.Right += thruster.MaxThrust;
                    }
                }
            }

            public static void Tick(IMyGridTerminalSystem GridTerminalSystem, string argument, IMyProgrammableBlock Me, List<IMyTextSurface> LogSurfaces)
            {
                Init(GridTerminalSystem, Me, LogSurfaces);

                if (argument == "clear")
                {
                    LogSurfaces.ForEach(l => l.WriteText(""));
                }

                var cockpits = new List<IMyCockpit>();
                GridTerminalSystem.GetBlocksOfType(cockpits, c => c.CubeGrid == Me.CubeGrid && c.IsUnderControl);
                Cockpit = cockpits.FirstOrDefault();

                if (Cockpit == null)
                {
                    LogSurfaces.ForEach(l => l.WriteText("Information only available when actively piloting"));
                    return;
                }

                var thrusters = new List<IMyThrust>();
                GridTerminalSystem.GetBlocksOfType(thrusters, t => t.CubeGrid == Me.CubeGrid);

                AtmoThrusters = thrusters.Where(t => t.BlockDefinition.SubtypeId.Contains("AtmosphericThrust")).ToList();
                HydroThrusters = thrusters.Where(t => t.BlockDefinition.SubtypeId.Contains("HydrogenThrust")).ToList();
                IonThrusters = thrusters.Where(t => t.BlockDefinition.SubtypeId.Contains("SmallThrust") || t.BlockDefinition.SubtypeId.Contains("LargeThurst")).ToList();

                GetThrustDirectionNewtonValues(AtmoThrusters, out AtmoThrustValues);
                GetThrustDirectionNewtonValues(HydroThrusters, out HydroThrustValues);
                GetThrustDirectionNewtonValues(IonThrusters, out IonThrustValues);

                double Elevation;
                bool IsUnderPlanetGravityEffects = Cockpit.TryGetPlanetElevation(MyPlanetElevation.Surface, out Elevation);
                if (!IsUnderPlanetGravityEffects)
                {
                    // Pretend our elevation is sea-level
                    Elevation = 0.0;
                }

                double ShipMass = Cockpit.CalculateShipMass().TotalMass;
                
                double Gravity = IsUnderPlanetGravityEffects ? Math.Abs(Cockpit.GetNaturalGravity().Length()) : 9.81;

                string EarthSuffix = IsUnderPlanetGravityEffects ? "" : " (Earth)";

                var sb = new StringBuilder();
                sb.AppendLine($"Mass: {ShipMass:N2}");
                if (AtmoThrusters.Count > 0)
                {
                    double ElevationFactor = -0.00009 * Elevation + 0.9;
                    
                    sb.AppendLine($"=== {AtmoThrusters.Count} Atmospheric Thrusters ===");
                    for (int i = 0; i < 6; ++i)
                    {
                        double ForceWeight = AtmoThrustValues[i] * ElevationFactor / Gravity - ShipMass;
                        string PositiveOrNegative = ForceWeight > 0.0 ? "+" : "-";
                        ForceWeight = Math.Abs(ForceWeight);
                        sb.AppendLine($"  {ThrustValues.GetNameFromIndex(i)}{PositiveOrNegative}{ForceWeight:N2}kN @ {-Gravity:N2}m/s2{EarthSuffix}");
                    }
                    sb.AppendLine();
                }

                if (HydroThrusters.Count > 0)
                {
                    sb.AppendLine($"=== {HydroThrusters.Count} Hydrogen Thrusters ===");
                    for (int i = 0; i < 6; ++i)
                    {
                        double ForceWeight = AtmoThrustValues[i] / Gravity - ShipMass;
                        string PositiveOrNegative = ForceWeight > 0.0 ? "+" : "-";
                        ForceWeight = Math.Abs(ForceWeight);
                        sb.AppendLine($"  {ThrustValues.GetNameFromIndex(i)}{PositiveOrNegative}{ForceWeight:N2}kN @ {-Gravity:N2}m/s2{EarthSuffix}");
                    }
                    sb.AppendLine();
                }

                if (IonThrusters.Count > 0)
                {
                    double ElevationFactor = 0.005 * Elevation + 30;
                    sb.AppendLine($"=== {IonThrusters.Count} Ion Thrusters ===");
                    for (int i = 0; i < 6; ++i)
                    {
                        double ForceWeight = IonThrustValues[i] * ElevationFactor / Gravity - ShipMass;
                        string PositiveOrNegative = ForceWeight > 0.0 ? "+" : "-";
                        ForceWeight = Math.Abs(ForceWeight);
                        sb.AppendLine($"  {ThrustValues.GetNameFromIndex(i)}{PositiveOrNegative}{ForceWeight:N2}kN @ {-Gravity:N2}m/s2{EarthSuffix}");
                    }
                    sb.AppendLine();
                }

                LogSurfaces.ForEach(s => s.WriteText(sb.ToString(), true));
            }
        }
#endif

        public static class BatteryManagement
        {
            private static List<IMyTextSurface> BatteryManagementLogSurfaces = new List<IMyTextSurface>();
            private static List<IMyBatteryBlock> Batteries = new List<IMyBatteryBlock>();
            private static List<IMyJumpDrive> JumpDrives = new List<IMyJumpDrive>();

            private static bool IsInitialized = false;
            private static void Init(IMyGridTerminalSystem GridTerminalSystem, IMyProgrammableBlock Me, List<IMyTextSurface> surfaces)
            {
                if (!IsInitialized)
                {
                    BatteryManagementLogSurfaces.AddRange(surfaces);
                    GridTerminalSystem.GetBlocksOfType(Batteries, b => b.CubeGrid == Me.CubeGrid);
                    GridTerminalSystem.GetBlocksOfType(JumpDrives, b => b.CubeGrid == Me.CubeGrid);
                    IsInitialized = true;
                }
            }

            public static void Tick(IMyGridTerminalSystem GridTerminalSystem, string argument, IMyProgrammableBlock Me, List<IMyTextSurface> surfaces)
            {
                Init(GridTerminalSystem, Me, surfaces);

                if (argument == "clear")
                {
                    BatteryManagementLogSurfaces.ForEach(s => s.WriteText(""));
                }

                StringBuilder sb = new StringBuilder();
                if (Batteries.Count > 0)
                {
                    float CurrStoredPower = Batteries.Sum(b => b.CurrentStoredPower);
                    float MaxStoredPower = Batteries.Sum(b => b.MaxStoredPower);
                    float PowerPercentage = (CurrStoredPower / MaxStoredPower) * 100.0f;

                    bool AnyCharging = Batteries.Any(b => b.IsCharging);
                    float CurrentInput = Batteries.Sum(b => b.CurrentInput);
                    float CurrentOutput = Batteries.Sum(b => b.CurrentOutput);
                    float NetInput = CurrentInput - CurrentOutput;

                    sb.Append($"Battery: {PowerPercentage:N2}%");
                    if (NetInput > 0.0f)
                    {
                        float NetInputPerMinute = NetInput / 60.0f;
                        float RemainingStorage = MaxStoredPower - CurrStoredPower;
                        float MinutesToFullyCharged = RemainingStorage / NetInputPerMinute;

                        if (MinutesToFullyCharged > 0.1f)
                        {
                            sb.Append($" ({MinutesToFullyCharged:N1}m)");
                        }
                    }
                    sb.AppendLine();
                }

                if (JumpDrives.Count > 0)
                {
                    float CurrStoredPower = JumpDrives.Sum(j => j.CurrentStoredPower);
                    float MaxStoredPower = JumpDrives.Sum(j => j.MaxStoredPower);
                    float PowerPercentage = (CurrStoredPower / MaxStoredPower) * 100.0f;

                    sb.AppendLine($"Jump: {PowerPercentage:N2}% ({JumpDrives[0].MaxJumpDistanceMeters / 1000.0:N2}km)");
                }

                if (sb.Length > 0)
                {
                    BatteryManagementLogSurfaces.ForEach(s => s.WriteText($"{sb}\n", true));
                }
            }
        }

        public static class DistanceToStopManager
        {
            class ThrustValues
            {
                public double Up = 0.0;
                public double Down = 0.0;
                public double Forward = 0.0;
                public double Backward = 0.0;
                public double Left = 0.0;
                public double Right = 0.0;

                public double this[int i]
                {
                    get
                    {
                        switch (i)
                        {
                            case 0: return Up;
                            case 1: return Down;
                            case 2: return Forward;
                            case 3: return Backward;
                            case 4: return Left;
                            case 5: return Right;
                        }

                        return -1;
                    }
                }

                public static string GetNameFromIndex(int index)
                {
                    switch (index)
                    {
                        case 0: return "Up:       ";
                        case 1: return "Down:     ";
                        case 2: return "Forward:  ";
                        case 3: return "Backward: ";
                        case 4: return "Left:     ";
                        case 5: return "Right:    ";
                    }

                    return "ERROR-UNKNOWN";
                }
            }

            private static List<IMyTextSurface> LogSurfaces = new List<IMyTextSurface>();

            private static IMyCockpit Cockpit = null;
            private static List<IMyThrust> Thrusters = new List<IMyThrust>();
            private static ThrustValues AllThrustValues = new ThrustValues();

            private static bool Initialized = false;
            private static void Init(IMyGridTerminalSystem GridTerminalSystem, IMyProgrammableBlock Me, List<IMyTextSurface> surfaces)
            {
                if (!Initialized)
                {
                    LogSurfaces.AddRange(surfaces);
                    Initialized = true;
                }
            }

            private static void GetThrustDirectionNewtonValues(
            List<IMyThrust> thrusters, out ThrustValues thrustValues)
            {
                thrustValues = new ThrustValues();
                thrustValues.Up = 0.0;
                thrustValues.Down = 0.0;
                thrustValues.Forward = 0.0;
                thrustValues.Backward = 0.0;
                thrustValues.Left = 0.0;
                thrustValues.Right = 0.0;

                // Sum up all the thruster values
                foreach (var thruster in thrusters)
                {
                    if (thruster.GridThrustDirection == Vector3I.Up)
                    {
                        thrustValues.Up += thruster.MaxThrust;
                    }
                    else if (thruster.GridThrustDirection == Vector3I.Down)
                    {
                        thrustValues.Down += thruster.MaxThrust;
                    }
                    else if (thruster.GridThrustDirection == Vector3I.Forward)
                    {
                        thrustValues.Forward = thruster.MaxThrust;
                    }
                    else if (thruster.GridThrustDirection == Vector3I.Backward)
                    {
                        thrustValues.Backward += thruster.MaxThrust;
                    }
                    else if (thruster.GridThrustDirection == Vector3I.Left)
                    {
                        thrustValues.Left += thruster.MaxThrust;
                    }
                    else if (thruster.GridThrustDirection == Vector3I.Right)
                    {
                        thrustValues.Right += thruster.MaxThrust;
                    }
                }
            }

            private static string GetDistanceString(double Meters)
            {
                if (Meters > 1000.0)
                {
                    return $"{Meters / 1000.0:N2}km";
                }
                else
                {
                    return $"{Meters:N2}m";
                }
            }

            public static void Tick(IMyGridTerminalSystem GridTerminalSystem, string argument, IMyProgrammableBlock Me, List<IMyTextSurface> surfaces)
            {
                Init(GridTerminalSystem, Me, surfaces);

                if (argument == "clear")
                {
                    LogSurfaces.ForEach(l => l.WriteText(""));
                }

                var cockpits = new List<IMyCockpit>();
                GridTerminalSystem.GetBlocksOfType(cockpits, c => c.CubeGrid == Me.CubeGrid && c.IsUnderControl);
                Cockpit = cockpits.FirstOrDefault();

                if (Cockpit == null)
                {
                    LogSurfaces.ForEach(l => l.WriteText("Information only available when actively piloting", true));
                    return;
                }

                GridTerminalSystem.GetBlocksOfType(Thrusters, t => t.CubeGrid == Me.CubeGrid);
                GetThrustDirectionNewtonValues(Thrusters, out AllThrustValues);

                double ShipMass = Cockpit.CalculateShipMass().TotalMass;
                double CurrentShipSpeed = Cockpit.GetShipSpeed();
                double ShipSpeedSquared = CurrentShipSpeed * CurrentShipSpeed;
                double RawBrakeForce = AllThrustValues.Backward > 200.0 ? 200.0 : AllThrustValues.Backward;
                double TwoXBrakeForce = RawBrakeForce * 2;
                double DistanceToStop = ShipMass * ShipSpeedSquared / TwoXBrakeForce;

                // We're going to reduce our speed linearly apply our brakeforce
                // Speed = -BrakeForce + CurrentShipSpeed

                var sb = new StringBuilder();
                const double FudgeFactor = 10000.0; // No idea
                sb.AppendLine($"Stop: {GetDistanceString(DistanceToStop / FudgeFactor)}");

                LogSurfaces.ForEach(s => s.WriteText(sb.ToString(), true));
            }
        }

        public static class RaycastManager
        {
            private const string CameraBlockName = "[Explorer] Camera";
            private static List<IMyTextSurface> LogSurfaces = new List<IMyTextSurface>();
            private static IMyCameraBlock Camera = null;

            private static Vector3D? PrevHitPosition = null;

            private static bool Initialized = false;
            private static void Init(IMyGridTerminalSystem GridTerminalSystem, string argument, IMyProgrammableBlock Me, List<IMyTextSurface> surfaces)
            {
                if (!Initialized)
                {
                    LogSurfaces.AddRange(surfaces);
                    Camera = GridTerminalSystem.GetBlockWithName(CameraBlockName) as IMyCameraBlock;
                    Camera.EnableRaycast = true;

                    Initialized = true;
                }
            }

            private static string GetTimeUntilNextScanString(int TimeUntilNextScan)
            {
                if (TimeUntilNextScan > 0)
                {
                    double Seconds = (double)TimeUntilNextScan / 1000;
                    return $" ({Seconds:N1}s)";
                }

                return "";
            }

            private static string GetDistanceString(double Meters)
            {
                if (Meters > 1000.0)
                {
                    return $"{Meters / 1000.0:N2}km";
                }
                else
                {
                    return $"{Meters:N2}m";
                }
            }

            public static void Tick(IMyGridTerminalSystem GridTerminalSystem, string argument, IMyProgrammableBlock Me, List<IMyTextSurface> surfaces)
            {
                Init(GridTerminalSystem, argument, Me, surfaces);

                const double ScanDistance = 20000.0;
                int TimeUntilNextScan = Camera.TimeUntilScan(ScanDistance);
                if (TimeUntilNextScan <= 0)
                {
                    // Do the scan
                    var ScanResults = Camera.Raycast(ScanDistance);
                    PrevHitPosition = ScanResults.HitPosition;
                }

                if (PrevHitPosition != null)
                {
                    double distance = Vector3D.Distance(Camera.GetPosition(), (Vector3D)PrevHitPosition);
                    LogSurfaces.ForEach(s => s.WriteText($"Distance: {GetDistanceString(distance)}{GetTimeUntilNextScanString(TimeUntilNextScan)}\n", true));
                }
                else
                {
                    LogSurfaces.ForEach(s => s.WriteText($"Distance: [Nothing]{GetTimeUntilNextScanString(TimeUntilNextScan)}\n", true));
                }
            }
        }
    }
}
