﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Sandbox.ModAPI.Ingame;
using SpaceEngineers.Game.ModAPI.Ingame;
using VRage.Game;
using VRage.Game.ModAPI.Ingame;
using VRageMath;

namespace IngameScript
{
    partial class Program : MyGridProgram
    {
        // On any node that supports displays, set the display to Text and Images, then click "Edit CustomData"
        // and put in the following based on what you want to display
        //
        /*
        {{START_SURFACE;Index=0;ThrustDir=Down;Camera=Name Of Camera}}  // Indicates the start of this display surface
                                                                        //      Index: Default of 0. For blocks with multiple surfaces
                                                                        //      ThrustDir: For ThrustManager calculations. If not present, assumed to be "All"
                                                                        //      Camera: For the name of the camera block for distance calculations
        %BatteryCount% Batteries: %BatteryPercentage%% (%BatteryTime%)
           Curr: %BatteryStored% Max: %BatteryMax%
           Input:  %BatteryInput%
           Output: %BatteryOutput%
              Net: %BatteryNet%

        %WindCount% Turbines: %Wind%
        %SolarCount% Panels: %Solar%
        %ReactorCount% Reactors: %Reactor%

        Oxygen: %OxygenPercentage% (%OxygenCount% Tanks)
        Hydrogen: %HydrogenPercentage% (%HydrogenCount% Tanks)

        Drills:
        %DrillsVolume% / %DrillsMaxVolume% (%DrillsMass%)
           %DrillsPercentage%%

        %OreInformation%
        %IngotInformation%

        %TotalCargoMass%
        %TotalCargoPercentage%

        Distance From Camera: %Distance% (Next Scan: %DistanceNextScanTime%)

        Atmospheric Thrusters:
        %AtmoThrusterInformation%

        Hydrogen Thrusters:
        %HydroThrusterInformation%

        IonThrusters:
        %IonThrusterInformation%

        %DrillerStatus%
        %DrillerEventLog%

        %HoverCurrentHeight%
        %HoverRequestedHeight%
        %HoverCurrentOverride%

        %TurretAmmoInformation%
        {{END_SURFACE}}                                                 // Indicates the end of this display surface
        */

        public const bool SearchOnlyThisGrid = true;

        public Program()
        {
            Runtime.UpdateFrequency = UpdateFrequency.Update1;
        }

        private static bool Initialized = false;
        public static int TickCounter = 0;
        private void Init()
        {
            if (!Initialized)
            {
                PowerManager.Init(this);
                CargoManager.Init(this);
                var AirlockSets = new List<string>
                {
                    "[HydroShip] Entrance Airlock Door 1", "[HydroShip] Entrance Airlock Door 2"
                };
                AirlockManager.Init(this, AirlockSets);
                GasManager.Init(this);
                DistanceManager.Init(this);
                ThrustManager.Init(this);
                ShipManager.Init(this);
                TurretAmmoManager.Init(this);
                SurfaceManager.Init(this);
                
                Initialized = true;
            }
        }

        public void Main(string argument, UpdateType updateSource)
        {
            if (!Initialized)
            {
                Init();
            }

            PowerManager.Tick();
            CargoManager.Tick();
            GasManager.Tick();
            foreach (var CameraBlockName in SurfaceManager.CameraBlockNames)
            {
                DistanceManager.Tick(CameraBlockName);
            }
            ThrustManager.Tick();
            ShipManager.Tick();
            TurretAmmoManager.Tick();

            SurfaceManager.Tick();

            TickCounter = TickCounter++ % 100000;
        }

        public static bool ShouldUpdate(int TickFreq)
        {
            return TickCounter % TickFreq == 0;
        }

        #region Surface
        public static class SurfaceManager
        {
            private static IMyGridTerminalSystem GridTerminalSystem;
            private static IMyProgrammableBlock Me;

            class ScriptDef
            {
                public IMyTextSurface Surface = null;
                public int SurfaceIdx = 0;
                public ThrustDirection ThrustDirection = ThrustDirection.All;
                public string CameraBlockName = null;
                public IMyCameraBlock CameraBlock = null;
                public string Script = "";
            }

            private static List<ScriptDef> ScriptDefs = new List<ScriptDef>();

            public static List<string> CameraBlockNames
            {
                get
                {
                    return ScriptDefs
                        .Where(sd => !string.IsNullOrEmpty(sd.CameraBlockName))
                        .Select(sd => sd.CameraBlockName)
                        .ToList();
                }
            }

            public static void Init(Program prog)
            {
                GridTerminalSystem = prog.GridTerminalSystem;
                Me = prog.Me;
            }

            const int UpdateSurfaceScriptsFreq = 333;
            private static void UpdateSurfaceScripts()
            {
                if (ShouldUpdate(UpdateSurfaceScriptsFreq))
                {
                    ScriptDefs.Clear();

                    List<IMyTerminalBlock> blocks = new List<IMyTerminalBlock>();
                    GridTerminalSystem.GetBlocks(blocks);

                    blocks = blocks
                        .Where(b => b is IMyTextSurfaceProvider)
                        .Where(b => !string.IsNullOrWhiteSpace(b.CustomData))
                        .ToList();

                    if (SearchOnlyThisGrid)
                    {
                        blocks = blocks
                            .Where(b => b.CubeGrid == Me.CubeGrid)
                            .ToList();
                    }

                    foreach (var block in blocks)
                    {
                        string SurfaceScript = !string.IsNullOrWhiteSpace(block.CustomData) ? block.CustomData : "";
                        if (SurfaceScript.Contains("{{START_SURFACE") && SurfaceScript.Contains("{{END_SURFACE}}"))
                        {
                            ScriptDef scriptDef = null;

                            var lines = SurfaceScript.Split(new char[] { '\r', '\n' });
                            foreach (var line in lines)
                            {
                                if (line.StartsWith("{{START_SURFACE"))
                                {
                                    string trimmedLine = line.Trim().Replace("{{", "").Replace("}}", "");
                                    scriptDef = new ScriptDef();
                                    var startSurfaceSplitRes = trimmedLine.Split(new char[] { ';' }, 4);
                                    foreach (var startSurfaceSplitItem in startSurfaceSplitRes)
                                    {
                                        const string IndexEqualStr = "Index=";
                                        if (startSurfaceSplitItem.StartsWith(IndexEqualStr, StringComparison.OrdinalIgnoreCase))
                                        {
                                            int index;
                                            if (int.TryParse(startSurfaceSplitItem.Substring(IndexEqualStr.Length), out index))
                                            {
                                                var surfaceProviderBlock = (IMyTextSurfaceProvider)block;
                                                if (index < surfaceProviderBlock.SurfaceCount)
                                                {
                                                    scriptDef.SurfaceIdx = index;
                                                }
                                            }
                                        }

                                        const string ThrustDirEqualStr = "ThrustDir=";
                                        if (startSurfaceSplitItem.StartsWith(ThrustDirEqualStr, StringComparison.OrdinalIgnoreCase))
                                        {
                                            ThrustDirection dir;
                                            string inputdir = startSurfaceSplitItem.Substring(ThrustDirEqualStr.Length).ToLower();
                                            if (ThrustManager.DirectionStringToThrustDirection.TryGetValue(inputdir, out dir))
                                            {
                                                scriptDef.ThrustDirection = dir;
                                            }
                                        }

                                        const string CameraEqualStr = "Camera=";
                                        if (startSurfaceSplitItem.StartsWith(CameraEqualStr, StringComparison.OrdinalIgnoreCase))
                                        {
                                            scriptDef.CameraBlockName = startSurfaceSplitItem.Substring(CameraEqualStr.Length);

                                            var cameraTerminalBlocks = new List<IMyTerminalBlock>();
                                            GridTerminalSystem.SearchBlocksOfName(scriptDef.CameraBlockName, cameraTerminalBlocks, c => c is IMyCameraBlock);
                                            scriptDef.CameraBlock = cameraTerminalBlocks.FirstOrDefault() as IMyCameraBlock;
                                        }
                                    }
                                } 
                                else if (line.StartsWith("{{END_SURFACE}}"))
                                {
                                    if (scriptDef != null)
                                    {
                                        var surfaceProviderBlock = (IMyTextSurfaceProvider)block;
                                        scriptDef.Surface = surfaceProviderBlock.GetSurface(scriptDef.SurfaceIdx);
                                        if (scriptDef.Surface != null)
                                        {
                                            ScriptDefs.Add(scriptDef);
                                        }
                                        
                                        scriptDef = null;
                                    }
                                }
                                else if (scriptDef != null)
                                {
                                    scriptDef.Script += line + "\n";
                                }
                            }
                        }
                    }
                }
            }

            public static void Tick()
            {
                UpdateSurfaceScripts();

                foreach (var scriptDef in ScriptDefs)
                {
                    var ValueReplacedScript = scriptDef.Script;
                    // %VARIABLES%
                    ValueReplacedScript = ValueReplacedScript.Replace("%BatteryPercentage%", PowerManager.BatteryPercentage);
                    ValueReplacedScript = ValueReplacedScript.Replace("%BatteryTime%", PowerManager.BatteryTime);
                    ValueReplacedScript = ValueReplacedScript.Replace("%BatteryCount%", PowerManager.BatteryCount);
                    ValueReplacedScript = ValueReplacedScript.Replace("%BatteryInput%", PowerManager.BatteryInput);
                    ValueReplacedScript = ValueReplacedScript.Replace("%BatteryOutput%", PowerManager.BatteryOutput);
                    ValueReplacedScript = ValueReplacedScript.Replace("%BatteryNet%", PowerManager.BatteryNet);
                    ValueReplacedScript = ValueReplacedScript.Replace("%BatteryStored%", PowerManager.BatteryStored);
                    ValueReplacedScript = ValueReplacedScript.Replace("%BatteryMax%", PowerManager.BatteryMax);
                    ValueReplacedScript = ValueReplacedScript.Replace("%Wind%", PowerManager.Wind);
                    ValueReplacedScript = ValueReplacedScript.Replace("%WindCount%", PowerManager.WindCount);
                    ValueReplacedScript = ValueReplacedScript.Replace("%Solar%", PowerManager.Solar);
                    ValueReplacedScript = ValueReplacedScript.Replace("%SolarCount%", PowerManager.SolarCount);
                    ValueReplacedScript = ValueReplacedScript.Replace("%Reactor%", PowerManager.Reactor);
                    ValueReplacedScript = ValueReplacedScript.Replace("%ReactorCount%", PowerManager.ReactorCount);

                    // Drills
                    ValueReplacedScript = ValueReplacedScript.Replace("%DrillsMass%", CargoManager.DrillsMass);
                    ValueReplacedScript = ValueReplacedScript.Replace("%DrillsVolume%", CargoManager.DrillsVolume);
                    ValueReplacedScript = ValueReplacedScript.Replace("%DrillsMaxVolume%", CargoManager.DrillsMaxVolume);
                    ValueReplacedScript = ValueReplacedScript.Replace("%DrillsPercentage%", CargoManager.DrillsPercentage);
                    // Ores
                    ValueReplacedScript = ValueReplacedScript.Replace("%OreInformation%", CargoManager.OreInformation);
                    ValueReplacedScript = ValueReplacedScript.Replace("%Ice%", CargoManager.Ice);
                    ValueReplacedScript = ValueReplacedScript.Replace("%Stone%", CargoManager.Stone);
                    ValueReplacedScript = ValueReplacedScript.Replace("%Scrap%", CargoManager.Scrap);
                    ValueReplacedScript = ValueReplacedScript.Replace("%IronOre%", CargoManager.IronOre);
                    ValueReplacedScript = ValueReplacedScript.Replace("%NickelOre%", CargoManager.NickelOre);
                    ValueReplacedScript = ValueReplacedScript.Replace("%CobaltOre%", CargoManager.CobaltOre);
                    ValueReplacedScript = ValueReplacedScript.Replace("%MagnesiumOre%", CargoManager.MagnesiumOre);
                    ValueReplacedScript = ValueReplacedScript.Replace("%SiliconOre%", CargoManager.SiliconOre);
                    ValueReplacedScript = ValueReplacedScript.Replace("%SilverOre%", CargoManager.SilverOre);
                    ValueReplacedScript = ValueReplacedScript.Replace("%GoldOre%", CargoManager.GoldOre);
                    ValueReplacedScript = ValueReplacedScript.Replace("%PlatinumOre%", CargoManager.PlatinumOre);
                    ValueReplacedScript = ValueReplacedScript.Replace("%UraniumOre%", CargoManager.UraniumOre);
                    //Ingots
                    ValueReplacedScript = ValueReplacedScript.Replace("%IngotInformation%", CargoManager.IngotInformation);
                    ValueReplacedScript = ValueReplacedScript.Replace("%Gravel%", CargoManager.Gravel);
                    ValueReplacedScript = ValueReplacedScript.Replace("%IronIngot%", CargoManager.IronIngot);
                    ValueReplacedScript = ValueReplacedScript.Replace("%NickelIngot%", CargoManager.NickelIngot);
                    ValueReplacedScript = ValueReplacedScript.Replace("%CobaltIngot%", CargoManager.CobaltIngot);
                    ValueReplacedScript = ValueReplacedScript.Replace("%MagnesiumIngot%", CargoManager.MagnesiumIngot);
                    ValueReplacedScript = ValueReplacedScript.Replace("%SiliconWafer%", CargoManager.SiliconWafer);
                    ValueReplacedScript = ValueReplacedScript.Replace("%SilverIngot%", CargoManager.SilverIngot);
                    ValueReplacedScript = ValueReplacedScript.Replace("%GoldIngot%", CargoManager.GoldIngot);
                    ValueReplacedScript = ValueReplacedScript.Replace("%PlatinumIngot%", CargoManager.PlatinumIngot);
                    ValueReplacedScript = ValueReplacedScript.Replace("%UraniumIngot%", CargoManager.UraniumIngot);
                    //Components
                    ValueReplacedScript = ValueReplacedScript.Replace("%ComponentInformation%", CargoManager.ComponentInformation);
                    ValueReplacedScript = ValueReplacedScript.Replace("%ConstructionComponent%", CargoManager.ConstructionComponent);
                    ValueReplacedScript = ValueReplacedScript.Replace("%MetalGrid%", CargoManager.MetalGrid);
                    ValueReplacedScript = ValueReplacedScript.Replace("%InteriorPlate%", CargoManager.InteriorPlate);
                    ValueReplacedScript = ValueReplacedScript.Replace("%SteelPlate%", CargoManager.SteelPlate);
                    ValueReplacedScript = ValueReplacedScript.Replace("%Girder%", CargoManager.Girder);
                    ValueReplacedScript = ValueReplacedScript.Replace("%SmallTube%", CargoManager.SmallTube);
                    ValueReplacedScript = ValueReplacedScript.Replace("%LargeTube%", CargoManager.LargeTube);
                    ValueReplacedScript = ValueReplacedScript.Replace("%Motor%", CargoManager.Motor);
                    ValueReplacedScript = ValueReplacedScript.Replace("%Display%", CargoManager.Display);
                    ValueReplacedScript = ValueReplacedScript.Replace("%BulletproofGlass%", CargoManager.BulletproofGlass);
                    ValueReplacedScript = ValueReplacedScript.Replace("%Superconductor%", CargoManager.Superconductor);
                    ValueReplacedScript = ValueReplacedScript.Replace("%Computer%", CargoManager.Computer);
                    ValueReplacedScript = ValueReplacedScript.Replace("%SolarCell%", CargoManager.SolarCell);
                    ValueReplacedScript = ValueReplacedScript.Replace("%PowerCell%", CargoManager.PowerCell);
                    //CanCraft
                    ValueReplacedScript = ValueReplacedScript.Replace("%CanCraftInformation%", CargoManager.CanCraftInformation);
                    ValueReplacedScript = ValueReplacedScript.Replace("%CanCraftConstruction%", CargoManager.CanCraftConstruction);
                    ValueReplacedScript = ValueReplacedScript.Replace("%CanCraftMetalGrid%", CargoManager.CanCraftMetalGrid);
                    ValueReplacedScript = ValueReplacedScript.Replace("%CanCraftInteriorPlate%", CargoManager.CanCraftInteriorPlate);
                    ValueReplacedScript = ValueReplacedScript.Replace("%CanCraftSteelPlate%", CargoManager.CanCraftSteelPlate);
                    ValueReplacedScript = ValueReplacedScript.Replace("%CanCraftGirder%", CargoManager.CanCraftGirder);
                    ValueReplacedScript = ValueReplacedScript.Replace("%CanCraftSmallTube%", CargoManager.CanCraftSmallTube);
                    ValueReplacedScript = ValueReplacedScript.Replace("%CanCraftLargeTube%", CargoManager.CanCraftLargeTube);
                    ValueReplacedScript = ValueReplacedScript.Replace("%CanCraftMotor%", CargoManager.CanCraftMotor);
                    ValueReplacedScript = ValueReplacedScript.Replace("%CanCraftDisplay%", CargoManager.CanCraftDisplay);
                    ValueReplacedScript = ValueReplacedScript.Replace("%CanCraftBulletproofGlass%", CargoManager.CanCraftBulletproofGlass);
                    ValueReplacedScript = ValueReplacedScript.Replace("%CanCraftSuperconductor%", CargoManager.CanCraftSuperconductor);
                    ValueReplacedScript = ValueReplacedScript.Replace("%CanCraftComputer%", CargoManager.CanCraftComputer);
                    ValueReplacedScript = ValueReplacedScript.Replace("%CanCraftSolarCell%", CargoManager.CanCraftSolarCell);
                    ValueReplacedScript = ValueReplacedScript.Replace("%CanCraftPowerCell%", CargoManager.CanCraftPowerCell);

                    //Overall Cargo
                    ValueReplacedScript = ValueReplacedScript.Replace("%TotalCargoMass%", CargoManager.TotalCargoMass);
                    ValueReplacedScript = ValueReplacedScript.Replace("%TotalCargoPercentage%", CargoManager.TotalCargoPercentage);

                    ValueReplacedScript = ValueReplacedScript.Replace("%OxygenPercentage%", GasManager.OxygenPercentage);
                    ValueReplacedScript = ValueReplacedScript.Replace("%OxygenCount%", GasManager.OxygenCount);
                    ValueReplacedScript = ValueReplacedScript.Replace("%HydrogenPercentage%", GasManager.HydrogenPercentage);
                    ValueReplacedScript = ValueReplacedScript.Replace("%HydrogenCount%", GasManager.HydrogenCount);

                    ValueReplacedScript = ValueReplacedScript.Replace("%Distance%", DistanceManager.GetDistance(scriptDef.CameraBlockName));
                    ValueReplacedScript = ValueReplacedScript.Replace("%DistanceNextScanTime%", DistanceManager.GetDistanceNextScanTime(scriptDef.CameraBlockName));

                    ValueReplacedScript = ValueReplacedScript.Replace("%DistanceToStop%", ShipManager.DistanceToStop);

                    ThrustManager.Direction = scriptDef.ThrustDirection;
                    ValueReplacedScript = ValueReplacedScript.Replace("%AtmoThrusterInformation%", ThrustManager.AtmoThrusterInformation);
                    ValueReplacedScript = ValueReplacedScript.Replace("%HydroThrusterInformation%", ThrustManager.HydroThrusterInformation);
                    ValueReplacedScript = ValueReplacedScript.Replace("%IonThrusterInformation%", ThrustManager.IonThrusterInformation);
                    ThrustManager.Direction = ThrustDirection.All;

                    ValueReplacedScript = ValueReplacedScript.Replace("%TurretAmmoInformation%", TurretAmmoManager.TurretAmmoInformation);

                    try
                    {
                        scriptDef.Surface.WriteText(ValueReplacedScript);
                    }
                    catch
                    {
                        // Soft-fail
                    }
                }
            }
        }
        #endregion

        #region Power
        public class BatteryData
        {
            public BatteryData(List<IMyBatteryBlock> Batteries)
            {
                const double MegaFactor = 1000000.0;
                TotalInput = Batteries.Sum(b => b.CurrentInput * MegaFactor);
                TotalOutput = Batteries.Sum(b => b.CurrentOutput * MegaFactor);
                TotalStored = Batteries.Sum(b => b.CurrentStoredPower * MegaFactor);
                TotalStorageMax = Batteries.Sum(b => b.MaxStoredPower * MegaFactor);

                TotalPercentage = TotalStored / TotalStorageMax;
                NetInput = TotalInput - TotalOutput;

                const double SecondsInHour = 3600.0;    // Everything is in watt-hours, so we need to multipy by 3600 to get seconds
                if (NetInput > 1.0)
                {
                    // Charging
                    double RemainingCharge = TotalStorageMax - TotalStored;
                    RechargeSeconds = RemainingCharge / NetInput * SecondsInHour;
                }
                else if (NetInput < -1.0)
                {
                    // Discharging
                    RechargeSeconds = TotalStored / NetInput * SecondsInHour;
                }
                else
                {
                    RechargeSeconds = 0.0;
                }
            }

            public double TotalInput { get; set; }
            public double TotalOutput { get; set; }
            public double TotalStored { get; set; }
            public double TotalStorageMax { get; set; }
            public double TotalPercentage { get; set; }

            public double NetInput { get; set; }
            public double RechargeSeconds { get; set; }
        }

        

        public static class PowerManager
        {
            public static BatteryData BatteryData { get; private set; }

            public static double? TotalWindTurbinesOutput { get; private set; }
            public static double? TotalSolarPanelsOutput { get; private set; }
            public static double? TotalReactorOutput { get; private set; }

            private static IMyGridTerminalSystem GridTerminalSystem;
            private static IMyProgrammableBlock Me;

            public static void Init(Program prog)
            {
                GridTerminalSystem = prog.GridTerminalSystem;
                Me = prog.Me;
            }

            private static List<IMyBatteryBlock> Batteries = new List<IMyBatteryBlock>();
            private static List<IMyWindTurbine> WindTurbines = new List<IMyWindTurbine>();
            private static List<IMySolarPanel> SolarPanels = new List<IMySolarPanel>();
            private static List<IMyReactor> Reactors = new List<IMyReactor>();

            private const int UpdateBlockFreq = 500;
            private static void UpdateBlocks()
            {
                if (ShouldUpdate(UpdateBlockFreq))
                {
                    GridTerminalSystem.GetBlocksOfType(Batteries, b => b.CubeGrid == Me.CubeGrid && b.IsFunctional);
                    GridTerminalSystem.GetBlocksOfType(WindTurbines, b => b.CubeGrid == Me.CubeGrid && b.IsFunctional);
                    GridTerminalSystem.GetBlocksOfType(SolarPanels, b => b.IsFunctional);   // Can be on a different grid because of sun-tracking CTC
                    GridTerminalSystem.GetBlocksOfType(Reactors, b => b.CubeGrid == Me.CubeGrid && b.IsFunctional);
                }
            }

            private const int UpdatePowerFreq = 10;
            private static void UpdateBatteries()
            {
                BatteryData = Batteries.Count > 0 ? new BatteryData(Batteries) : null;
            }

            private static void UpdateWindAndSolar()
            {
                TotalWindTurbinesOutput = WindTurbines.Count > 0 ? WindTurbines.Sum(sp => sp.CurrentOutput) : (double?)null;
                TotalSolarPanelsOutput = SolarPanels.Count > 0 ? SolarPanels.Sum(sp => sp.CurrentOutput) : (double?)null;
            }

            private static void UpdateReactors()
            {
                TotalReactorOutput = Reactors.Count > 0 ? Reactors.Sum(r => r.CurrentOutput) : (double?)null;
            }

            public static void Tick()
            {
                UpdateBlocks();

                if (ShouldUpdate(UpdatePowerFreq))
                {
                    UpdateBatteries();
                    UpdateWindAndSolar();
                    UpdateReactors();
                }
            }

            public static string BatteryPercentage
            {
                get
                {
                    if (BatteryData != null)
                    {
                        return $"{BatteryData.TotalPercentage * 100.0:N1}";
                    }

                    return "";
                }
            }

            public static string BatteryTime
            {
                get
                {
                    if (BatteryData != null)
                    {
                        if (BatteryData.NetInput > 1.0 && BatteryData.TotalPercentage < 0.95)
                        {
                            return $"{Conversions.Time(BatteryData.RechargeSeconds)}";
                        }
                        else if (BatteryData.NetInput < -1.0 && BatteryData.TotalPercentage > 0.05)
                        {
                            double DepletionSeconds = Math.Abs(BatteryData.RechargeSeconds);
                            return $"{Conversions.Time(DepletionSeconds)}";
                        }
                    }

                    return "";
                }
            }

            public static string BatteryCount
            {
                get
                {
                    return $"{Batteries.Count}";
                }
            }

            public static string BatteryInput
            {
                get
                {
                    if (BatteryData != null)
                    {
                        return $"{Conversions.Power(BatteryData.TotalInput)}";
                    }

                    return "";
                }
            }

            public static string BatteryOutput
            {
                get
                {
                    if (BatteryData != null)
                    {
                        return $"{Conversions.Power(BatteryData.TotalOutput)}";
                    }

                    return "";
                }
            }

            public static string BatteryNet
            {
                get
                {
                    if (BatteryData != null)
                    {
                        return $"{Conversions.Power(BatteryData.NetInput)}";
                    }

                    return "";
                }
            }

            public static string BatteryStored
            {
                get
                {
                    if (BatteryData != null)
                    {
                        return $"{Conversions.Power(BatteryData.TotalStored)}";
                    }

                    return "";
                }
            }

            public static string BatteryMax
            {
                get
                {
                    if (BatteryData != null)
                    {
                        return $"{Conversions.Power(BatteryData.TotalStorageMax)}";
                    }

                    return "";
                }
            }

            public static string Wind
            {
                get
                {
                    if (TotalWindTurbinesOutput != null)
                    {
                        return $"{Conversions.Power((double)TotalWindTurbinesOutput)}";
                    }

                    return "";
                    
                }
            }

            public static string WindCount
            {
                get
                {
                    return $"{WindTurbines.Count}";
                }
            }

            public static string Solar
            {
                get
                {
                    if (TotalSolarPanelsOutput != null)
                    {
                        return $"{Conversions.Power((double)TotalSolarPanelsOutput)}";
                    }

                    return "";
                }
            }

            public static string SolarCount
            {
                get
                {
                    return $"{SolarPanels.Count}";
                }
            }

            public static string Reactor
            {
                get
                {
                    if (TotalReactorOutput != null)
                    {
                        return $"{Conversions.Power((double)TotalReactorOutput)}";
                    }

                    return "";
                }
            }

            public static string ReactorCount
            {
                get
                {
                    return $"{Reactors.Count}";
                }
            }
        }
        #endregion

        #region Cargo
        public static class CargoManager
        {
            private static IMyGridTerminalSystem GridTerminalSystem;
            private static IMyProgrammableBlock Me;

            private static List<IMyCargoContainer> Cargos = new List<IMyCargoContainer>();
            private static List<IMyShipDrill> Drills = new List<IMyShipDrill>();

            public static Dictionary<string, double> OreToKg { get; private set; } = new Dictionary<string, double>();
            public static Dictionary<string, double> IngotToKg { get; private set; } = new Dictionary<string, double>();
            public static Dictionary<string, double> ComponentToKg { get; private set; } = new Dictionary<string, double>();

            public static double? TotalMass { get; private set; }
            public static double? TotalVolume { get; private set; }
            public static double? TotalMaxVolume { get; private set; }
            public static double? TotalVolumePercentage { get; private set; }

            public static double? DrillMass { get; private set; }
            public static double? DrillVolume { get; private set; }
            public static double? DrillMaxVolume { get; private set; }
            public static double? DrillVolumePercentage { get; private set; }

            public class ResourceToAmount
            {
                public string Name { get; set; }
                public double Amount { get; set; }
            };

            public static Dictionary<string, List<ResourceToAmount>> ComponentToPrerequisites = new Dictionary<string, List<ResourceToAmount>>()
            {
                { 
                    "Construction", new List<ResourceToAmount>() 
                    { 
                        new ResourceToAmount { Name = "Iron", Amount = 8 } 
                    } 
                },
                { 
                    "MetalGrid", new List<ResourceToAmount>() 
                    { 
                        new ResourceToAmount { Name = "Iron", Amount = 12 },
                        new ResourceToAmount { Name = "Nickel", Amount = 5 },
                        new ResourceToAmount { Name = "Cobalt", Amount = 3 },
                    } 
                },
                { 
                    "InteriorPlate", new List<ResourceToAmount>()
                    {
                        new ResourceToAmount { Name = "Iron", Amount = 3 }
                    }
                },
                { 
                    "SteelPlate", new List<ResourceToAmount>()
                    {
                        new ResourceToAmount { Name = "Iron", Amount = 21 }
                    }
                },
                { 
                    "Girder", new List<ResourceToAmount>()
                    {
                        new ResourceToAmount { Name = "Iron", Amount = 6 }
                    }
                },
                { 
                    "SmallTube", new List<ResourceToAmount>()
                    {
                        new ResourceToAmount { Name = "Iron", Amount = 5 }
                    }
                },
                { 
                    "LargeTube", new List<ResourceToAmount>()
                    {
                        new ResourceToAmount { Name = "Iron", Amount = 30 }
                    }
                },
                { 
                    "Motor", new List<ResourceToAmount>()
                    {
                        new ResourceToAmount { Name = "Iron", Amount = 20 },
                        new ResourceToAmount { Name = "Nickel", Amount = 5 },
                    }
                },
                { 
                    "Display", new List<ResourceToAmount>()
                    {
                        new ResourceToAmount { Name = "Iron", Amount = 1 },
                        new ResourceToAmount { Name = "Silicon", Amount = 5 },
                    }
                },
                { 
                    "BulletproofGlass", new List<ResourceToAmount>()
                    {
                        new ResourceToAmount { Name = "Silicon", Amount = 15 }
                    }
                },
                { 
                    "Superconductor", new List<ResourceToAmount>()
                    {
                        new ResourceToAmount { Name = "Iron", Amount = 10 },
                        new ResourceToAmount { Name = "Gold", Amount = 2 },
                    }
                },
                { 
                    "Computer", new List<ResourceToAmount>()
                    {
                        new ResourceToAmount { Name = "Iron", Amount = 0.5 },
                        new ResourceToAmount { Name = "Silicon", Amount = 0.2 },
                    }
                },
                { 
                    "SolarCell", new List<ResourceToAmount>()
                    {
                        new ResourceToAmount { Name = "Nickel", Amount = 3 },
                        new ResourceToAmount { Name = "Silicon", Amount = 6 },
                    }
                },
                { 
                    "PowerCell", new List<ResourceToAmount>()
                    {
                        new ResourceToAmount { Name = "Iron", Amount = 10 },
                        new ResourceToAmount { Name = "Nickel", Amount = 2 },
                        new ResourceToAmount { Name = "Silicon", Amount = 1 },
                    }
                },
            };

            public static void Init(Program prog)
            {
                GridTerminalSystem = prog.GridTerminalSystem;
                Me = prog.Me;
            }

            private const int UpdateBlockFreq = 500;
            private static void UpdateBlocks()
            {
                if (ShouldUpdate(UpdateBlockFreq))
                {
                    GridTerminalSystem.GetBlocksOfType(Cargos, b => b.CubeGrid == Me.CubeGrid && b.IsFunctional && b.IsWorking);
                    GridTerminalSystem.GetBlocksOfType(Drills, b => b.IsFunctional);
                }
            }

            private const int UpdateCargoFreq = 30;
            public static void Tick()
            {
                UpdateBlocks();

                if (ShouldUpdate(UpdateCargoFreq))
                {
                    UpdateVolumeAndMass();
                    RefreshInventoryItems();
                }
            }

            private static void UpdateVolumeAndMass()
            {
                if (Cargos.Count > 0)
                {
                    TotalMass = Cargos.Sum(c => (double)c.GetInventory().CurrentMass);
                    TotalVolume = Cargos.Sum(c => (double)c.GetInventory().CurrentVolume);
                    TotalMaxVolume = Cargos.Sum(c => (double)c.GetInventory().MaxVolume);
                    TotalVolumePercentage = TotalVolume / TotalMaxVolume;

                    DrillMass = Drills.Sum(c => (double)c.GetInventory().CurrentMass);
                    DrillVolume = Drills.Sum(c => (double)c.GetInventory().CurrentVolume);
                    DrillMaxVolume = Drills.Sum(c => (double)c.GetInventory().MaxVolume);
                    DrillVolumePercentage = TotalVolume / TotalMaxVolume;
                }
                else
                {
                    TotalMass = null;
                    TotalVolume = null;
                    TotalMaxVolume = null;
                    TotalVolumePercentage = null;

                    DrillMass = null;
                    DrillVolume = null;
                    DrillMaxVolume = null;
                    DrillVolumePercentage = null;
                }
            }

            private static string GetDisplayName(string Name, bool IsIngot = false)
            {
                string ObjectType = IsIngot ? "Ingot" : "Ore";
                if (Name == "Ice")
                {
                    ObjectType = "";
                }
                else if (Name == "Stone")
                {
                    Name = IsIngot ? "Gravel" : "Stone";
                    ObjectType = "";
                }
                else if (Name == "Silicon" && IsIngot)
                {
                    ObjectType = "Wafer";
                }
                else if (Name == "Magnesium" && IsIngot)
                {
                    ObjectType = "Powder";
                }

                return $"{Name} {ObjectType}";
            }

            private static List<string> ValidComponents = new List<string>()
            {
                "Construction", "MetalGrid", "InteriorPlate", "SteelPlate", "Girder", 
                "SmallTube", "LargeTube", "Motor", "Display", "BulletproofGlass",
                "Superconductor", "Computer", "SolarCell", "PowerCell"
            };

            private static void RefreshInventoryItems()
            {
                OreToKg.Clear();
                IngotToKg.Clear();
                ComponentToKg.Clear();

                var blocks = new List<IMyTerminalBlock>();
                GridTerminalSystem.GetBlocks(blocks);
                blocks = blocks
                    .Where(c => c.CubeGrid == Me.CubeGrid && c.IsFunctional && c.IsWorking && c.InventoryCount > 0)
                    .ToList();

                var inventories = new List<IMyInventory>();
                blocks.ForEach(c =>
                {
                    for (int InventoryIdx = 0; InventoryIdx < c.InventoryCount; ++InventoryIdx)
                    {
                        inventories.Add(c.GetInventory(InventoryIdx));
                    }
                });

                var allItems = new List<MyInventoryItem>();
                inventories.ForEach(i =>
                {
                    var inventoryItems = new List<MyInventoryItem>();
                    i.GetItems(inventoryItems);
                    allItems.AddRange(inventoryItems);
                });

                foreach (var item in allItems)
                {
                    if (item.Type.ToString().Contains("Ore"))
                    {
                        var name = GetDisplayName(item.Type.SubtypeId, IsIngot: false);
                        double amount = (double)item.Amount;
                        if (!OreToKg.ContainsKey(name))
                        {
                            OreToKg.Add(name, amount);
                        }
                        else
                        {
                            OreToKg[name] += amount;
                        }
                    }

                    if (item.Type.ToString().Contains("Ingot"))
                    {
                        var name = GetDisplayName(item.Type.SubtypeId, IsIngot: true);
                        double amount = (double)item.Amount;
                        if (!IngotToKg.ContainsKey(name))
                        {
                            IngotToKg.Add(name, amount);
                        }
                        else
                        {
                            IngotToKg[name] += amount;
                        }
                    }

                    if (item.Type.ToString().Contains("Component") && ValidComponents.Contains(item.Type.SubtypeId))
                    {
                        var name = item.Type.SubtypeId;
                        double amount = (double)item.Amount;

                        if (!ComponentToKg.ContainsKey(name))
                        {
                            ComponentToKg.Add(name, amount);
                        }
                        else
                        {
                            ComponentToKg[name] += amount;
                        }
                    }
                }

                // This isn't strictly "safe" (dictionaries are unordered) but the likelihood of this not working is low
                OreToKg = OreToKg.OrderByDescending(kvp => kvp.Value).ToDictionary(kvp => kvp.Key, kvp => kvp.Value);
                IngotToKg = IngotToKg.OrderByDescending(kvp => kvp.Value).ToDictionary(kvp => kvp.Key, kvp => kvp.Value);
                ComponentToKg = ComponentToKg.OrderByDescending(kvp => kvp.Value).ToDictionary(kvp => kvp.Key, kvp => kvp.Value);
            }

            public static string OreInformation
            {
                get
                {
                    var sb = new StringBuilder();
                    foreach (var kvp in OreToKg)
                    {
                        sb.AppendLine($"{kvp.Key}: {Conversions.Weight(kvp.Value)}");
                    }
                    return sb.ToString().TrimEnd();
                }
            }

            public static double GetOreWeight(string DisplayName)
            {
                double val = 0.0;
                OreToKg.TryGetValue(GetDisplayName(DisplayName, IsIngot: false), out val);
                return val;
            }

            public static string GetOreWeightString(string DisplayName)
            {
                double val = GetOreWeight(DisplayName);
                return val > 0.0 ? Conversions.Weight(val) : string.Empty;
            }

            public static string Ice
            {
                get { return GetOreWeightString("Ice"); }
            }

            public static string Stone
            {
                get { return GetOreWeightString("Stone"); }
            }

            public static string Scrap
            {
                get { return GetOreWeightString("Scrap"); }
            }

            public static string IronOre
            {
                get { return GetOreWeightString("Iron Ore"); }
            }

            public static string NickelOre
            {
                get { return GetOreWeightString("Nickel Ore"); }
            }

            public static string CobaltOre
            {
                get { return GetOreWeightString("Cobalt Ore"); }
            }

            public static string MagnesiumOre
            {
                get { return GetOreWeightString("Magnesium Ore"); }
            }

            public static string SiliconOre
            {
                get { return GetOreWeightString("Silicon Ore"); }
            }

            public static string SilverOre
            {
                get { return GetOreWeightString("Silver Ore"); }
            }

            public static string GoldOre
            {
                get { return GetOreWeightString("Gold Ore"); }
            }

            public static string PlatinumOre
            {
                get { return GetOreWeightString("Platinum Ore"); }
            }

            public static string UraniumOre
            {
                get { return GetOreWeightString("Uranium Ore"); }
            }

            public static string IngotInformation
            {
                get
                {
                    var sb = new StringBuilder();
                    foreach (var kvp in IngotToKg)
                    {
                        sb.AppendLine($"{kvp.Key}: {Conversions.Weight(kvp.Value)}");
                    }
                    return sb.ToString().TrimEnd();
                }
            }

            public static double GetIngotWeight(string DisplayName)
            {
                double val;
                var disp = GetDisplayName(DisplayName, IsIngot: true);
                bool success = IngotToKg.TryGetValue(disp, out val);

                return val;
            }

            public static string GetIngotWeightString(string DisplayName)
            {
                double val = GetIngotWeight(DisplayName);
                return val > 0.0 ? Conversions.Weight(val) : string.Empty;
            }

            public static string Gravel
            {
                get { return GetIngotWeightString("Stone"); }
            }

            public static string IronIngot
            {
                get { return GetIngotWeightString("Iron"); }
            }

            public static string NickelIngot
            {
                get { return GetIngotWeightString("Nickel"); }
            }

            public static string CobaltIngot
            {
                get { return GetIngotWeightString("Cobalt"); }
            }

            public static string MagnesiumIngot
            {
                get { return GetIngotWeightString("Magnesium"); }
            }

            public static string SiliconWafer
            {
                get { return GetIngotWeightString("Silicon"); }
            }

            public static string SilverIngot
            {
                get { return GetIngotWeightString("Silver"); }
            }

            public static string GoldIngot
            {
                get { return GetIngotWeightString("Gold"); }
            }

            public static string PlatinumIngot
            {
                get { return GetIngotWeightString("Platinum"); }
            }

            public static string UraniumIngot
            {
                get { return GetIngotWeightString("Uranium"); }
            }

            public static string ComponentInformation
            {
                get
                {
                    var sb = new StringBuilder();
                    foreach (var kvp in ComponentToKg)
                    {
                        sb.AppendLine($"{kvp.Key}: {kvp.Value}");
                    }
                    return sb.ToString().TrimEnd();
                }
            }

            public static double GetComponentCount(string DisplayName)
            {
                double val = 0.0;
                ComponentToKg.TryGetValue(DisplayName, out val);
                return val;
            }

            public static string GetComponentCountString(string DisplayName)
            {
                double val = GetComponentCount(DisplayName);
                return $"{val}";
            }

            public static string ConstructionComponent
            {
                get { return GetComponentCountString("Construction"); }
            }

            public static string MetalGrid
            {
                get { return GetComponentCountString("MetalGrid"); }
            }

            public static string InteriorPlate
            {
                get { return GetComponentCountString("InteriorPlate"); }
            }

            public static string SteelPlate
            {
                get { return GetComponentCountString("SteelPlate"); }
            }

            public static string Girder
            {
                get { return GetComponentCountString("Girder"); }
            }

            public static string SmallTube
            {
                get { return GetComponentCountString("SmallTube"); }
            }

            public static string LargeTube
            {
                get { return GetComponentCountString("LargeTube"); }
            }

            public static string Motor
            {
                get { return GetComponentCountString("Motor"); }
            }

            public static string Display
            {
                get { return GetComponentCountString("Display"); }
            }

            public static string BulletproofGlass
            {
                get { return GetComponentCountString("BulletproofGlass"); }
            }

            public static string Superconductor
            {
                get { return GetComponentCountString("Superconductor"); }
            }

            public static string Computer
            {
                get { return GetComponentCountString("Computer"); }
            }

            public static string SolarCell
            {
                get { return GetComponentCountString("SolarCell"); }
            }

            public static string PowerCell
            {
                get { return GetComponentCountString("PowerCell"); }
            }

            public static string CanCraftInformation
            {
                get
                {
                    var sb = new StringBuilder();
                    foreach (var validComponent in ValidComponents)
                    {
                        var canCraft = GetCanCraft(validComponent);
                        if (canCraft > 0)
                        {
                            sb.AppendLine($"{validComponent}: {canCraft}");
                        }
                    }
                    return sb.ToString().TrimEnd();
                }
            }

            private const double ASSEMBLER_EFFICIENCY = 3; 

            public static int GetCanCraft(string DisplayName)
            {
                if (ComponentToPrerequisites.ContainsKey(DisplayName))
                {
                    var prereqs = ComponentToPrerequisites[DisplayName];

                    int MinAmt = 0;
                    foreach (var prereq in prereqs)
                    {   
                        var TotalAvailableAmount = GetIngotWeight(prereq.Name);
                        var MaximumAmount = TotalAvailableAmount / prereq.Amount * ASSEMBLER_EFFICIENCY;
                        if (MinAmt == 0 || MaximumAmount < MinAmt)
                        {
                            MinAmt = (int)MaximumAmount;
                        }
                    }

                    return MinAmt;
                }

                return 0;
            }

            public static string GetCanCraftString(string DisplayName)
            {
                var CanCraftValue = GetCanCraft(DisplayName);
                return $"{CanCraftValue}";
            }

            public static string CanCraftConstruction
            {
                get { return GetCanCraftString("Construction"); }
            }

            public static string CanCraftMetalGrid
            {
                get { return GetCanCraftString("MetalGrid"); }
            }

            public static string CanCraftInteriorPlate
            {
                get { return GetCanCraftString("InteriorPlate"); }
            }

            public static string CanCraftSteelPlate
            {
                get { return GetCanCraftString("SteelPlate"); }
            }

            public static string CanCraftGirder
            {
                get { return GetCanCraftString("Girder"); }
            }

            public static string CanCraftSmallTube
            {
                get { return GetCanCraftString("SmallTube"); }
            }

            public static string CanCraftLargeTube
            {
                get { return GetCanCraftString("LargeTube"); }
            }

            public static string CanCraftMotor
            {
                get { return GetCanCraftString("Motor"); }
            }

            public static string CanCraftDisplay
            {
                get { return GetCanCraftString("Display"); }
            }

            public static string CanCraftBulletproofGlass
            {
                get { return GetCanCraftString("BulletproofGlass"); }
            }

            public static string CanCraftSuperconductor
            {
                get { return GetCanCraftString("Superconductor"); }
            }

            public static string CanCraftComputer
            {
                get { return GetCanCraftString("Computer"); }
            }

            public static string CanCraftSolarCell
            {
                get { return GetCanCraftString("SolarCell"); }
            }

            public static string CanCraftPowerCell
            {
                get { return GetCanCraftString("PowerCell"); }
            }

            public static string TotalCargoMass
            {
                get
                {
                    if (TotalMass != null)
                    {
                        return $"{Conversions.Weight((double)TotalMass)}";
                    }

                    return "";
                }
            }

            public static string TotalCargoPercentage
            {
                get
                {
                    if (TotalVolumePercentage != null)
                    {
                        return $"{TotalVolumePercentage * 100.0:N1}%";
                    }

                    return "";
                }
            }

            public static string DrillsMass
            {
                get
                {
                    if (DrillMass != null)
                    {
                        return $"{Conversions.Weight((double)DrillMass)}";
                    }

                    return "";
                }
            }

            public static string DrillsVolume
            {
                get
                {
                    if (DrillVolume != null)
                    {
                        return $"{DrillVolume:N1}";
                    }

                    return "";
                }
            }

            public static string DrillsMaxVolume
            {
                get
                {
                    if (DrillMaxVolume != null)
                    {
                        return $"{DrillMaxVolume:N1}";
                    }

                    return "";
                }
            }

            public static string DrillsPercentage
            {
                get
                {
                    if (DrillVolumePercentage != null)
                    {
                        return $"{DrillVolumePercentage:N1}";
                    }

                    return "";
                }
            }
        }
        #endregion

        #region AirlockManager
        public static class AirlockManager
        {
            class DoorSet
            {
                public IMyDoor DoorOne;
                public IMyDoor DoorTwo;

                public bool DoorOnePendingOpen;
                public bool DoorTwoPendingOpen;

                public DoorStatus DoorOneStatus;
                public DoorStatus DoorTwoStatus;

                public int CloseTickCounter = -1;
            }

            private static List<DoorSet> DoorSets = new List<DoorSet>();

            public static void Init(Program prog, List<string> AirlockDoorSets)
            {
                if (AirlockDoorSets == null)
                    return;

                if (AirlockDoorSets.Count % 2 != 0)
                    throw new Exception("Uneven set of AirlockDoors");
                
                while (AirlockDoorSets.Count > 0)
                {
                    var DoorOneName = AirlockDoorSets[0];
                    var DoorTwoName = AirlockDoorSets[1];
                    AirlockDoorSets = AirlockDoorSets.Skip(2).ToList();

                    DoorSet DoorSet = new DoorSet
                    {
                        DoorOne = prog.GridTerminalSystem.GetBlockWithName(DoorOneName) as IMyDoor,
                        DoorTwo = prog.GridTerminalSystem.GetBlockWithName(DoorTwoName) as IMyDoor,
                    };

                    if (DoorSet.DoorOne != null && DoorSet.DoorTwo != null)
                    {
                        DoorSet.DoorOneStatus = DoorSet.DoorOne.Status;
                        DoorSet.DoorTwoStatus = DoorSet.DoorTwo.Status;

                        DoorSets.Add(DoorSet);
                    }
                }
            }

            // Anything lower than this appears to cause airlock failures in general
            private const int NumTicksToWait = 150;

            private static void CloseDoorOne(DoorSet DoorSet)
            {
                if (DoorSet.DoorOne.Status != DoorStatus.Closed && DoorSet.DoorOne.Status != DoorStatus.Closing)
                {
                    DoorSet.DoorOne.CloseDoor();
                }
            }

            private static void CloseDoorTwo(DoorSet DoorSet)
            {
                if (DoorSet.DoorTwo.Status != DoorStatus.Closed && DoorSet.DoorTwo.Status != DoorStatus.Closing)
                {
                    DoorSet.DoorTwo.CloseDoor();
                }
            }

            public static void Tick()
            {
                for (int i = 0; i < DoorSets.Count; i++)
                {
                    var DoorSet = DoorSets[i];
                    var DoorOne = DoorSet.DoorOne;
                    var DoorTwo = DoorSet.DoorTwo;

                    bool DoorOneChanged = DoorSet.DoorOneStatus != DoorOne.Status;
                    bool DoorTwoChanged = DoorSet.DoorTwoStatus != DoorTwo.Status;

                    bool DoorOneOpen = DoorSet.DoorOneStatus != DoorStatus.Closed;
                    bool DoorTwoOpen = DoorSet.DoorTwoStatus != DoorStatus.Closed;

                    bool DoorOneInFlight = DoorSet.DoorOneStatus == DoorStatus.Opening || DoorSet.DoorOneStatus == DoorStatus.Closing;
                    bool DoorTwoInFlight = DoorSet.DoorTwoStatus == DoorStatus.Opening || DoorSet.DoorTwoStatus == DoorStatus.Closing;

                    if (DoorSet.DoorOnePendingOpen)
                    {
                        // Keep Forcing door two to close
                        if (DoorSet.DoorTwoStatus != DoorStatus.Closed)
                        {
                            CloseDoorTwo(DoorSet);
                        }
                        else if (DoorSet.DoorOneStatus != DoorStatus.Open)
                        {
                            CloseDoorTwo(DoorSet);

                            if (DoorSet.CloseTickCounter == -1)
                            {
                                // We just finished closing, so set the CloseTickCounter
                                DoorSet.CloseTickCounter = NumTicksToWait;
                            }
                            else if (DoorSet.CloseTickCounter == 0)
                            {
                                DoorOne.OpenDoor();
                                DoorSet.CloseTickCounter = -1;
                            }
                            else
                            {
                                DoorSet.CloseTickCounter--;
                            }
                        }
                        else
                        {
                            // DoorOne is open, remove pending open
                            DoorSet.DoorOnePendingOpen = false;
                        }
                    }
                    else if (DoorSet.DoorTwoPendingOpen)
                    {
                        // Keep Forcing door one to close
                        if (DoorSet.DoorOneStatus != DoorStatus.Closed)
                        {
                            CloseDoorOne(DoorSet);
                        }
                        else if (DoorSet.DoorTwoStatus != DoorStatus.Open)
                        {
                            CloseDoorOne(DoorSet);

                            if (DoorSet.CloseTickCounter == -1)
                            {
                                // We just finished closing, so set the CloseTickCounter
                                DoorSet.CloseTickCounter = NumTicksToWait;
                            }
                            else if (DoorSet.CloseTickCounter == 0)
                            {
                                DoorTwo.OpenDoor();
                                DoorSet.CloseTickCounter = -1;
                            }
                            else
                            {
                                DoorSet.CloseTickCounter--;
                            }
                        }
                        else
                        {
                            // DoorOne is open, remove pending open
                            DoorSet.DoorTwoPendingOpen = false;
                        }
                    }
                    else if (DoorOneOpen && DoorTwoOpen)
                    {
                        if (DoorOneInFlight)
                        {
                            CloseDoorOne(DoorSet);
                            DoorSet.DoorOnePendingOpen = true;
                        }
                        else if (DoorTwoInFlight)
                        {
                            CloseDoorTwo(DoorSet);
                            DoorSet.DoorTwoPendingOpen = true;
                        }
                        else
                        {
                            // They're both fully open
                            CloseDoorOne(DoorSet);
                            CloseDoorTwo(DoorSet);
                        }
                    }


                    // Update status
                    DoorSet.DoorOneStatus = DoorOne.Status;
                    DoorSet.DoorTwoStatus = DoorTwo.Status;

                    DoorSets[i] = DoorSet;
                }
            }
        }
        #endregion

        #region Gas
        public static class GasManager
        {
            private static IMyGridTerminalSystem GridTerminalSystem;
            private static IMyProgrammableBlock Me;

            private static List<IMyGasTank> OxygenTanks = new List<IMyGasTank>();
            private static List<IMyGasTank> HydrogenTanks = new List<IMyGasTank>();

            public static double? OxygenCurrentVolume { get; private set; }
            public static double? OxygenMaxVolume { get; private set; }
            public static double? OxygenStoredPercentage { get; private set; }

            public static double? HydrogenCurrentVolume { get; private set; }
            public static double? HydrogenMaxVolume { get; private set; }
            public static double? HydrogenStoredPercentage { get; private set; }

            public static void Init(Program prog)
            {
                GridTerminalSystem = prog.GridTerminalSystem;
                Me = prog.Me;
            }

            public static void Tick()
            {
                UpdateBlocks();
                UpdateGas();
            }
            private const int UpdateBlockFreq = 500;
            private static void UpdateBlocks()
            {
                if (ShouldUpdate(UpdateBlockFreq))
                {
                    var tanks = new List<IMyGasTank>();
                    GridTerminalSystem.GetBlocksOfType(tanks, b => b.CubeGrid == Me.CubeGrid);

                    OxygenTanks = tanks.Where(t => t.BlockDefinition.TypeIdString.ToLower().Contains("oxygen")).ToList();
                    HydrogenTanks = tanks.Where(t => t.BlockDefinition.SubtypeId.ToLower().Contains("hydrogen")).ToList();
                }
            }

            private const int UpdateGasFreq = 100;
            private static void UpdateGas()
            {
                if (ShouldUpdate(UpdateGasFreq))
                {
                    if (OxygenTanks.Count > 0)
                    {
                        OxygenCurrentVolume = OxygenTanks.Sum(o => o.FilledRatio * o.Capacity);
                        OxygenMaxVolume = OxygenTanks.Sum(o => o.Capacity);
                        OxygenStoredPercentage = OxygenCurrentVolume / OxygenMaxVolume;
                    }
                    else
                    {
                        OxygenCurrentVolume = null;
                        OxygenMaxVolume = null;
                        OxygenStoredPercentage = null;
                    }

                    if (HydrogenTanks.Count > 0)
                    {
                        HydrogenCurrentVolume = HydrogenTanks.Sum(o => o.FilledRatio * o.Capacity);
                        HydrogenMaxVolume = HydrogenTanks.Sum(o => o.Capacity);
                        HydrogenStoredPercentage = HydrogenCurrentVolume / HydrogenMaxVolume;
                    }
                    else
                    {
                        HydrogenCurrentVolume = null;
                        HydrogenMaxVolume = null;
                        HydrogenStoredPercentage = null;
                    }
                }
            }

            public static string OxygenPercentage
            {
                get
                {
                    if (OxygenTanks.Count > 0)
                    {
                        return $"{OxygenStoredPercentage * 100.0:N1}";
                    }

                    return "";
                }
            }

            public static string OxygenCount
            {
                get
                {
                    return $"{OxygenTanks.Count}";
                }
            }

            public static string HydrogenPercentage
            {
                get
                {
                    if (HydrogenTanks.Count > 0)
                    {
                        return $"{HydrogenStoredPercentage * 100.0:N1}";
                    }

                    return "";
                }
            }

            public static string HydrogenCount
            {
                get
                {
                    return $"{HydrogenTanks.Count}";
                }
            }
        }
        #endregion

        #region Distance
        public class DistanceInformation
        {
            public Vector3D? PrevHitPosition { get; set; }
            public double? SecondsUntilNextScan { get; set; } = 2.0;
            public double? Distance { get; set; }
        }

        public static class DistanceManager
        {
            private static IMyGridTerminalSystem GridTerminalSystem;
            private static IMyProgrammableBlock Me;

            private static Dictionary<string, IMyCameraBlock> CameraBlockNamesToCameras = new Dictionary<string, IMyCameraBlock>();
            private static Dictionary<string, DistanceInformation> CameraBlockNamesToDistanceInformation = new Dictionary<string, DistanceInformation>();

            public static void Init(Program prog)
            {
                GridTerminalSystem = prog.GridTerminalSystem;
                Me = prog.Me;
            }

            public static void Tick(string cameraBlockName)
            {
                UpdateBlocks(cameraBlockName);
                UpdateDistance(cameraBlockName);
            }

            private const int UpdateBlockFreq = 500;
            private static void UpdateBlocks(string cameraBlockName)
            {
                cameraBlockName = cameraBlockName != null ? cameraBlockName : "";

                bool ValidCameraBlockName = !string.IsNullOrEmpty(cameraBlockName);
                if (ValidCameraBlockName)
                {
                    bool NewCameraBlockName = !CameraBlockNamesToCameras.ContainsKey(cameraBlockName);
                    if (NewCameraBlockName)
                    {
                        List<IMyTerminalBlock> terminalBlocks = new List<IMyTerminalBlock>();
                        GridTerminalSystem.SearchBlocksOfName(cameraBlockName, terminalBlocks, tb => tb is IMyCameraBlock);
                        CameraBlockNamesToCameras.Add(cameraBlockName, terminalBlocks.FirstOrDefault() as IMyCameraBlock);
                        CameraBlockNamesToDistanceInformation.Add(cameraBlockName, new DistanceInformation());
                    }

                    if (ShouldUpdate(UpdateBlockFreq))
                    {
                        List<IMyTerminalBlock> terminalBlocks = new List<IMyTerminalBlock>();

                        var keys = CameraBlockNamesToCameras.Keys.ToList();
                        foreach (var key in keys)
                        {
                            terminalBlocks.Clear();
                            GridTerminalSystem.SearchBlocksOfName(key, terminalBlocks, tb => tb is IMyCameraBlock);
                            var cameraBlock = terminalBlocks.FirstOrDefault() as IMyCameraBlock;
                            if (cameraBlock == null)
                            {
                                CameraBlockNamesToCameras.Remove(key);
                                CameraBlockNamesToDistanceInformation.Remove(key);
                            }
                            else
                            {
                                cameraBlock.EnableRaycast = true;
                                CameraBlockNamesToCameras[cameraBlockName] = cameraBlock;
                            }
                        }
                    }
                }
            }

            private static void UpdateDistance(string cameraBlockName)
            {
                cameraBlockName = cameraBlockName != null ? cameraBlockName : "";

                IMyCameraBlock Camera;
                if (CameraBlockNamesToCameras.TryGetValue(cameraBlockName, out Camera))
                {
                    // Every frame
                    const double ScanDistance = 20000.0;

                    var DistanceInfo = CameraBlockNamesToDistanceInformation[cameraBlockName];
                    DistanceInfo.SecondsUntilNextScan -= 0.016666666;
                    
                    // For slight variabilities
                    DistanceInfo.SecondsUntilNextScan = DistanceInfo.SecondsUntilNextScan <= 0.0 ? (Camera.TimeUntilScan(ScanDistance) / 1000.0) : DistanceInfo.SecondsUntilNextScan;

                    if (DistanceInfo.SecondsUntilNextScan <= 0.0)
                    {
                        var ScanResults = Camera.Raycast(ScanDistance);
                        DistanceInfo.PrevHitPosition = ScanResults.HitPosition;

                        var MsUntilNextScan = Camera.TimeUntilScan(ScanDistance);
                        DistanceInfo.SecondsUntilNextScan = MsUntilNextScan > 200.0 ? MsUntilNextScan / 1000.0 : 2.0;

                    }

                    if (DistanceInfo.PrevHitPosition != null)
                    {
                        DistanceInfo.Distance = Vector3D.Distance(Camera.GetPosition(), (Vector3D)DistanceInfo.PrevHitPosition);
                    }
                    else
                    {
                        DistanceInfo.Distance = null;
                    }
                }
            }

            public static string GetDistance(string cameraBlockName)
            {
                cameraBlockName = cameraBlockName != null ? cameraBlockName : "";

                IMyCameraBlock Camera;
                if (CameraBlockNamesToCameras.TryGetValue(cameraBlockName, out Camera))
                {
                    var DistanceInfo = CameraBlockNamesToDistanceInformation[cameraBlockName];
                    if (DistanceInfo.Distance != null)
                    {
                        return $"{Conversions.Distance((double)DistanceInfo.Distance)}";
                    }
                    else
                    {
                        return "[N/A]";
                    }
                }

                return "";
            }

            public static string GetDistanceNextScanTime(string cameraBlockName)
            {
                cameraBlockName = cameraBlockName != null ? cameraBlockName : "";

                IMyCameraBlock Camera;
                if (CameraBlockNamesToCameras.TryGetValue(cameraBlockName, out Camera))
                {
                    var DistanceInfo = CameraBlockNamesToDistanceInformation[cameraBlockName];
                    return $"{Conversions.Time((double)DistanceInfo.SecondsUntilNextScan, true)}";
                }

                return "";
            }
        }
        #endregion

        #region Thrust
        public enum ThrustDirection
        {
            Up = 0,
            Down = 1,
            Forward = 2,
            Backward = 3,
            Left = 4,
            Right = 5,

            All = 100
        }

        public static class ThrustManager
        {
            private static IMyGridTerminalSystem GridTerminalSystem;
            private static IMyProgrammableBlock Me;
            public static ThrustDirection Direction;

            public static Dictionary<string, ThrustDirection> DirectionStringToThrustDirection = new Dictionary<string, ThrustDirection>
            {
                { "up", ThrustDirection.Up },
                { "down", ThrustDirection.Down },
                { "forward", ThrustDirection.Forward },
                { "backward", ThrustDirection.Backward },
                { "back", ThrustDirection.Backward },
                { "left", ThrustDirection.Left },
                { "right", ThrustDirection.Right },
            };

            private static List<IMyThrust> AtmoThrusters = new List<IMyThrust>();
            private static List<IMyThrust> HydroThrusters = new List<IMyThrust>();
            private static List<IMyThrust> IonThrusters = new List<IMyThrust>();

            public static ThrustValues AtmoThrustValues = new ThrustValues();
            public static ThrustValues HydroThrustValues = new ThrustValues();
            public static ThrustValues IonThrustValues = new ThrustValues();

            public static ThrustValues ActiveThrustValues = new ThrustValues();

            public static bool IsUnderPlanetGravity { get; private set; } = false;
            public static double Elevation { get; private set; } = 0.0;
            public static double Gravity { get; private set; } = 9.81;
            public static double ShipPhysicalMass { get; private set; } = 1.0;
            public static double ShipTotalMass { get; private set; } = 1.0;
            public static double ShipSpeed { get; private set; } = 0.0;

            public static bool InformationAvailable { get; set; }

            public static void Init(Program prog, string direction = null)
            {
                GridTerminalSystem = prog.GridTerminalSystem;
                Me = prog.Me;
                direction = direction != null ? direction.ToLower() : "";

                if (!DirectionStringToThrustDirection.Keys.Contains(direction))
                {
                    Direction = ThrustDirection.All;
                }
                else
                {
                    Direction = DirectionStringToThrustDirection[direction];
                }
            }

            public class ThrustValues
            {
                public double Up = 0.0;
                public double Down = 0.0;
                public double Forward = 0.0;
                public double Backward = 0.0;
                public double Left = 0.0;
                public double Right = 0.0;

                public double this[int i]
                {
                    get
                    {
                        switch (i)
                        {
                            case 0: return Up;
                            case 1: return Down;
                            case 2: return Forward;
                            case 3: return Backward;
                            case 4: return Left;
                            case 5: return Right;
                        }

                        return -1;
                    }
                }

                public static string GetNameFromIndex(int index)
                {
                    switch (index)
                    {
                        case 0: return "Up:       ";
                        case 1: return "Down:     ";
                        case 2: return "Forward:  ";
                        case 3: return "Backward: ";
                        case 4: return "Left:     ";
                        case 5: return "Right:    ";
                    }

                    return "ERROR-UNKNOWN";
                }
            }

            private static void GetThrustDirectionNewtonValues(List<IMyThrust> thrusters, out ThrustValues thrustValues)
            {
                thrustValues = new ThrustValues();
                thrustValues.Up = 0.0;
                thrustValues.Down = 0.0;
                thrustValues.Forward = 0.0;
                thrustValues.Backward = 0.0;
                thrustValues.Left = 0.0;
                thrustValues.Right = 0.0;

                // Sum up all the thruster values
                foreach (var thruster in thrusters)
                {
                    if (thruster.GridThrustDirection == Vector3I.Up)
                    {
                        thrustValues.Up += thruster.MaxThrust;
                    }
                    else if (thruster.GridThrustDirection == Vector3I.Down)
                    {
                        thrustValues.Down += thruster.MaxThrust;
                    }
                    else if (thruster.GridThrustDirection == Vector3I.Forward)
                    {
                        thrustValues.Forward = thruster.MaxThrust;
                    }
                    else if (thruster.GridThrustDirection == Vector3I.Backward)
                    {
                        thrustValues.Backward += thruster.MaxThrust;
                    }
                    else if (thruster.GridThrustDirection == Vector3I.Left)
                    {
                        thrustValues.Left += thruster.MaxThrust;
                    }
                    else if (thruster.GridThrustDirection == Vector3I.Right)
                    {
                        thrustValues.Right += thruster.MaxThrust;
                    }
                }
            }

            private const int UpdateBlockFreq = 500;
            private static void UpdateBlocks()
            {
                if (ShouldUpdate(UpdateBlockFreq))
                {
                    var thrusters = new List<IMyThrust>();
                    GridTerminalSystem.GetBlocksOfType(thrusters, t => t.CubeGrid == Me.CubeGrid && t.IsFunctional);

                    AtmoThrusters = thrusters.Where(t => t.BlockDefinition.SubtypeId.Contains("AtmosphericThrust")).ToList();
                    HydroThrusters = thrusters.Where(t => t.BlockDefinition.SubtypeId.Contains("HydrogenThrust")).ToList();
                    IonThrusters = thrusters.Where(t => t.BlockDefinition.SubtypeId.Contains("SmallThrust") || t.BlockDefinition.SubtypeId.Contains("LargeThurst")).ToList();

                    GetThrustDirectionNewtonValues(AtmoThrusters, out AtmoThrustValues);
                    GetThrustDirectionNewtonValues(HydroThrusters, out HydroThrustValues);
                    GetThrustDirectionNewtonValues(IonThrusters, out IonThrustValues);

                    // Active ones
                    GridTerminalSystem.GetBlocksOfType(thrusters, t => t.CubeGrid == Me.CubeGrid && t.IsFunctional && t.IsWorking);
                    GetThrustDirectionNewtonValues(thrusters, out ActiveThrustValues);
                }
            }

            public static void Tick()
            {
                UpdateBlocks();

                var cockpits = new List<IMyCockpit>();
                GridTerminalSystem.GetBlocksOfType(cockpits, c => c.CubeGrid == Me.CubeGrid && c.IsUnderControl);
                var cockpit = cockpits.FirstOrDefault();
                InformationAvailable = cockpit != null;
                if (cockpit == null)
                {
                    return;
                }

                double OutElevation;
                IsUnderPlanetGravity = cockpit.TryGetPlanetElevation(MyPlanetElevation.Surface, out OutElevation);
                if (IsUnderPlanetGravity)
                {
                    Elevation = OutElevation;
                }
                else
                {
                    // Pretend elevation is sea-level
                    Elevation = 0.0;
                }

                ShipPhysicalMass = cockpit.CalculateShipMass().PhysicalMass;
                ShipTotalMass = cockpit.CalculateShipMass().TotalMass;
                Gravity = IsUnderPlanetGravity ? Math.Abs(cockpit.GetNaturalGravity().Length()) : 9.81;
                ShipSpeed = cockpit.GetShipSpeed();
            }

            public static string AtmoThrusterInformation
            {
                get
                {
                    var sb = new StringBuilder();

                    if (InformationAvailable && AtmoThrusters.Count > 0)
                    {
                        double ElevationFactor = -0.00009 * Elevation + 0.9;
                        double ForceWeightMultiplier = ElevationFactor / Gravity - ShipPhysicalMass;

                        if (Direction == ThrustDirection.All)
                        {
                            sb.AppendLine($"=== {AtmoThrusters.Count} Atmo Thrusters ===");
                            for (int i = (int)ThrustDirection.Up; i <= (int)ThrustDirection.Right; ++i)
                            {
                                double ForceWeight = AtmoThrustValues[i] * ForceWeightMultiplier;
                                string PositiveOrNegative = ForceWeight >= 0.0 ? "+" : "-";
                                ForceWeight = Math.Abs(ForceWeight) / 1000.0;
                                sb.AppendLine($"  {ThrustValues.GetNameFromIndex(i)}{PositiveOrNegative}{ForceWeight:N2}kN @ {-Gravity:N2}m/s2");
                            }
                        }
                        else
                        {
                            int DirectionIdx = (int)Direction;
                            double ForceWeight = AtmoThrustValues[DirectionIdx] * ForceWeightMultiplier;
                            string PositiveOrNegative = ForceWeight >= 0.0 ? "+" : "-";
                            ForceWeight = Math.Abs(ForceWeight);
                            sb.Append($"{ThrustValues.GetNameFromIndex(DirectionIdx)}{PositiveOrNegative}{ForceWeight:N2}kN @ {-Gravity:N2}m/s2");
                        }
                    }
                    
                    return sb.ToString().TrimEnd();
                }
            }

            public static string HydroThrusterInformation
            {
                get
                {
                    var sb = new StringBuilder();
                    if (InformationAvailable && HydroThrusters.Count > 0)
                    {
                        double ForceWeightMultiplier = 1.0 / (Gravity - ShipPhysicalMass);

                        if (Direction == ThrustDirection.All)
                        {
                            sb.AppendLine($"=== {HydroThrusters.Count} Hydro Thrusters ===");
                            for (int i = (int)ThrustDirection.Up; i <= (int)ThrustDirection.Right; ++i)
                            {
                                double ForceWeight = HydroThrustValues[i] * ForceWeightMultiplier;
                                string PositiveOrNegative = ForceWeight >= 0.0 ? "+" : "-";
                                ForceWeight = Math.Abs(ForceWeight);
                                sb.AppendLine($"  {ThrustValues.GetNameFromIndex(i)}{PositiveOrNegative}{ForceWeight:N2}kN @ {-Gravity:N2}m/s2");
                            }
                        }
                        else
                        {
                            int DirectionIdx = (int)Direction;
                            double ForceWeight = HydroThrustValues[DirectionIdx] * ForceWeightMultiplier;
                            string PositiveOrNegative = ForceWeight >= 0.0 ? "+" : "-";
                            ForceWeight = Math.Abs(ForceWeight);
                            sb.Append($"{ThrustValues.GetNameFromIndex(DirectionIdx)}{PositiveOrNegative}{ForceWeight:N2}kN @ {-Gravity:N2}m/s2");
                        }
                    }
                    
                    return sb.ToString().TrimEnd();
                }
            }

            public static string IonThrusterInformation
            {
                get
                {
                    var sb = new StringBuilder();

                    if (InformationAvailable && IonThrusters.Count > 0)
                    {
                        double ElevationFactor = 0.005 * Elevation + 30;
                        double ForceWeightMultiplier = ElevationFactor / Gravity - ShipPhysicalMass;

                        if (Direction == ThrustDirection.All)
                        {
                            sb.AppendLine($"=== {IonThrusters.Count} Ion Thrusters ===");
                            for (int i = (int)ThrustDirection.Up; i <= (int)ThrustDirection.Right; ++i)
                            {
                                double ForceWeight = IonThrustValues[i] * ForceWeightMultiplier;
                                string PositiveOrNegative = ForceWeight >= 0.0 ? "+" : "-";
                                ForceWeight = Math.Abs(ForceWeight);
                                sb.AppendLine($"  {ThrustValues.GetNameFromIndex(i)}{PositiveOrNegative}{ForceWeight:N2}kN @ {-Gravity:N2}m/s2");
                            }
                        }
                        else
                        {
                            int DirectionIdx = (int)Direction;
                            double ForceWeight = IonThrustValues[DirectionIdx] * ForceWeightMultiplier;
                            string PositiveOrNegative = ForceWeight >= 0.0 ? "+" : "-";
                            ForceWeight = Math.Abs(ForceWeight);
                            sb.Append($"{ThrustValues.GetNameFromIndex(DirectionIdx)}{PositiveOrNegative}{ForceWeight:N2}kN @ {-Gravity:N2}m/s2");
                        }
                    }
                    
                    return sb.ToString().TrimEnd();
                }
            }
        }
        #endregion Thrust

        #region Ship
        public static class ShipManager
        {
            private static IMyGridTerminalSystem GridTerminalSystem;
            private static IMyProgrammableBlock Me;

            private static bool CanCalculate = false;

            public static double? ShipDistanceToStop { get; private set; }

            public static void Init(Program prog)
            {
                GridTerminalSystem = prog.GridTerminalSystem;
                Me = prog.Me;
            }

            public static void Tick()
            {
                UpdateBlocks();

                if (CanCalculate)
                {
                    const double FudgeFactor = 10000.0; // Not sure why
                    double BrakeForce = ThrustManager.ActiveThrustValues.Backward > 200.0 ? 200.0 : ThrustManager.ActiveThrustValues.Backward;
                    ShipDistanceToStop = ThrustManager.ShipPhysicalMass * ThrustManager.ShipSpeed * ThrustManager.ShipSpeed / (BrakeForce * BrakeForce) / FudgeFactor;
                }
                else
                {
                    ShipDistanceToStop = null;
                }
            }

            private const int UpdateBlockFreq = 200;
            private static void UpdateBlocks()
            {
                if (ShouldUpdate(UpdateBlockFreq))
                {
                    List<IMyThrust> thrusters = new List<IMyThrust>();
                    GridTerminalSystem.GetBlocksOfType(thrusters, t => t.CubeGrid == Me.CubeGrid);

                    CanCalculate = ThrustManager.InformationAvailable && thrusters.Count > 0 && ThrustManager.ShipSpeed > 0.01;
                }
            }

            public static string DistanceToStop
            {
                get
                {
                    if (ShipDistanceToStop != null)
                    {
                        return $"{Conversions.Distance((double)ShipDistanceToStop)}";
                    }

                    return "";
                }
            }
        }
        #endregion

        #region TurretAmmo
        public static class TurretAmmoManager
        {
            private static IMyGridTerminalSystem GridTerminalSystem;
            private static IMyProgrammableBlock Me;

            public static Dictionary<string, int> AmmoTypeToCount = new Dictionary<string, int>();

            public static void Init(Program prog)
            {
                GridTerminalSystem = prog.GridTerminalSystem;
                Me = prog.Me;
            }

            private const int TurretAmmoUpdateFreq = 20000;
            public static void Tick()
            {
                if (ShouldUpdate(TurretAmmoUpdateFreq))
                {
                    var Cargos = new List<IMyCargoContainer>();
                    GridTerminalSystem.GetBlocksOfType(Cargos, c => c.CubeGrid == Me.CubeGrid);

                    AmmoTypeToCount.Clear();

                    var inventories = Cargos
                        .Select(c => c.GetInventory())
                        .ToList();
                    foreach (var inventory in inventories)
                    {
                        var items = new List<MyInventoryItem>();
                        inventory.GetItems(items);
                        foreach (var item in items)
                        {
                            if (item.Type.SubtypeId.ToLower().Contains("ammo"))
                            {
                                if (!AmmoTypeToCount.ContainsKey(item.Type.SubtypeId))
                                {
                                    AmmoTypeToCount.Add(item.Type.SubtypeId, (int)item.Amount);
                                }
                                else
                                {
                                    AmmoTypeToCount[item.Type.SubtypeId] += (int)item.Amount;
                                }
                            }
                        }
                    }
                }
            }

            public static string TurretAmmoInformation
            {
                get
                {
                    if (AmmoTypeToCount.Count > 0)
                    {
                        var sb = new StringBuilder();
                        foreach (var kvp in AmmoTypeToCount)
                        {
                            string AmmoType = kvp.Key;
                            if (AmmoType == "MediumCalibreAmmo")
                            {
                                AmmoType = "Turret Ammo";
                            }
                            sb.AppendLine($"{AmmoType}: {kvp.Value}");
                        }

                        return sb.ToString().TrimEnd();
                    }

                    return "";
                }
            }
        }
        #endregion

        

        #region Conversions
        public enum Unit
        {
            Single = 0,
            Kilo = 1,
            Mega = 2,
        }

        public static class Conversions
        {
            private static double GetFactor(Unit unit)
            {
                switch(unit)
                {
                    case Unit.Kilo:
                        return 1000.0;
                    case Unit.Mega:
                        return 1000000.0;
                }

                return 1.0;
            }

            public static string Distance(double Distance, Unit unit = Unit.Single)
            {
                Distance *= GetFactor(unit);

                if (Distance > 1000.0)
                {
                    return $"{Distance / 1000.0:N2} km";
                }
                else
                {
                    return $"{Distance:N2} m";
                }
            }

            public static string Power(double Power, Unit unit = Unit.Single)
            {
                Power *= GetFactor(unit);

                double AbsPower = Math.Abs(Power);
                if (AbsPower > 1000000.0)
                {
                    return $"{Power / 1000000.0:N2} MW";
                }
                else if (AbsPower > 1000.0)
                {
                    return $"{Power / 1000.0:N2} KW";
                }
                else
                {
                    return $"{Power:N2} W";
                }
            }

            public static string Weight(double Weight)
            {
                // Weight is always in kg
                return $"{Weight:N2} kg";
            }

            public static string Time(double Seconds, bool DecimalPointPrecision = false)
            {
                TimeSpan ts = TimeSpan.FromSeconds(Seconds);
                if (ts.Hours > 0)
                {
                    return $"{ts.Hours}h{ts.Minutes}m{ts.Seconds}s";
                }
                else if (ts.Minutes > 0)
                {
                    return $"{ts.Minutes}m{ts.Seconds}s";
                }
                else
                {
                    if (DecimalPointPrecision)
                    {
                        return $"{ts.TotalSeconds:N1}s";
                    }
                    else
                    {
                        return $"{ts.Seconds}s";
                    }
                }
            }
        }
        #endregion Conversions
    }
}
