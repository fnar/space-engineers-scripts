﻿using Sandbox.ModAPI.Ingame;
using SpaceEngineers.Game.ModAPI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VRage.Game.ModAPI.Ingame;

using IMySolarPanel = SpaceEngineers.Game.ModAPI.Ingame.IMySolarPanel;

namespace IngameScript
{
    partial class Program : MyGridProgram
    {
        public Program()
        {
            Runtime.UpdateFrequency = UpdateFrequency.Update1;
        }
        private const string DisplaySurfaceCubeName = "[Station] Status Panel";
        private const string DisplayCargoCubeName = "[Station] Cargo Panel";

        public void Main(string argument, UpdateType updateSource)
        {
            BatteryManagement.Tick(GridTerminalSystem, "clear", Me);
            GasManagement.Tick(GridTerminalSystem, "", Me);
            TurretAmmoManagement.Tick(GridTerminalSystem, "", Me);

            BaseCargoManagement.Tick(GridTerminalSystem, "", Me);
        }

        public static class BatteryManagement
        {
            #region Constants
            private const string BatteryManagementSurfaceCubeName = DisplaySurfaceCubeName;
            #endregion

            public static float CurrStoredPower = 0.0f;

            private static List<IMyTextSurface> BatteryManagementLogSurfaces = new List<IMyTextSurface>();
            private static List<IMyBatteryBlock> Batteries = new List<IMyBatteryBlock>();
            private static List<IMySolarPanel> SolarPanels = new List<IMySolarPanel>();

            private static bool IsInitialized = false;
            private static void Init(IMyGridTerminalSystem GridTerminalSystem, IMyProgrammableBlock Me)
            {
                if (!IsInitialized)
                {
                    if (!string.IsNullOrEmpty(BatteryManagementSurfaceCubeName))
                    {
                        List<IMyTerminalBlock> terminalBlocks = new List<IMyTerminalBlock>();
                        GridTerminalSystem.SearchBlocksOfName(BatteryManagementSurfaceCubeName, terminalBlocks, b => b.CubeGrid == Me.CubeGrid);
                        BatteryManagementLogSurfaces = terminalBlocks.Where(b => b is IMyTextSurface).Select(b => (IMyTextSurface)b).ToList();
                    }

                    GridTerminalSystem.GetBlocksOfType(Batteries, b => b.CubeGrid == Me.CubeGrid);
                    GridTerminalSystem.GetBlocksOfType(SolarPanels);
                    IsInitialized = true;
                }
            }

            private static string GetWattageString(float wattage)
            {
                const float OneMillion = 1000000.0f;
                const float OneThousand = 1000.0f;
                if (Math.Abs(wattage) > OneMillion)
                {
                    return $"{wattage / OneMillion:N2} MW";
                }
                else if (Math.Abs(wattage) > OneThousand)
                {
                    return $"{wattage / OneThousand:N2} kW";
                }
                else
                {
                    return $"{wattage:N2} w";
                }
            }

            public static void Tick(IMyGridTerminalSystem GridTerminalSystem, string argument, IMyProgrammableBlock Me)
            {
                Init(GridTerminalSystem, Me);

                if (argument == "clear")
                {
                    BatteryManagementLogSurfaces.ForEach(s => s.WriteText(""));
                }
                
                if (Batteries.Count > 0)
                {
                    CurrStoredPower = Batteries.Sum(b => b.CurrentStoredPower);
                    float MaxStoredPower = Batteries.Sum(b => b.MaxStoredPower);
                    float PowerPercentage = (CurrStoredPower / MaxStoredPower) * 100.0f;

                    bool AnyCharging = Batteries.Any(b => b.IsCharging);
                    float CurrentInput = Batteries.Sum(b => b.CurrentInput);
                    float CurrentOutput = Batteries.Sum(b => b.CurrentOutput);
                    float NetInput = CurrentInput - CurrentOutput;

                    StringBuilder sb = new StringBuilder();
                    sb.Append($"Battery: {PowerPercentage:N2}%");
                    if (NetInput > 0.0f)
                    {
                        float NetInputPerMinute = NetInput / 60.0f;
                        float RemainingStorage = MaxStoredPower - CurrStoredPower;
                        float MinutesToFullyCharged = RemainingStorage / NetInputPerMinute;

                        if (MinutesToFullyCharged > 0.1f)
                        {
                            sb.Append($" ({MinutesToFullyCharged:N1}m)");
                        }
                    }
                    sb.AppendLine();

                    const float WattFactor = 1000000000.0f;
                    CurrentInput *= WattFactor;
                    CurrentOutput *= WattFactor;
                    NetInput *= WattFactor;

                    sb.AppendLine($"  Input: {GetWattageString(CurrentInput)}");
                    sb.AppendLine($"  Output: {GetWattageString(CurrentOutput)}");
                    string Prefix = NetInput > 0.0f ? "+" : "";
                    sb.AppendLine($"  Net: {Prefix}{GetWattageString(NetInput)}");

                    float SolarNetOutput = SolarPanels.Sum(s => s.CurrentOutput);
                    sb.AppendLine($"      SolarOut: {GetWattageString(SolarNetOutput*WattFactor)} ({SolarPanels.Count} Panels)");

                    BatteryManagementLogSurfaces.ForEach(s => s.WriteText($"{sb}\n", true));
                }
            }
        }

        public static class GasManagement
        {
            #region Constants
            private const string GasManagementSurfaceCubeName = DisplaySurfaceCubeName;
            #endregion

            private static List<IMyTextSurface> GasManagementLogSurfaces = new List<IMyTextSurface>();
            private static List<IMyGasTank> OxygenTanks = new List<IMyGasTank>();
            private static List<IMyGasTank> HydrogenTanks = new List<IMyGasTank>();

            private static bool IsInitialized = false;
            private static void Init(IMyGridTerminalSystem GridTerminalSystem, IMyProgrammableBlock Me)
            {
                if (!IsInitialized)
                {
                    if (!string.IsNullOrEmpty(GasManagementSurfaceCubeName))
                    {
                        List<IMyTerminalBlock> terminalBlocks = new List<IMyTerminalBlock>();
                        GridTerminalSystem.SearchBlocksOfName(GasManagementSurfaceCubeName, terminalBlocks, b => b.CubeGrid == Me.CubeGrid);
                        GasManagementLogSurfaces = terminalBlocks.Where(b => b is IMyTextSurface).Select(b => (IMyTextSurface)b).ToList();
                    }

                    var tanks = new List<IMyGasTank>();
                    GridTerminalSystem.GetBlocksOfType(tanks, b => b.CubeGrid == Me.CubeGrid);

                    OxygenTanks = tanks.Where(t => t.BlockDefinition.TypeIdString.ToLower().Contains("oxygen")).ToList();
                    HydrogenTanks = tanks.Where(t => t.BlockDefinition.SubtypeId.ToLower().Contains("hydrogen")).ToList();

                    IsInitialized = true;
                }
            }

            public static void Tick(IMyGridTerminalSystem GridTerminalSystem, string argument, IMyProgrammableBlock Me)
            {
                Init(GridTerminalSystem, Me);

                if (argument == "clear")
                {
                    GasManagementLogSurfaces.ForEach(s => s.WriteText(""));
                }

                if (OxygenTanks.Count > 0 || HydrogenTanks.Count > 0)
                {
                    if (OxygenTanks.Count > 0)
                    {
                        GasManagementLogSurfaces.ForEach(s => s.WriteText($"Oxygen: {GetTankStoragePercentage(OxygenTanks) * 100.0:N2}% ({OxygenTanks.Count} tanks)\n", true));
                    }
                    if (HydrogenTanks.Count > 0)
                    {
                        GasManagementLogSurfaces.ForEach(s => s.WriteText($"Hydrogen: {GetTankStoragePercentage(HydrogenTanks) * 100.0:N2}% ({HydrogenTanks.Count} tanks)\n", true));
                    }
                }
            }

            private static double GetTankStoragePercentage(List<IMyGasTank> tanks)
            {
                double TankCurrentStorage = tanks.Sum(t => t.FilledRatio * t.Capacity);
                double TankMaxStorage = tanks.Sum(t => t.Capacity);
                return TankCurrentStorage / TankMaxStorage;
            }
        }

        public static class TurretAmmoManagement
        {
            #region Constants
            private const string TurretAmmoManagementSurfaceCubeName = DisplaySurfaceCubeName;
            #endregion

            private static List<IMyTextSurface> TurretAmmoManagementLogSurfaces = new List<IMyTextSurface>();
            private static List<IMyCargoContainer> Cargos = new List<IMyCargoContainer>();

            private static bool IsInitialized = false;
            private static void Init(IMyGridTerminalSystem GridTerminalSystem, IMyProgrammableBlock Me)
            {
                if (!IsInitialized)
                {
                    if (!string.IsNullOrEmpty(TurretAmmoManagementSurfaceCubeName))
                    {
                        List<IMyTerminalBlock> terminalBlocks = new List<IMyTerminalBlock>();
                        GridTerminalSystem.SearchBlocksOfName(TurretAmmoManagementSurfaceCubeName, terminalBlocks, b => b.CubeGrid == Me.CubeGrid);
                        TurretAmmoManagementLogSurfaces = terminalBlocks.Where(b => b is IMyTextSurface).Select(b => (IMyTextSurface)b).ToList();
                    }

                    GridTerminalSystem.GetBlocksOfType(Cargos, c => c.CubeGrid == Me.CubeGrid);

                    IsInitialized = true;
                }
            }

            public static void Tick(IMyGridTerminalSystem GridTerminalSystem, string argument, IMyProgrammableBlock Me)
            {
                Init(GridTerminalSystem, Me);

                if (argument == "clear")
                {
                    TurretAmmoManagementLogSurfaces.ForEach(s => s.WriteText(""));
                }

                var AmmoTypeToCount = new Dictionary<string, int>();

                var inventories = Cargos.Select(c => c.GetInventory()).ToList();
                foreach (var inventory in inventories)
                {
                    var items = new List<MyInventoryItem>();
                    inventory.GetItems(items);
                    foreach (var item in items)
                    {
                        if (item.Type.SubtypeId.ToLower().Contains("ammo"))
                        {
                            if (!AmmoTypeToCount.ContainsKey(item.Type.SubtypeId))
                            {
                                AmmoTypeToCount.Add(item.Type.SubtypeId, (int)item.Amount);
                            }
                            else
                            {
                                AmmoTypeToCount[item.Type.SubtypeId] += (int)item.Amount;
                            }
                        }
                    }
                }

                if (AmmoTypeToCount.Count > 0)
                {
                    foreach (var kvp in AmmoTypeToCount)
                    {
                        string AmmoType = kvp.Key;
                        if (AmmoType == "MediumCalibreAmmo")
                        {
                            AmmoType = "Turret Ammo";
                        }
                        TurretAmmoManagementLogSurfaces.ForEach(s => s.WriteText($"{AmmoType}: {kvp.Value}\n", true));
                    }
                }
            }
        }

        public static class BaseCargoManagement
        {
            private static List<IMyTextSurface> WritableSurfaces = new List<IMyTextSurface>();

            private static bool IsInitialized = false;
            private static void Init(IMyGridTerminalSystem GridTerminalSystem, string argument, IMyProgrammableBlock Me)
            {
                if (!IsInitialized)
                {
                    if (!string.IsNullOrEmpty(DisplayCargoCubeName))
                    {
                        List<IMyTerminalBlock> terminalBlocks = new List<IMyTerminalBlock>();
                        GridTerminalSystem.SearchBlocksOfName(DisplayCargoCubeName, terminalBlocks, b => b.CubeGrid == Me.CubeGrid);
                        WritableSurfaces = terminalBlocks.Where(b => b is IMyTextSurface).Select(b => (IMyTextSurface)b).ToList();
                    }

                    IsInitialized = true;
                }
            }

            public static void Tick(IMyGridTerminalSystem GridTerminalSystem, string argument, IMyProgrammableBlock Me)
            {
                Init(GridTerminalSystem, argument, Me);

                var OreToKg = new Dictionary<string, float>();
                var IngotToKg = new Dictionary<string, float>();

                var blocks = new List<IMyTerminalBlock>();
                GridTerminalSystem.GetBlocks(blocks);
                blocks = blocks.Where(c => c.CubeGrid == Me.CubeGrid && c.InventoryCount > 0).ToList();

                var inventories = new List<IMyInventory>();
                blocks.ForEach(c =>
                {
                    for (int i = 0; i < c.InventoryCount; ++i)
                    {
                        inventories.Add(c.GetInventory(i));
                    }
                });

                foreach (var inventory in inventories)
                {
                    var items = new List<MyInventoryItem>();
                    inventory.GetItems(items);

                    foreach (var item in items)
                    {
                        if (item.Type.ToString().Contains("Ore"))
                        {
                            var name = item.Type.SubtypeId;

                            if (!OreToKg.ContainsKey(name))
                            {
                                OreToKg.Add(name, (float)item.Amount);
                            }
                            else
                            {
                                OreToKg[name] += (float)item.Amount;
                            }
                        }

                        if (item.Type.ToString().Contains("Ingot"))
                        {
                            var name = item.Type.SubtypeId;

                            if (!IngotToKg.ContainsKey(name))
                            {
                                IngotToKg.Add(name, (float)item.Amount);
                            }
                            else
                            {
                                IngotToKg[name] += (float)item.Amount;
                            }
                        }
                    }
                }

                // This isn't strictly "safe" (dictionaries are unordered) but the likelihood of this not working is low
                OreToKg = OreToKg.OrderByDescending(kvp => kvp.Value).ToDictionary(kvp => kvp.Key, kvp => kvp.Value);
                IngotToKg = IngotToKg.OrderByDescending(kvp => kvp.Value).ToDictionary(kvp => kvp.Key, kvp => kvp.Value);

                var sb = new StringBuilder();
                foreach (var kvp in OreToKg)
                {
                    if (kvp.Key == "Ice" || kvp.Key == "Scrap")
                    {
                        sb.AppendLine($"{kvp.Key}: {kvp.Value:N2}kg");
                    }
                    else
                    {
                        sb.AppendLine($"{kvp.Key} Ore: {kvp.Value:N2}kg");
                    }

                }

                if (OreToKg.Count > 0)
                {
                    sb.AppendLine("===========");
                }

                foreach (var kvp in IngotToKg)
                {
                    if (kvp.Key == "Stone")
                    {
                        sb.AppendLine($"Gravel: {kvp.Value:N2}kg");
                    }
                    else if (kvp.Key == "Silicon")
                    {
                        sb.AppendLine($"Silicon Wafer: {kvp.Value:N2}kg");
                    }
                    else
                    {
                        sb.AppendLine($"{kvp.Key} Ingot: {kvp.Value:N2}kg");
                    }
                }

                WritableSurfaces.ForEach(ws => ws.WriteText(sb.ToString()));
            }
        }
    }
}
