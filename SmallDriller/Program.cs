﻿using Sandbox.ModAPI.Ingame;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IngameScript
{
    partial class Program : MyGridProgram
    {
        public Program()
        {
            Runtime.UpdateFrequency = UpdateFrequency.Update1;
        }

        public void Main(string argument, UpdateType updateSource)
        {
            var programmableBlocks = new List<IMyProgrammableBlock>();
            GridTerminalSystem.GetBlocksOfType(programmableBlocks, b => b.CubeGrid == Me.CubeGrid);

            var writableSurface = programmableBlocks[0].GetSurface(0);

            var cargos = new List<IMyCargoContainer>();
            GridTerminalSystem.GetBlocksOfType(cargos, s => s.CubeGrid == Me.CubeGrid);

            var drills = new List<IMyShipDrill>();
            GridTerminalSystem.GetBlocksOfType(drills, d => d.CubeGrid == Me.CubeGrid);

            float TopCurrentDrillStorage = drills
                .Where(d => d.CustomName.Contains("Top"))
                .Sum(d => (float)d.GetInventory().CurrentVolume);
            float TopMaxDrillStorage = drills
                .Where(d => d.CustomName.Contains("Top"))
                .Sum(d => (float)d.GetInventory().MaxVolume);
            float TopDrillStoragePercentage = (TopCurrentDrillStorage / TopMaxDrillStorage) * 100.0f;

            float BottomCurrentDrillStorage = drills
                .Where(d => d.CustomName.Contains("Bottom"))
                .Sum(d => (float)d.GetInventory().CurrentVolume);
            float BottomMaxDrillStorage = drills
                .Where(d => d.CustomName.Contains("Bottom"))
                .Sum(d => (float)d.GetInventory().MaxVolume);
            float BottomDrillStoragePercentage = (BottomCurrentDrillStorage / BottomMaxDrillStorage) * 100.0f;

            float TotalDrillStoragePercentage = ((TopCurrentDrillStorage + BottomCurrentDrillStorage) / (TopMaxDrillStorage + BottomMaxDrillStorage)) * 100.0f;

            float CargoCurrentStorage = cargos.Sum(c => (float)c.GetInventory().CurrentVolume);
            float CargoMaxStorage = cargos.Sum(c => (float)c.GetInventory().MaxVolume);
            float CargoStoragePercentage = ((CargoCurrentStorage) / (CargoMaxStorage)) * 100.0f;


            var batteries = new List<IMyBatteryBlock>();
            GridTerminalSystem.GetBlocksOfType(batteries, b => b.CubeGrid == Me.CubeGrid);

            float CurrStoredPower = batteries.Sum(b => b.CurrentStoredPower);
            float MaxStoredPower = batteries.Sum(b => b.MaxStoredPower);
            float PowerPercentage = (CurrStoredPower / MaxStoredPower) * 100.0f;

            bool AnyCharging = batteries.Any(b => b.IsCharging);
            float CurrentInput = batteries.Sum(b => b.CurrentInput);
            float CurrentOutput = batteries.Sum(b => b.CurrentOutput);
            float NetInput = CurrentInput - CurrentOutput;

            string MinutesToFullyChargedStr = string.Empty;
            if (NetInput > 0.0f)
            {
                float NetInputPerMinute = NetInput / 60.0f;
                float RemainingStorage = MaxStoredPower - CurrStoredPower;
                float MinutesToFullyCharged = RemainingStorage / NetInputPerMinute;
                MinutesToFullyChargedStr = MinutesToFullyCharged < 1.0f ? "Done" : MinutesToFullyCharged.ToString("N1") + "m";
            }
            else
            {
                MinutesToFullyChargedStr = "Inf";
            }

            var sb = new StringBuilder();
            sb.AppendLine($"Battery: {PowerPercentage:N2}%");
            if (NetInput > 0.0f)
            {
                sb.AppendLine($"Time to Charge: {MinutesToFullyChargedStr}");
            }

            sb.AppendLine();

            sb.AppendLine($"Cargo: {CargoStoragePercentage:N2}%");
            sb.AppendLine();
            sb.AppendLine($"Top Drills: {TopDrillStoragePercentage:N2}%");
            sb.AppendLine($"Bot Drills: {BottomDrillStoragePercentage:N2}%");

            writableSurface.WriteText(sb.ToString());
        }
    }
}
