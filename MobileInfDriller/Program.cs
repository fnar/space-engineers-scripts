﻿using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Weapons;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using VRage.GameServices;

namespace IngameScript
{
    partial class Program : MyGridProgram
    {
        /*
         **** Start operation:
         * Start Drill head drills
         * InfDrill extend operation (4th or 5th one should get us into the ground)
         * extend the drillhead pistons to make space
         * retract drillhead pistons
         * for each of 4 directions:
         *   angle drill hinge, extend drillhead pistons, retract drillhead pistons
         * set drillhead hinges to 0
         * extend drillhead pistons
         * Start drill wing drills
         * begin rotating drill head
         * slowly adjust wing hinges to 90
         * we should now have a small hole with a large hole below, and can start rotating & digging
         *
         **** Stop Operation:
         * rotate drillhead to 0
         * stop drills
         * raise wings
         * InfDrill retract operation
         * retract drillhead pistons
         */
        enum DrillState
        {
            Idle,
            RaisingBoom,
            LoweringBoom,
            DrillPilot,
            ExpandDrillHole,
            DrillWithRotation,
            StoppingDrills,
            PreparingForRetraction,
            Retracting,
        }
        private static DrillState activeDrillState = DrillState.Idle;

        enum InfRailState
        {
            Idle,
            Advance, // determines where we are, and starts advancing
            AdvanceFront,
            AdvanceRear,
            Retreat, // determines where we are, and starts retreating
            RetreatFront,
            RetreatRear,
        }
        private static InfRailState activeInfRailState = InfRailState.Idle;

        private static bool fullAutoDrill = false;

        private const string DefaultLogSurfaceCubeName = "[MIDriller] DrillManagerDisplay";

        private const string DrillHeadName      = "[MIDriller] Drill Head";
        private const string DrillWingLeftName  = "[MIDriller] Drill Wing Left";
        private const string DrillWingRightName = "[MIDriller] Drill Wing Right";
        // hinges to swing drill head in 4 directions (forward, backward, left, right)
        private const string DrillHeadHingeFBName = "[MIDriller] DrillHead Hinge FB";
        private const string DrillHeadHingeRLName = "[MIDriller] DrillHead Hinge RL";
        private const string DrillWingHingeLeftName  = "[MIDriller] Drill Wing Left Hinge";
        private const string DrillWingHingeRightName = "[MIDriller] Drill Wing Right Hinge";
        // pistons to move the drill head away from the infdrill track.
        private const string DrillHeadPistonGroupName = "[MIDriller] DrillHead Pistons";
        // Rotator to spin the wings around, and get more ground drilled out.
        private const string DrillHeadRotatorName = "[MIDriller] DrillHead Rotor";

        private static List<IMyShipDrill> DrillHeadDrills = new List<IMyShipDrill>();
        private static List<IMyShipDrill> DrillWingsDrills = new List<IMyShipDrill>();
        private static IMyMotorAdvancedStator DrillHeadRotor;
        private static IMyMotorAdvancedStator DrillHeadLeftRightHinge;
        private static IMyMotorAdvancedStator DrillHeadForwardBackHinge;
        private static IMyMotorAdvancedStator DrillWingLeftHinge;
        private static IMyMotorAdvancedStator DrillWingRightHinge;
        private static List<IMyPistonBase> DrillHeadPistons = new List<IMyPistonBase>();

        private const string InfRailProjectorName = "[MIDriller] InfRail Projector";
        private const string InfRailRearRotorName = "[MIDriller] InfRail Rotor Rear";
        private const string InfRailFrontRotorName = "[MIDriller] InfRail Rotor Front";
        private const string InfRailPistonName = "[MIDriller] InfRail Piston";
        private const string InfRailWelderName = "[MIDriller] InfRail Welder";
        private const string InfRailGrinderName = "[MIDriller] InfRail Grinder";

        private static IMyProjector InfRailProjector;
        private static IMyMotorAdvancedStator InfRailRearRotor;
        private static IMyMotorAdvancedStator InfRailFrontRotor;
        private static IMyPistonBase InfRailPiston;
        private static IMyShipWelder InfRailWelder;
        private static IMyShipGrinder InfRailGrinder;

        private const string BoomHingeName = "[MIDriller] Boom Hinge";
        private static IMyMotorAdvancedStator BoomHinge;
        private static float BoomArmRotationTarget;

        private static bool IsExtending { get; set; } = false;
        private static bool IsRetracting { get; set; } = false;
        private static int LastActivePistonGroupIndex = -1;

        private static List<string> DrillerLog = new List<string>();
        private static List<IMyTextSurface> DrillerLogSurfaces = new List<IMyTextSurface>();

        public Program()
        {
            Runtime.UpdateFrequency = UpdateFrequency.Update1;
        }

        private static bool Initialized = false;
        private void Init()
        {
            if (!Initialized)
            {
                InitLoggingBlocks();
                InitDrillHeadVariables();
                InitInfRailVariables();

                RepositionDrillHeadHingesToZero();

                // Setup our Drill parts.
                BoomHinge = (IMyMotorAdvancedStator)GridTerminalSystem.GetBlockWithName(BoomHingeName);
                BoomArmRotationTarget = BoomHinge.Angle;

                Log($"Found {DrillHeadDrills.Count} drills on DrillHead");
                Log($"Found {DrillWingsDrills.Count} drills on Drill Wings (total)");
                Initialized = true;
            }
        }

        public void InitLoggingBlocks()
        {
            List<IMyTerminalBlock> LogSurfaceBlocks = new List<IMyTerminalBlock>();
            GridTerminalSystem.GetBlocks(LogSurfaceBlocks);
            DrillerLogSurfaces = LogSurfaceBlocks
                .Where(ts => ts.CustomName == DefaultLogSurfaceCubeName)
                .Select(lsb => (IMyTextSurface)lsb)
                .ToList();

            // Clear log surfaces
            DrillerLogSurfaces.ForEach(dls => dls.WriteText(""));
        }

        public void InitDrillHeadVariables()
        {
            // Drill groups
            var BlockGroup = GridTerminalSystem.GetBlockGroupWithName(DrillHeadName);
            BlockGroup.GetBlocksOfType(DrillHeadDrills);
            BlockGroup = GridTerminalSystem.GetBlockGroupWithName(DrillWingLeftName);
            BlockGroup.GetBlocksOfType(DrillWingsDrills);
            List<IMyShipDrill> drillWingSet2 = new List<IMyShipDrill>();
            BlockGroup = GridTerminalSystem.GetBlockGroupWithName(DrillWingRightName);
            BlockGroup.GetBlocksOfType(drillWingSet2);
            DrillWingsDrills = DrillWingsDrills.Concat(drillWingSet2).ToList();

            // Rotors, hinges, pistons
            DrillHeadRotor = GridTerminalSystem.GetBlockWithName(DrillHeadRotatorName) as IMyMotorAdvancedStator;
            if (DrillHeadRotor == null)
                Log($"Unable to find DrillHead Rotator:\n  {DrillHeadRotatorName}");
            DrillHeadLeftRightHinge = GridTerminalSystem.GetBlockWithName(DrillHeadHingeRLName) as IMyMotorAdvancedStator;
            if (DrillHeadLeftRightHinge == null)
                Log($"Unable to find DrillHead L/R Hinge:\n  {DrillHeadHingeRLName}");
            DrillHeadForwardBackHinge = GridTerminalSystem.GetBlockWithName(DrillHeadHingeFBName) as IMyMotorAdvancedStator;
            if (DrillHeadForwardBackHinge == null)
                Log($"Unable to find DrillHead F/B Hinge:\n  {DrillHeadHingeFBName}");
            DrillWingLeftHinge = GridTerminalSystem.GetBlockWithName(DrillWingHingeLeftName) as IMyMotorAdvancedStator;
            if (DrillWingLeftHinge == null)
                Log($"Unable to find DrillWing Left Hinge:\n  {DrillWingHingeLeftName}");
            DrillWingRightHinge = GridTerminalSystem.GetBlockWithName(DrillWingHingeRightName) as IMyMotorAdvancedStator;
            if (DrillWingRightHinge == null)
                Log($"Unable to find DrillWing Right Hinge:\n  {DrillWingHingeRightName}");

            BlockGroup = GridTerminalSystem.GetBlockGroupWithName(DrillHeadPistonGroupName);
            BlockGroup.GetBlocksOfType(DrillHeadPistons);
        }

        public void InitInfRailVariables()
        {
            InfRailProjector = GridTerminalSystem.GetBlockWithName(InfRailProjectorName) as IMyProjector;
            if (InfRailProjector == null)
                Log($"Unable to find InfRail Projector:\n  {InfRailProjectorName}");
            InfRailRearRotor = GridTerminalSystem.GetBlockWithName(InfRailRearRotorName) as IMyMotorAdvancedStator;
            if (InfRailRearRotor == null)
                Log($"Unable to find InfRail Rear Rotor:\n  {InfRailRearRotorName}");
            InfRailFrontRotor = GridTerminalSystem.GetBlockWithName(InfRailFrontRotorName) as IMyMotorAdvancedStator;
            if (InfRailFrontRotor == null)
                Log($"Unable to find InfRail Front Rotor:\n  {InfRailFrontRotorName}");
            InfRailPiston = GridTerminalSystem.GetBlockWithName(InfRailPistonName) as IMyPistonBase;
            if (InfRailPiston == null)
                Log($"Unable to find InfRail Piston:\n  {InfRailPistonName}");
            InfRailWelder = GridTerminalSystem.GetBlockWithName(InfRailWelderName) as IMyShipWelder;
            if (InfRailWelder == null)
                Log($"Unable to find InfRail Welder:\n  {InfRailWelderName}");
            InfRailGrinder = GridTerminalSystem.GetBlockWithName(InfRailGrinderName) as IMyShipGrinder;
            if (InfRailGrinder == null)
                Log($"Unable to find InfRail Grinder:\n  {InfRailGrinderName}");
        }

        public void RepositionDrillHeadHingesToZero()
        {
            DrillHeadLeftRightHinge.RotateToAngle(MyRotationDirection.AUTO, 0, 1.0f);
            DrillHeadForwardBackHinge.RotateToAngle(MyRotationDirection.AUTO, 0, 1.0f);
        }

        // Main runs on every tick, so it's for our "Updates" too.
        public void Main(string argument, UpdateType updateSource)
        {
            argument = argument.ToLower();

            Init();
            bool IsArgFullStart = argument == "fullStart";
            bool IsArgStart = argument == "startWithCurrentBoom";
            bool IsArgStop = argument == "stop";

            if (IsArgStart)
            {
                Log($"Starting Drilling Process");
                fullAutoDrill = true;
                RaiseBoom();
            }

            if (IsArgStop)
            {
                Log($"Starting Drilling Process");
                CreateHole();
            }

            if (IsArgStop)
            {
                StopAndRetractDrills();
            }

            if (activeDrillState == DrillState.RaisingBoom)
            {
                if(IsBoomMoving() == false)
                {
                    Log("Boom is ready to drill.");
                    activeDrillState = DrillState.Idle;
                    if (fullAutoDrill)
                        CreateHole();
                }
            }

            if (activeDrillState == DrillState.LoweringBoom)
            {
                if (IsBoomMoving() == false)
                {
                    Log("Boom is locked and ready to drive.");
                    activeDrillState = DrillState.Idle;
                }
                if (IsBoomMoving() == false && fullAutoDrill)
                {

                }
            }

            if (activeDrillState == DrillState.DrillPilot)
            {
                if (PilotHoleIsDone())
                    ExpandPilotHole();
            }

            if (activeDrillState == DrillState.StoppingDrills)
            {
                if (AreDrillWingsReadyToCollapse())
                    CollapseDrillWings();
            }
            if (activeDrillState == DrillState.PreparingForRetraction)
            {
                if (AreDrillsReadyToRetract())
                    RetractDrills();
            }

            RefreshLogSurface();
        }

        private static void RaiseBoom()
        {

            Log($"Raising boom");
            activeDrillState = DrillState.RaisingBoom;
            RotateDrillBoom(BoomHinge, 0);
        }

        private static void RotateDrillBoom(IMyMotorAdvancedStator hinge, float toDegrees=0)
        {
            BoomArmRotationTarget = toDegrees;
            if (hinge == null)
                return;
            if (toDegrees > 90 || toDegrees < -90)
                return;

            hinge.RotorLock = false;
            hinge.RotateToAngle(MyRotationDirection.AUTO, toDegrees, 0.75f);
        }

        private static bool IsBoomMoving()
        {
            if (BoomArmRotationTarget == BoomHinge.Angle)
            {
                BoomHinge.RotorLock = true;
                return false;
            }
            return true;
        }

        private static void CreateHole()
        {
            activeDrillState = DrillState.DrillPilot;
            // Start Drill head drills
            DrillHeadDrills.ForEach((drill) => drill.Enabled = true);
            // InfDrill extend operation (4th or 5th one should get us into the ground)
            activeInfRailState = InfRailState.Advance;
            InfRailAdvance();
            // extend the drillhead pistons to make space
            // retract drillhead pistons
            // for each of 4 directions:
            //   angle drill hinge, extend drillhead pistons, retract drillhead pistons
            // set drillhead hinges to 0
            // extend drillhead pistons
            // Start drill wing drills
            // begin rotating drill head
            // slowly adjust wing hinges to 90
            // we should now have a small hole with a large hole below!
        }

        private static void InfRailAdvance()
        {
            if(InfRailRearRotor.IsAttached)
            {
                if(InfRailFrontRotor.IsAttached)
                {
                    if (InfRailPiston.CurrentPosition == InfRailPiston.HighestPosition)
                    {
                        // Move the rear rotor
                        InfRailProjector.Enabled = false;
                        InfRailDetachRearRotor();
                        InfRailPiston.Velocity = 2.0f;
                        InfRailPiston.Retract();
                    }
                    else
                    {
                        if(InfRailPiston.CurrentPosition == InfRailPiston.LowestPosition)
                        {
                            // Move the front rotor.
                            InfRailDetachFrontRotor();
                            InfRailPiston.Velocity = 0.1f;
                            InfRailPiston.Extend();
                        }
                        else
                        {
                            Log($"ERR: InfRail Piston Position weird:\n{InfRailPiston.LowestPosition} < {InfRailPiston.CurrentPosition} < {InfRailPiston.HighestPosition}");
                        }
                    }
                }
                else
                {
                    // waiting for the front rotor to be attachable.
                    if (InfRailPiston.CurrentPosition == InfRailPiston.HighestPosition)
                    {
                        // try an attach the rotor.
                        InfRailFrontRotor.Attach();
                    }
                }
            }
            else
            {
                // waiting for rear rotor to be attachable.
                if (InfRailPiston.CurrentPosition == InfRailPiston.HighestPosition)
                {
                    // try an attach the rotor.
                    InfRailRearRotor.Attach();
                    InfRailProjector.Enabled = true;
                }
            }
        }

        private static void InfRailDetachRearRotor()
        {
            if (InfRailFrontRotor.IsAttached)
                InfRailRearRotor.Detach();
            else
                Log("ERR: Req detach Rear Rotor while Front not attached");
        }
        private static void InfRailDetachFrontRotor()
        {
            if (InfRailRearRotor.IsAttached)
                InfRailFrontRotor.Detach();
            else
                Log("ERR: Req detach Front Rotor while Rear not attached");
        }


        private static void StopAndRetractDrills()
        {
            activeDrillState = DrillState.StoppingDrills;
            DrillHeadRotor.RotateToAngle(MyRotationDirection.CCW, 0, 1.0f);
            DrillHeadDrills.ForEach(drill => drill.Enabled = false);
        }

        private static bool AreDrillWingsReadyToCollapse()
        {
            if (DrillHeadRotor.Angle.IsEffectively(0))
                return true;
            return false;
        }

        private static void CollapseDrillWings()
        {
            activeDrillState = DrillState.PreparingForRetraction;
            DrillWingLeftHinge.RotateToAngle(MyRotationDirection.AUTO, 0, 1.0f);
            DrillWingRightHinge.RotateToAngle(MyRotationDirection.AUTO, 0, 1.0f);
        }

        private static bool AreDrillsReadyToRetract()
        {
            if (DrillHeadRotor.Angle.IsEffectively(0) &&
                DrillWingLeftHinge.Angle.IsEffectively(0) &&
                DrillWingRightHinge.Angle.IsEffectively(0) &&
                DrillHeadLeftRightHinge.Angle.IsEffectively(0) &&
                DrillHeadForwardBackHinge.Angle.IsEffectively(0))
            {
                return true;
            }
            return false;
        }

        private static void RetractDrills()
        {
            activeDrillState = DrillState.Retracting;
            DrillHeadPistons.ForEach((piston) => piston.Retract());
        }


        //
        // Logging
        //

        private static string PersistentLogString = "";
        private static void LogPersistent(string message)
        {
            PersistentLogString = message;
        }

        private const int LogLineLength = 15;
        private static void Log(string message)
        {
            DrillerLog.Add(message);

            if (DrillerLog.Count > LogLineLength)
            {
                DrillerLog.RemoveAt(0);
            }
        }

        private static void RefreshLogSurface()
        {
            string LogText = string.Join("\n", DrillerLog);

            if (!string.IsNullOrWhiteSpace(PersistentLogString))
            {
                LogText = $"{PersistentLogString}\n\n{LogText}";
            }

            DrillerLogSurfaces.ForEach(dls => dls.WriteText(LogText));
        }

    }
    public static class Extensions
    {
        public static bool IsEffectively(this float value, float target)
        {
            if (value > target - 0.00001 && value < target + 0.00001)
                return true;
            return false;
        }
    }
}
