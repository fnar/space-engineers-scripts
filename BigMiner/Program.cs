﻿using Sandbox.ModAPI.Ingame;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VRage.Game.ModAPI.Ingame;
using VRageMath;

namespace IngameScript
{
    partial class Program : MyGridProgram
    {
        public Program()
        {
            Runtime.UpdateFrequency = UpdateFrequency.Update1;
        }

        public void Save()
        {

        }

        public void Main(string argument, UpdateType updateSource)
        {
            var cockpit = GridTerminalSystem.GetBlockWithName("[BigMiner] Cockpit") as IMyCockpit;
            var surface = cockpit.GetSurface(0);

            BatteryManagement.Tick(GridTerminalSystem, "clear", Me, surface);
            DistanceToStopManager.Tick(GridTerminalSystem, "", Me, surface);
            CargoManagement.Tick(GridTerminalSystem, "", Me, surface);
            RaycastManager.Tick(GridTerminalSystem, "", Me, surface);
        }

        public static class BatteryManagement
        {
            private static List<IMyTextSurface> BatteryManagementLogSurfaces = new List<IMyTextSurface>();
            private static List<IMyBatteryBlock> Batteries = new List<IMyBatteryBlock>();

            private static List<IMyReactor> Reactors = new List<IMyReactor>();

            private static bool IsInitialized = false;
            private static void Init(IMyGridTerminalSystem GridTerminalSystem, IMyProgrammableBlock Me, IMyTextSurface surface)
            {
                if (!IsInitialized)
                {
                    BatteryManagementLogSurfaces.Add(surface);
                    GridTerminalSystem.GetBlocksOfType(Batteries, b => b.CubeGrid == Me.CubeGrid);
                    GridTerminalSystem.GetBlocksOfType(Reactors, b => b.CubeGrid == Me.CubeGrid);
                    IsInitialized = true;
                }
            }

            public static void Tick(IMyGridTerminalSystem GridTerminalSystem, string argument, IMyProgrammableBlock Me, IMyTextSurface surface)
            {
                Init(GridTerminalSystem, Me, surface);

                if (argument == "clear")
                {
                    BatteryManagementLogSurfaces.ForEach(s => s.WriteText(""));
                }

                if (Batteries.Count > 0)
                {
                    float CurrStoredPower = Batteries.Sum(b => b.CurrentStoredPower);
                    float MaxStoredPower = Batteries.Sum(b => b.MaxStoredPower);
                    float PowerPercentage = (CurrStoredPower / MaxStoredPower) * 100.0f;

                    bool AnyCharging = Batteries.Any(b => b.IsCharging);
                    float CurrentInput = Batteries.Sum(b => b.CurrentInput);
                    float CurrentOutput = Batteries.Sum(b => b.CurrentOutput);
                    float NetInput = CurrentInput - CurrentOutput;

                    StringBuilder sb = new StringBuilder();
                    sb.Append($"Battery: {PowerPercentage:N2}%");
                    if (NetInput > 0.0f)
                    {
                        float NetInputPerMinute = NetInput / 60.0f;
                        float RemainingStorage = MaxStoredPower - CurrStoredPower;
                        float MinutesToFullyCharged = RemainingStorage / NetInputPerMinute;

                        if (MinutesToFullyCharged > 0.1f)
                        {
                            sb.Append($" ({MinutesToFullyCharged:N1}m)");
                        }
                    }

                    BatteryManagementLogSurfaces.ForEach(s => s.WriteText($"{sb}\n", true));
                }

                if (Reactors.Count > 0)
                {

                }
            }
        }

        public static class CargoManagement
        {
            #region Constants
            private const string DrillGroupName = "[BigMiner] Drills";
            #endregion

            private static IMyTextSurface CargoManagementLogSurface = null;
            private static List<IMyShipDrill> Drills = new List<IMyShipDrill>();
            private static List<IMyCargoContainer> Cargos = new List<IMyCargoContainer>();

            private static bool IsInitialized = false;
            private static void Init(IMyGridTerminalSystem GridTerminalSystem, IMyProgrammableBlock Me, IMyTextSurface surface)
            {
                if (!IsInitialized)
                {
                    CargoManagementLogSurface = surface;
                    if (string.IsNullOrWhiteSpace(DrillGroupName)) throw new ArgumentNullException(nameof(DrillGroupName));

                    var drillGroup = GridTerminalSystem.GetBlockGroupWithName(DrillGroupName);
                    if (drillGroup == null) throw new ArgumentException($"Could not find group {DrillGroupName}");
                    drillGroup.GetBlocksOfType(Drills);
                    if (Drills.Count == 0) throw new ArgumentException($"There are no drills in {DrillGroupName}");

                    GridTerminalSystem.GetBlocksOfType(Cargos, c => c.CubeGrid == Me.CubeGrid);
                    if (Cargos.Count == 0) throw new ArgumentException($"Could not find any cargos");

                    IsInitialized = true;
                }
            }

            private static Dictionary<string, float> CalcOres()
            {
                var OreToKg = new Dictionary<string, float>();

                List<IMyInventory> inventories = Cargos.Select(c => c.GetInventory()).ToList();
                foreach (var inventory in inventories)
                {
                    var items = new List<MyInventoryItem>();
                    inventory.GetItems(items);
                    foreach (var item in items)
                    {
                        if (item.Type.ToString().Contains("Ore"))
                        {
                            var name = item.Type.SubtypeId;

                            if (!OreToKg.ContainsKey(name))
                            {
                                OreToKg.Add(name, (float)item.Amount);
                            }
                            else
                            {
                                OreToKg[name] += (float)item.Amount;
                            }
                        }
                    }
                }

                return OreToKg;
            }

            public static void Tick(IMyGridTerminalSystem GridTerminalSystem, string argument, IMyProgrammableBlock Me, IMyTextSurface surface)
            {
                Init(GridTerminalSystem, Me, surface);

                float CurrentVolume = Cargos.Sum(c => (float)c.GetInventory().CurrentVolume);
                float MaxVolume = Cargos.Sum(c => (float)c.GetInventory().MaxVolume);
                float CurrentVolumePercentage = CurrentVolume / MaxVolume * 100.0f;

                float CurrentMass = Cargos.Sum(c => (float)c.GetInventory().CurrentMass);

                float CurrentDrillVolume = Drills.Sum(d => (float)d.GetInventory().CurrentVolume);
                float MaxDrillVolume = Drills.Sum(d => (float)d.GetInventory().MaxVolume);
                float CurrentDrillVolumePercentage = CurrentDrillVolume / MaxDrillVolume * 100.0f;

                CargoManagementLogSurface.WriteText($"Cargo: {CurrentVolumePercentage:N2}%\n", true);
                CargoManagementLogSurface.WriteText($"Drill Cargo: {CurrentDrillVolumePercentage:N2}%\n", true);
                CargoManagementLogSurface.WriteText("\n", true);
                CargoManagementLogSurface.WriteText($"Mass: {CurrentMass:N2}kg\n", true);
                CargoManagementLogSurface.WriteText($"====================\n", true);

                var OreToKg = CalcOres()
                    .OrderByDescending(kvp => kvp.Value)
                    .ToDictionary(kvp => kvp.Key, kvp => kvp.Value);
                foreach (var kvp in OreToKg)
                {
                    if (kvp.Key == "Ice" || kvp.Key == "Scrap")
                    {
                        CargoManagementLogSurface.WriteText($"{kvp.Key}: {kvp.Value:N2}kg\n", true);
                    }
                    else
                    {
                        CargoManagementLogSurface.WriteText($"{kvp.Key} Ore: {kvp.Value:N2}kg\n", true);
                    }
                }
            }
        }

        public static class DistanceToStopManager
        {
            class ThrustValues
            {
                public double Up = 0.0;
                public double Down = 0.0;
                public double Forward = 0.0;
                public double Backward = 0.0;
                public double Left = 0.0;
                public double Right = 0.0;

                public double this[int i]
                {
                    get
                    {
                        switch (i)
                        {
                            case 0: return Up;
                            case 1: return Down;
                            case 2: return Forward;
                            case 3: return Backward;
                            case 4: return Left;
                            case 5: return Right;
                        }

                        return -1;
                    }
                }

                public static string GetNameFromIndex(int index)
                {
                    switch (index)
                    {
                        case 0: return "Up:       ";
                        case 1: return "Down:     ";
                        case 2: return "Forward:  ";
                        case 3: return "Backward: ";
                        case 4: return "Left:     ";
                        case 5: return "Right:    ";
                    }

                    return "ERROR-UNKNOWN";
                }
            }

            private static List<IMyTextSurface> LogSurfaces = new List<IMyTextSurface>();

            private static IMyCockpit Cockpit = null;
            private static List<IMyThrust> Thrusters = new List<IMyThrust>();
            private static ThrustValues AllThrustValues = new ThrustValues();

            private static bool Initialized = false;
            private static void Init(IMyGridTerminalSystem GridTerminalSystem, IMyProgrammableBlock Me, IMyTextSurface surface)
            {
                if (!Initialized)
                {
                    LogSurfaces.Add(surface);
                    Initialized = true;
                }
            }

            private static void GetThrustDirectionNewtonValues(
            List<IMyThrust> thrusters, out ThrustValues thrustValues)
            {
                thrustValues = new ThrustValues();
                thrustValues.Up = 0.0;
                thrustValues.Down = 0.0;
                thrustValues.Forward = 0.0;
                thrustValues.Backward = 0.0;
                thrustValues.Left = 0.0;
                thrustValues.Right = 0.0;

                // Sum up all the thruster values
                foreach (var thruster in thrusters)
                {
                    if (thruster.GridThrustDirection == Vector3I.Up)
                    {
                        thrustValues.Up += thruster.MaxThrust;
                    }
                    else if (thruster.GridThrustDirection == Vector3I.Down)
                    {
                        thrustValues.Down += thruster.MaxThrust;
                    }
                    else if (thruster.GridThrustDirection == Vector3I.Forward)
                    {
                        thrustValues.Forward = thruster.MaxThrust;
                    }
                    else if (thruster.GridThrustDirection == Vector3I.Backward)
                    {
                        thrustValues.Backward += thruster.MaxThrust;
                    }
                    else if (thruster.GridThrustDirection == Vector3I.Left)
                    {
                        thrustValues.Left += thruster.MaxThrust;
                    }
                    else if (thruster.GridThrustDirection == Vector3I.Right)
                    {
                        thrustValues.Right += thruster.MaxThrust;
                    }
                }
            }

            private static string GetDistanceString(double Meters)
            {
                if (Meters > 1000.0)
                {
                    return $"{Meters / 1000.0:N2}km";
                }
                else
                {
                    return $"{Meters:N2}m";
                }
            }

            public static void Tick(IMyGridTerminalSystem GridTerminalSystem, string argument, IMyProgrammableBlock Me, IMyTextSurface surface)
            {
                Init(GridTerminalSystem, Me, surface);

                if (argument == "clear")
                {
                    LogSurfaces.ForEach(l => l.WriteText(""));
                }

                var cockpits = new List<IMyCockpit>();
                GridTerminalSystem.GetBlocksOfType(cockpits, c => c.CubeGrid == Me.CubeGrid && c.IsUnderControl);
                Cockpit = cockpits.FirstOrDefault();

                if (Cockpit == null)
                {
                    LogSurfaces.ForEach(l => l.WriteText("Information only available when actively piloting", true));
                    return;
                }

                GridTerminalSystem.GetBlocksOfType(Thrusters, t => t.CubeGrid == Me.CubeGrid);
                GetThrustDirectionNewtonValues(Thrusters, out AllThrustValues);

                double ShipMass = Cockpit.CalculateShipMass().TotalMass;
                double CurrentShipSpeed = Cockpit.GetShipSpeed();
                double ShipSpeedSquared = CurrentShipSpeed * CurrentShipSpeed;
                double RawBrakeForce = AllThrustValues.Backward > 200.0 ? 200.0 : AllThrustValues.Backward;
                double TwoXBrakeForce = RawBrakeForce * 2;
                double DistanceToStop = ShipMass * ShipSpeedSquared / TwoXBrakeForce;

                // We're going to reduce our speed linearly apply our brakeforce
                // Speed = -BrakeForce + CurrentShipSpeed

                var sb = new StringBuilder();
                const double FudgeFactor = 10000.0; // No idea
                sb.AppendLine($"Stop: {GetDistanceString(DistanceToStop / FudgeFactor)}");

                LogSurfaces.ForEach(s => s.WriteText(sb.ToString(), true));
            }
        }

        public static class RaycastManager
        {
            private const string CameraBlockName = "[BigMiner] Camera";
            private static List<IMyTextSurface> LogSurfaces = new List<IMyTextSurface>();
            private static IMyCameraBlock Camera = null;

            private static Vector3D? PrevHitPosition = null;

            private static bool Initialized = false;
            private static void Init(IMyGridTerminalSystem GridTerminalSystem, string argument, IMyProgrammableBlock Me, IMyTextSurface surface)
            {
                if (!Initialized)
                {
                    LogSurfaces.Add(surface);
                    Camera = GridTerminalSystem.GetBlockWithName(CameraBlockName) as IMyCameraBlock;
                    Camera.EnableRaycast = true;

                    Initialized = true;
                }
            }

            private static string GetTimeUntilNextScanString(int TimeUntilNextScan)
            {
                if (TimeUntilNextScan > 0)
                {
                    double Seconds = (double)TimeUntilNextScan / 1000;
                    return $" ({Seconds:N1}s)";
                }

                return "";
            }

            private static string GetDistanceString(double Meters)
            {
                if (Meters > 1000.0)
                {
                    return $"{Meters / 1000.0:N2}km";
                }
                else
                {
                    return $"{Meters:N2}m";
                }
            }

            public static void Tick(IMyGridTerminalSystem GridTerminalSystem, string argument, IMyProgrammableBlock Me, IMyTextSurface surface)
            {
                Init(GridTerminalSystem, argument, Me, surface);

                const double ScanDistance = 20000.0;
                int TimeUntilNextScan = Camera.TimeUntilScan(ScanDistance);
                if (TimeUntilNextScan == 0)
                {
                    // Do the scan
                    var ScanResults = Camera.Raycast(ScanDistance);
                    PrevHitPosition = ScanResults.HitPosition;
                }
                
                if (PrevHitPosition != null)
                {
                    double distance = Vector3D.Distance(Camera.GetPosition(), (Vector3D)PrevHitPosition);
                    LogSurfaces.ForEach(s => s.WriteText($"Distance: {GetDistanceString(distance)}{GetTimeUntilNextScanString(TimeUntilNextScan)}\n", true));
                }
                else
                {
                    LogSurfaces.ForEach(s => s.WriteText($"Distance: [Nothing]{GetTimeUntilNextScanString(TimeUntilNextScan)}\n", true));
                }
            }
        }
    }
}
