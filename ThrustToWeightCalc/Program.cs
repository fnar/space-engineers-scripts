﻿using Sandbox.ModAPI.Ingame;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using VRageMath;


namespace IngameScript
{
    partial class Program : MyGridProgram
    {
        public Program()
        {
            Runtime.UpdateFrequency = UpdateFrequency.Once;
        }

        public void Main(string arguments, UpdateType updateSource)
        {
            // "PistonTest"
            var Piston = GridTerminalSystem.GetBlockWithName("PistonTest") as IMyPistonBase;
            Piston.Velocity = 0.1f;
            Piston.ApplyAction("Retract");
            
        }

        public static class ThurstToWeightManager
        {
            private const string ThrustToWeightDisplayBlockName = "Large Ship ThurstToWeight Display";

            class ThrustValues
            {
                public double Up = 0.0;
                public double Down = 0.0;
                public double Forward = 0.0;
                public double Backward = 0.0;
                public double Left = 0.0;
                public double Right = 0.0;

                public double this[int i]
                {
                    get
                    {
                        switch(i)
                        {
                            case 0: return Up;
                            case 1: return Down;
                            case 2: return Forward;
                            case 3: return Backward;
                            case 4: return Left;
                            case 5: return Right;
                        }

                        return -1;
                    }
                }

                public static string GetNameFromIndex(int index)
                {
                    switch (index)
                    {
                        case 0: return "Up";
                        case 1: return "Down";
                        case 2: return "Forward";
                        case 3: return "Backward";
                        case 4: return "Left";
                        case 5: return "Right";
                    }

                    return "UNKNOWN";
                }
            }

            private static List<IMyTextSurface> LogSurfaces = new List<IMyTextSurface>();

            private static IMyCockpit Cockpit = null;
            private static List<IMyThrust> AtmoThrusters = new List<IMyThrust>();
            private static List<IMyThrust> HydroThrusters = new List<IMyThrust>();
            private static List<IMyThrust> IonThrusters = new List<IMyThrust>();

            private static ThrustValues AtmoThrustValues = new ThrustValues();
            private static ThrustValues HydroThrustValues = new ThrustValues();
            private static ThrustValues IonThrustValues = new ThrustValues();

            private static bool Initialized = false;
            private static void Init(IMyGridTerminalSystem GridTerminalSystem, IMyProgrammableBlock Me)
            {
                if (!Initialized)
                {
                    var terminalBlocks = new List<IMyTerminalBlock>();
                    GridTerminalSystem.SearchBlocksOfName(ThrustToWeightDisplayBlockName, terminalBlocks, b => b.CubeGrid == Me.CubeGrid);
                    LogSurfaces = terminalBlocks.Where(b => b is IMyTextSurface).Select(b => (IMyTextSurface)b).ToList();

                    var cockpits = new List<IMyCockpit>();
                    GridTerminalSystem.GetBlocksOfType(cockpits, c => c.CubeGrid == Me.CubeGrid && c.IsMainCockpit);
                    Cockpit = cockpits.First();

                    var thrusters = new List<IMyThrust>();
                    GridTerminalSystem.GetBlocksOfType(thrusters, t => t.CubeGrid == Me.CubeGrid);

                    AtmoThrusters = thrusters.Where(t => t.Name == "Atmospheric Thruster").ToList();
                    HydroThrusters = thrusters.Where(t => t.Name == "Hydrogen Thruster").ToList();
                    IonThrusters = thrusters.Where(t => t.Name == "Ion Thruster").ToList();

                    GetThrustDirectionNewtonValues(AtmoThrusters, out AtmoThrustValues);
                    GetThrustDirectionNewtonValues(HydroThrusters, out HydroThrustValues);
                    GetThrustDirectionNewtonValues(IonThrusters, out IonThrustValues);

                    Initialized = true;
                }
            }

            private static void GetThrustDirectionNewtonValues(
            List<IMyThrust> thrusters, out ThrustValues thrustValues)
            {
                thrustValues = new ThrustValues();
                thrustValues.Up = 0.0;
                thrustValues.Down = 0.0;
                thrustValues.Forward = 0.0;
                thrustValues.Backward = 0.0;
                thrustValues.Left = 0.0;
                thrustValues.Right = 0.0;

                // Sum up all the thruster values
                foreach (var thruster in thrusters)
                {
                    if (thruster.GridThrustDirection == Vector3I.Up)
                    {
                        thrustValues.Up += thruster.MaxThrust;
                    }
                    else if (thruster.GridThrustDirection == Vector3I.Down)
                    {
                        thrustValues.Down += thruster.MaxThrust;
                    }
                    else if (thruster.GridThrustDirection == Vector3I.Forward)
                    {
                        thrustValues.Forward = thruster.MaxThrust;
                    }
                    else if (thruster.GridThrustDirection == Vector3I.Backward)
                    {
                        thrustValues.Backward += thruster.MaxThrust;
                    }
                    else if (thruster.GridThrustDirection == Vector3I.Left)
                    {
                        thrustValues.Left += thruster.MaxThrust;
                    }
                    else if (thruster.GridThrustDirection == Vector3I.Right)
                    {
                        thrustValues.Right += thruster.MaxThrust;
                    }
                }
            }

            public static void Tick(IMyGridTerminalSystem GridTerminalSystem, string argument, IMyProgrammableBlock Me)
            {
                Init(GridTerminalSystem, Me);

                if (argument == "clear")
                {
                    LogSurfaces.ForEach(l => l.WriteText(""));
                }

                LogSurfaces.ForEach(l => l.WriteText("Boo", true));

                double Elevation;
                bool IsUnderPlanetGravityEffects = Cockpit.TryGetPlanetElevation(MyPlanetElevation.Surface, out Elevation);
                double ShipMass = Cockpit.CalculateShipMass().PhysicalMass;
                double Gravity = IsUnderPlanetGravityEffects ? Math.Abs(Cockpit.GetNaturalGravity().Length()) : 9.81;

                var sb = new StringBuilder();
                if (AtmoThrusters.Count > 0)
                {
                    double ElevationFactor = -0.00009 * Elevation + 0.9;
                    sb.AppendLine("=== Atmospheric Thrusters ===");
                    for (int i = 0; i < 6; ++i)
                    {
                        double ForceWeight = ShipMass / (AtmoThrustValues[i] * ElevationFactor * Gravity);
                        string PositiveOrNegative = ForceWeight > 0.0 ? "+" : "-";
                        ForceWeight = Math.Abs(ForceWeight);

                        double ForceWeightKNewtons = ForceWeight / 1000.0;

                        if (IsUnderPlanetGravityEffects)
                        {
                            sb.AppendLine($"{ThrustValues.GetNameFromIndex(i)}: {PositiveOrNegative}{ForceWeightKNewtons:N2}kN @ {Gravity}m/s2");
                        }
                        else
                        {
                            sb.AppendLine($"Up: {PositiveOrNegative}{ForceWeightKNewtons:N2}kN @ {Gravity}m/s2 (Earth)");
                        }
                    }
                }

                if (HydroThrusters.Count > 0)
                {
                    sb.AppendLine("=== Hydrogen Thrusters ===");
                    for (int i = 0; i < 6; ++i)
                    {
                        double ForceWeight = ShipMass / (AtmoThrustValues[i]);
                        string PositiveOrNegative = ForceWeight > 0.0 ? "+" : "-";
                        ForceWeight = Math.Abs(ForceWeight);

                        double ForceWeightKNewtons = ForceWeight / 1000.0;
                        sb.AppendLine($"{ThrustValues.GetNameFromIndex(i)}: {PositiveOrNegative}{ForceWeightKNewtons:N2}kN\n");
                    }
                }

                if (IonThrusters.Count > 0)
                {
                    if (!IsUnderPlanetGravityEffects)
                    {
                        // Assuem sea level
                        Elevation = 0.0;
                    }

                    double ElevationFactor = 0.005 * Elevation + 30;
                    sb.AppendLine("=== Hydrogen Thrusters ===");
                    for (int i = 0; i < 6; ++i)
                    {
                        double ForceWeight = ShipMass / (AtmoThrustValues[i] * ElevationFactor);
                        string PositiveOrNegative = ForceWeight > 0.0 ? "+" : "-";
                        ForceWeight = Math.Abs(ForceWeight);

                        double ForceWeightKNewtons = ForceWeight / 1000.0;

                        if (IsUnderPlanetGravityEffects)
                        {
                            sb.AppendLine($"{ThrustValues.GetNameFromIndex(i)}: {PositiveOrNegative}{ForceWeightKNewtons:N2}kN @ Sea Level");
                        }
                        else
                        {
                            sb.AppendLine($"{ThrustValues.GetNameFromIndex(i)}: {PositiveOrNegative}{ForceWeightKNewtons:N2}kN @ {Elevation}m");
                        }
                    }
                }

                LogSurfaces.ForEach(s => s.WriteText(sb.ToString(), true));
            }
        }
    }
}
