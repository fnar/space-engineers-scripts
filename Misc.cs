﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static IngameScript.Program;
using VRageMath;

namespace IngameScript
{
    #region Debugger
    public static class Debugger
    {
        private const string DebuggerDisplayName = "DebuggerDisplay";

        private static int DebuggerCount = 0;
        private static IMyTextSurface DebuggerSurface;

        public static void Init(Program prog)
        {
            var LogSurfaceBlocks = new List<IMyTerminalBlock>();
            prog.GridTerminalSystem.GetBlocks(LogSurfaceBlocks);
            DebuggerSurface = LogSurfaceBlocks
                .Where(sb => sb.CustomName == DebuggerDisplayName)
                .Select(sb => (IMyTextSurface)sb)
                .FirstOrDefault();
        }

        private const int LogLineLength = 15;
        private static List<string> DebuggerLog = new List<string>();
        public static void Log(string message)
        {
            DebuggerLog.Add($"{DebuggerCount++}: {message}");
            if (DebuggerLog.Count > LogLineLength)
            {
                DebuggerLog.RemoveAt(0);
            }
        }

        private const int UpdateDebuggerFreq = 30;
        public static void Tick()
        {
            if (ShouldUpdate(UpdateDebuggerFreq))
            {
                DebuggerSurface?.WriteText(string.Join("\n", DebuggerLog));
            }
        }
    }
    #endregion

    public static class HoverManager
    {
        private static IMyGridTerminalSystem GridTerminalSystem;
        private static IMyProgrammableBlock Me;

        private static bool HasBeenUpdated = false;
        private static bool HoverManagerEnabled = false;

        private const string CameraSearchString = "Hover Camera";
        private static IMyCameraBlock Camera = null;

        private static List<IMyThrust> UpThrusters = new List<IMyThrust>();

        private static double HeightDelta = 0.25;
        private static double _CurrentHeight = 0.0;
        private static double _RequestedHeight = 3.0;

        public static void Init(Program prog)
        {
            GridTerminalSystem = prog.GridTerminalSystem;
            Me = prog.Me;
        }

        private const int UpdateBlockFreq = 3434;
        private static void UpdateBlocks()
        {
            if (!HasBeenUpdated || ShouldUpdate(UpdateBlockFreq))
            {
                List<IMyTerminalBlock> blocks = new List<IMyTerminalBlock>();
                GridTerminalSystem.SearchBlocksOfName(CameraSearchString, blocks, b => b is IMyCameraBlock);
                Camera = blocks.FirstOrDefault() as IMyCameraBlock;
                if (Camera == null)
                {
                    // Grab the first camera
                    GridTerminalSystem.GetBlocksOfType<IMyCameraBlock>(blocks);
                    Camera = blocks.FirstOrDefault() as IMyCameraBlock;
                }

                if (Camera != null)
                {
                    Camera.EnableRaycast = true;
                }

                GridTerminalSystem.GetBlocksOfType(UpThrusters, b => b.CubeGrid == Me.CubeGrid && ((IMyThrust)b).GridThrustDirection == Vector3I.Down);
                HasBeenUpdated = true;
            }
        }

        private const double MaximumHeight = 20.0;
        public static void Tick(string argument)
        {
            if (!HoverManagerEnabled)
                return;

            UpdateBlocks();

            if (Camera != null)
            {
                argument = argument.ToLower();
                bool Raise = argument.Contains("raise");
                bool Lower = argument.Contains("lower");

                if (Raise)
                {
                    _RequestedHeight += HeightDelta;
                    if (_RequestedHeight > MaximumHeight)
                    {
                        _RequestedHeight = MaximumHeight;
                    }
                }
                else if (Lower)
                {
                    _RequestedHeight -= HeightDelta;
                    if (_RequestedHeight < 0.0)
                    {
                        _RequestedHeight = 0.0;
                    }
                }

                // Normal tick
                if (Camera.CanScan(MaximumHeight))
                {
                    var RaycastResult = Camera.Raycast(MaximumHeight);
                    if (RaycastResult.HitPosition != null)
                    {
                        const float ThrustOverridePercentageDelta = 0.01f;
                        _CurrentHeight = Vector3D.Distance((Vector3D)RaycastResult.HitPosition, Camera.GetPosition());
                        UpThrusters.ForEach(t =>
                        {
                            if (_CurrentHeight > _RequestedHeight)
                            {
                                t.ThrustOverridePercentage -= ThrustOverridePercentageDelta;
                            }
                            else
                            {
                                t.ThrustOverridePercentage += ThrustOverridePercentageDelta;
                            }
                            if (t.ThrustOverridePercentage < 0.01f)
                            {
                                t.ThrustOverridePercentage = 0.01f;
                            }
                            else if (t.ThrustOverridePercentage > 100.0f)
                            {
                                t.ThrustOverridePercentage = 100.0f;
                            }
                        });
                        if (_CurrentHeight > _RequestedHeight)
                        {
                            UpThrusters.ForEach(t => t.ThrustOverridePercentage -= ThrustOverridePercentageDelta);
                        }
                        else
                        {
                            UpThrusters.ForEach(t => t.ThrustOverridePercentage += ThrustOverridePercentageDelta);
                        }
                    }
                }
            }
        }

        public static string RequestedHeight
        {
            get
            {
                return $"{Conversions.Distance(_RequestedHeight)}";
            }
        }

        public static string CurrentHeight
        {
            get
            {
                return $"{Conversions.Distance(_CurrentHeight)}";
            }
        }

        public static string CurrentOverride
        {
            get
            {
                if (UpThrusters.Count > 0)
                {
                    return $"{UpThrusters[0].ThrustOverridePercentage * 100.0:N2}%";
                }

                return "";
            }
        }
    }
}
