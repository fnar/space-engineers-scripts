﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Sandbox.ModAPI.Ingame;
using SpaceEngineers.Game.ModAPI.Ingame;
using VRage.Game.ModAPI.Ingame;
using VRageMath;

namespace IngameScript
{
    partial class Program : MyGridProgram
    {
        // Commands (arguments):
        // startdrilling
        // stopdrilling
        //
        // lowerlandinggear
        // raiselandinggear

        private const string WarningSoundBlockName = "Driller SoundBlock (Warning)";
        private const string WarningLightBlockName = "Driller Warning Light";


        public Program()
        {
            Runtime.UpdateFrequency = UpdateFrequency.Update1;
        }

        public void Main(string argument, UpdateType updateSource)
        {
            BatteryManagement.Tick(GridTerminalSystem, "clear", Me);
            WarningManager.Tick(GridTerminalSystem, argument, Me);
            DrillManagement.Tick(GridTerminalSystem, argument, Me);
            WarningLightManagement.Tick(GridTerminalSystem, argument, Me);
            CargoManagement.Tick(GridTerminalSystem, argument, Me);
            MagneticLanderManagement.Tick(GridTerminalSystem, argument, Me);
            AutoParkingBreak.Tick(GridTerminalSystem, argument, Me);
        }

        public static class BatteryManagement
        {
            #region Constants
            private const string BatteryManagementSurfaceCubeName = "Driller Cargo Display";
            #endregion

            private static List<IMyTextSurface> BatteryManagementLogSurfaces = new List<IMyTextSurface>();
            private static List<IMyBatteryBlock> Batteries = new List<IMyBatteryBlock>();

            private static bool IsInitialized = false;
            private static void Init(IMyGridTerminalSystem GridTerminalSystem, IMyProgrammableBlock Me)
            {
                if (!IsInitialized)
                {
                    if (!string.IsNullOrEmpty(BatteryManagementSurfaceCubeName))
                    {
                        List<IMyTerminalBlock> terminalBlocks = new List<IMyTerminalBlock>();
                        GridTerminalSystem.SearchBlocksOfName(BatteryManagementSurfaceCubeName, terminalBlocks, b => b.CubeGrid == Me.CubeGrid);
                        BatteryManagementLogSurfaces = terminalBlocks.Where(b => b is IMyTextSurface).Select(b => (IMyTextSurface)b).ToList();
                    }

                    GridTerminalSystem.GetBlocksOfType(Batteries, b => b.CubeGrid == Me.CubeGrid);
                    IsInitialized = true;
                }
            }

            public static void Tick(IMyGridTerminalSystem GridTerminalSystem, string argument, IMyProgrammableBlock Me)
            {
                Init(GridTerminalSystem, Me);

                if (argument == "clear")
                {
                    BatteryManagementLogSurfaces.ForEach(s => s.WriteText(""));
                }

                if (Batteries.Count > 0)
                {
                    float CurrStoredPower = Batteries.Sum(b => b.CurrentStoredPower);
                    float MaxStoredPower = Batteries.Sum(b => b.MaxStoredPower);
                    float PowerPercentage = (CurrStoredPower / MaxStoredPower) * 100.0f;

                    bool AnyCharging = Batteries.Any(b => b.IsCharging);
                    float CurrentInput = Batteries.Sum(b => b.CurrentInput);
                    float CurrentOutput = Batteries.Sum(b => b.CurrentOutput);
                    float NetInput = CurrentInput - CurrentOutput;

                    StringBuilder sb = new StringBuilder();
                    sb.Append($"Battery: {PowerPercentage:N2}%");
                    if (NetInput > 0.0f)
                    {
                        float NetInputPerMinute = NetInput / 60.0f;
                        float RemainingStorage = MaxStoredPower - CurrStoredPower;
                        float MinutesToFullyCharged = RemainingStorage / NetInputPerMinute;

                        if (MinutesToFullyCharged > 0.1f)
                        {
                            sb.Append($" ({MinutesToFullyCharged:N1}m)");
                        }
                    }

                    BatteryManagementLogSurfaces.ForEach(s => s.WriteText($"{sb}\n", true));
                }
            }
        }

        public static class DrillManagement
        {
            #region Constants
            // Any cube with a surface - Can be Programmable Block itself
            private const string DefaultLogSurfaceCubeName = "Driller DrillManagement Display";
            private const string CompleteSoundBlockName = "Driller SoundBlock (Complete)";

            private const string DefaultDrillGroupName = "Driller Drills";
            private const string DefaultPistonGroupPrefix = "Driller Piston Set";

            private const float DefaultExtendSpeed = -0.15f;
            private const float DefaultRetractSpeed = -0.5f;

            private const int DefaultTicksToWaitWhenFullyExtended = 500;
            #endregion

            private static float ExtendSpeed;
            private static float RetractSpeed;

            private static float TotalExtendTime;
            private static float TotalRetractTime;

            private static bool IsExtending { get; set; } = false;
            private static bool IsFullyExtendedAndWaiting { get; set; } = false;
            private static bool IsRetracting { get;set; } = false;

            private static int TicksToWaitWhenFullyExtended = -1;
            private static int LastActivePistonGroupIndex = -1;
            private static int FullyExtendedTickCounter = -1;

            private static List<string> DrillerLog = new List<string>();
            private static IMyTextSurface DrillerLogSurface = null;

            private static IMySoundBlock CompleteSoundBlock = null;
            private static List<IMyShipDrill> AllDrills = new List<IMyShipDrill>();
            private static List<List<IMyPistonBase>> PistonGroups = new List<List<IMyPistonBase>>();

            private static bool IsInitialized = false;
            private static void Init(IMyGridTerminalSystem GridTerminalSystem,
                                    string LogSurfaceCubeName = DefaultLogSurfaceCubeName,
                                    string DrillGroupName = DefaultDrillGroupName, 
                                    string PistonGroupPrefix = DefaultPistonGroupPrefix,
                                    float ExtendSpeed = DefaultExtendSpeed,
                                    float RetractSpeed = DefaultRetractSpeed,
                                    int TicksToWaitWhenFullyExtended = DefaultTicksToWaitWhenFullyExtended)
            {
                if (GridTerminalSystem == null) throw new ArgumentNullException(nameof(GridTerminalSystem));

                if (!IsInitialized)
                {
                    if (string.IsNullOrEmpty(LogSurfaceCubeName)) throw new ArgumentNullException(nameof(LogSurfaceCubeName));
                    if (string.IsNullOrEmpty(CompleteSoundBlockName)) throw new ArgumentNullException(nameof(CompleteSoundBlockName));

                    IMyTerminalBlock LogSurfaceBlock = GridTerminalSystem.GetBlockWithName(LogSurfaceCubeName);
                    if (LogSurfaceBlock == null) throw new ArgumentException($"Could not find {LogSurfaceCubeName} block");
                    if (LogSurfaceBlock is IMyTextSurface)
                    {
                        DrillerLogSurface = (IMyTextSurface)LogSurfaceBlock;
                    }
                    else
                    {
                        throw new ArgumentException($"{LogSurfaceCubeName} block is not a {nameof(IMyTextPanel)}");
                    }

                    CompleteSoundBlock = GridTerminalSystem.GetBlockWithName(CompleteSoundBlockName) as IMySoundBlock;
                    
                    if (string.IsNullOrEmpty(DrillGroupName)) throw new ArgumentNullException(nameof(DrillGroupName));
                    if (string.IsNullOrEmpty(PistonGroupPrefix)) throw new ArgumentNullException(nameof(PistonGroupPrefix));

                    if (ExtendSpeed < -10.0f || ExtendSpeed > 10.0f || ExtendSpeed == 0.0f) throw new ArgumentException("Extend speed should be in the range [-10.0, 10.0] and not 0");
                    DrillManagement.ExtendSpeed = ExtendSpeed;

                    if (RetractSpeed < -10.0f || RetractSpeed > 10.0f || RetractSpeed == 0.0f) throw new ArgumentException("Retract speed should be in the range [-10.0, 10.0] and not 0");
                    DrillManagement.RetractSpeed = RetractSpeed;

                    if (TicksToWaitWhenFullyExtended < 0 || TicksToWaitWhenFullyExtended > 5000) throw new ArgumentException("TicksToWaitWhenFullyExtended should be in the range [0, 5000]");
                    DrillManagement.TicksToWaitWhenFullyExtended = TicksToWaitWhenFullyExtended;

                    var drillGroup = GridTerminalSystem.GetBlockGroupWithName(DrillGroupName);
                    if (drillGroup == null) throw new ArgumentException("DrillGroupName provided was not found");
                    drillGroup.GetBlocksOfType(AllDrills);

                    bool keepSearching = true;
                    for (int pistonGroupIdx = 1; keepSearching; ++pistonGroupIdx)
                    {
                        string pistonGroupName = $"{PistonGroupPrefix} {pistonGroupIdx}";
                        var pistonBlockGroup = GridTerminalSystem.GetBlockGroupWithName(pistonGroupName);
                        if (pistonBlockGroup != null)
                        {
                            var groupPistons = new List<IMyPistonBase>();
                            pistonBlockGroup.GetBlocksOfType(groupPistons);
                            if (groupPistons.Count == 0) throw new ArgumentException($"Piston Group '{pistonGroupName}' has no pistons in it");
                            PistonGroups.Add(groupPistons);
                        }
                        else
                        {
                            keepSearching = false;
                        }
                    }

                    if (PistonGroups.Count == 0)
                    {
                        throw new ArgumentException($"There are no groups with the prefix {PistonGroupPrefix}");
                    }

                    IsInitialized = true;
                }
            }

            public static void Tick(IMyGridTerminalSystem GridTerminalSystem, string arguments, IMyProgrammableBlock _)
            {
                arguments = arguments.ToLower();

                Init(GridTerminalSystem);

                bool IsArgStart = arguments == "startdrilling";
                bool IsArgStop = arguments == "stopdrilling";

                if (IsArgStart)
                {
                    HandleArgStart();
                }

                if (IsArgStop)
                {
                    HandleArgStop();
                }

                if (IsExtending)
                {
                    HandleExtending();
                }
                else if (IsFullyExtendedAndWaiting)
                {
                    HandleFullyExtendedAndWaiting();
                }
                else if (IsRetracting)
                {
                    HandleRetracting();
                }
                else
                {
                    LogPersistent("Drill Process Inactive");
                }

                RefreshLogSurface();
            }

            private static void HandleArgStart()
            {
                List<int> AlreadyRunningPistonGroupIndices = Enumerable.Range(0, PistonGroups.Count)
                                                             .Where(i => PistonGroups[i][0].Status == PistonStatus.Extending || PistonGroups[i][0].Status == PistonStatus.Retracting || PistonGroups[i][0].Status == PistonStatus.Stopped)
                                                             .ToList();
                int NumActiveGroupIndices = AlreadyRunningPistonGroupIndices.Count;
                int CurrentPistonGroupIdx = NumActiveGroupIndices > 0 ? AlreadyRunningPistonGroupIndices.First() : 0;

                Log($"Detected {AllDrills.Count} drills & {PistonGroups.Count} piston groups");
                if (NumActiveGroupIndices >= 1)
                {
                    string pluralSuffix = NumActiveGroupIndices > 1 ? "s" : "";
                    Log($"Stopping {NumActiveGroupIndices} active piston group{pluralSuffix}");
                    PistonGroups.ForEach(pg => pg.ForEach(p => p.ApplyAction("OnOff_Off")));
                }

                Log($"Starting Drilling Process");
                AllDrills.ForEach(d => d.ApplyAction("OnOff_On"));
                PistonGroups.ForEach(pg => pg.ForEach(p =>
                {
                    p.Velocity = ExtendSpeed;
                    p.ApplyAction("OnOff_On");
                }));

                // Total distance / velocity
                TotalExtendTime = Math.Abs(PistonGroups.Sum(pg => pg[0].MaxLimit - pg[0].MinLimit) / ExtendSpeed);

                Log($"Extending Piston Group {CurrentPistonGroupIdx+1}");
                PistonGroups[CurrentPistonGroupIdx].ForEach(p => p.ApplyAction("Extend"));
                IsExtending = true;
                IsFullyExtendedAndWaiting = false;
                IsRetracting = false;
                LastActivePistonGroupIndex = CurrentPistonGroupIdx;
            }

            private static void HandleArgStop()
            {
                Log($"Stopping process");
                AllDrills.ForEach(d => d.ApplyAction("OnOff_Off"));
                PistonGroups.ForEach(pg => pg.ForEach(p =>
                {
                    p.Velocity = RetractSpeed;
                    p.ApplyAction("Retract");
                }));
                IsExtending = false;
                IsFullyExtendedAndWaiting = false;
                IsRetracting = true;
                CompleteSoundBlock.Play();
                LastActivePistonGroupIndex = -1;

                float MostExtendedPosition = PistonGroups.Max(pg => pg[0].CurrentPosition - pg[0].MinLimit);
                TotalRetractTime = Math.Abs(MostExtendedPosition / RetractSpeed);
            }

            private static void HandleExtending()
            {
                int CurrentPistonGroupIdx = LastActivePistonGroupIndex;

                float CompletedPercentage = (float)CurrentPistonGroupIdx / PistonGroups.Count;
                float CurrentPistonProgressPercentage = GetPistonProgressPercentage(PistonGroups[CurrentPistonGroupIdx][0]);
                CompletedPercentage += (CurrentPistonProgressPercentage * (1.0f / PistonGroups.Count));

                float SecondsRemaining = ((1.0f - CompletedPercentage) * TotalExtendTime);

                if (CompletedPercentage <= 0.99999f)
                {
                    if (PistonGroups[CurrentPistonGroupIdx][0].Status == PistonStatus.Extended)
                    {
                        // Move onto the next index and start it (if we can)
                        CurrentPistonGroupIdx++;
                        LastActivePistonGroupIndex++;

                        if (CurrentPistonGroupIdx < PistonGroups.Count)
                        {
                            Log($"Extending Piston Group {CurrentPistonGroupIdx+1}");
                            PistonGroups[CurrentPistonGroupIdx][0].ApplyAction("Extend");
                        }
                    }

                    // For some reason that isn't clear to me, you need to constantly extend
                    PistonGroups[CurrentPistonGroupIdx].ForEach(p => p.ApplyAction("Extend"));
                }
                else
                {
                    // Move to HandleFullyExtended
                    IsExtending = false;
                    IsFullyExtendedAndWaiting = true;
                    FullyExtendedTickCounter = TicksToWaitWhenFullyExtended;
                }

                LogPersistent($"Extend Progress: {CompletedPercentage * 100.0f:N2}% ({SecondsRemaining:N1}s)");
            }

            private static void HandleFullyExtendedAndWaiting()
            {
                LogPersistent($"Waiting: {TicksToWaitWhenFullyExtended - FullyExtendedTickCounter} / {TicksToWaitWhenFullyExtended}");

                if (FullyExtendedTickCounter >= 0)
                {
                    --FullyExtendedTickCounter;
                }
                else
                {
                    // Done, move to stopping
                    IsFullyExtendedAndWaiting = false;
                    HandleArgStop();
                }
            }

            private static void HandleRetracting()
            {
                float LargestPercentageExtended = PistonGroups.Max(pg => (pg[0].CurrentPosition - pg[0].MinLimit) / (pg[0].MaxLimit - pg[0].MinLimit));
                float PercentageToStart = 1.0f - LargestPercentageExtended;
                float SecondsRemaining = TotalRetractTime - (PercentageToStart * TotalRetractTime);

                if (PercentageToStart <= 0.99999f)
                {
                    LogPersistent($"Retract Progress: {PercentageToStart * 100.0f:N2}% ({SecondsRemaining:N1}s)");

                    // For some reason not immediately clear to me, you need to constantly retract
                    PistonGroups.ForEach(pg => pg.ForEach(p => p.ApplyAction("Retract")));
                }
                else
                {
                    IsRetracting = false;
                }
            }

            private static float GetPistonProgressPercentage(IMyPistonBase Piston)
            {
                // Since a piston can be from an arbitrary start distance to an arbitrary end distance,
                // we need to "normalize" the start to 0
                float End = Piston.MaxLimit - Piston.MinLimit;
                float CurrentPos = Piston.CurrentPosition - Piston.MinLimit;

                return CurrentPos / End;
            }

            private static string PersistentLogString = "";
            private static void LogPersistent(string message)
            {
                PersistentLogString = message;
            }

            private const int LogLineLength = 15;
            private static void Log(string message)
            {
                DrillerLog.Add(message);

                if (DrillerLog.Count > LogLineLength)
                {
                    DrillerLog.RemoveAt(0);
                }
            }

            private static void RefreshLogSurface()
            {
                string LogText = string.Join("\n", DrillerLog);

                if (!string.IsNullOrWhiteSpace(PersistentLogString))
                {
                    LogText = $"{PersistentLogString}\n\n{LogText}";
                }

                DrillerLogSurface.WriteText(LogText);
            }
        }

        public static class WarningLightManagement
        {
            #region Constants
            private const string LightGroupName = "Driller Center Spotlights";
            private const string WheelGroupName = "Driller Wheels";
            private const string ConnectorName = "Driller Connector";

            private static readonly Color ConnectedColor = Color.Orange;
            private static readonly Color GoodColor = Color.White;
            private static readonly Color BadColor = Color.Red;

            private const float HeightThreshold = 0.6f;
            #endregion

            private static List<IMyLightingBlock> Lights = new List<IMyLightingBlock>();
            private static List<IMyMotorSuspension> Wheels = new List<IMyMotorSuspension>();
            private static IMyShipConnector Connector = null;

            private static List<IMyShipController> ShipControllers = new List<IMyShipController>();

            private static bool IsInitialized = false;
            private static void Init(IMyGridTerminalSystem GridTerminalSystem, string argument, IMyProgrammableBlock Me)
            {
                if (!IsInitialized)
                {
                    if (string.IsNullOrWhiteSpace(LightGroupName)) throw new ArgumentNullException(nameof(LightGroupName));
                    if (String.IsNullOrWhiteSpace(WheelGroupName)) throw new ArgumentNullException(nameof(WheelGroupName));

                    var lightGroup = GridTerminalSystem.GetBlockGroupWithName(LightGroupName);
                    if (lightGroup == null) throw new ArgumentException($"Group {LightGroupName} could not be found");
                    lightGroup.GetBlocksOfType(Lights);
                    if (Lights.Count == 0) throw new ArgumentException($"There are no lights in {lightGroup}");

                    var wheelGroup = GridTerminalSystem.GetBlockGroupWithName(WheelGroupName);
                    if (wheelGroup == null) throw new ArgumentException($"Group {WheelGroupName} could not be found");
                    wheelGroup.GetBlocksOfType(Wheels);
                    if (Wheels.Count == 0) throw new ArgumentException($"There are no wheels in {WheelGroupName}");

                    Connector = GridTerminalSystem.GetBlockWithName(ConnectorName) as IMyShipConnector;
                    if (Connector == null) throw new ArgumentException($"Could not find connector with name {ConnectorName}");

                    GridTerminalSystem.GetBlocksOfType(ShipControllers, c => Me.CubeGrid == c.CubeGrid);

                    IsInitialized = true;
                }
            }

            public static void Tick(IMyGridTerminalSystem GridTerminalSystem, string argument, IMyProgrammableBlock Me)
            {
                Init(GridTerminalSystem, argument, Me);

                bool AtGoodHeight = Wheels.Any(w => w.Height > HeightThreshold);

                if (Connector.IsConnected)
                {
                    Lights.ForEach(l => l.Color = ConnectedColor);
                }
                else if (AtGoodHeight)
                {
                    Lights.ForEach(l => l.Color = GoodColor);
                }
                else
                {
                    Lights.ForEach(l => l.Color = BadColor);
                }

                bool Parked = ShipControllers.Any(c => c.HandBrake);
                bool Moving = Me.CubeGrid.Speed > 0.25;
                if (!Parked && Moving && !AtGoodHeight)
                {
                    WarningManager.TurnOn(GridTerminalSystem, WarningLightBlockName, WarningSoundBlockName);
                }
                else
                {
                    WarningManager.TurnOff(WarningLightBlockName, WarningSoundBlockName);
                }
            }
        }

        public static class CargoManagement
        {
            #region Constants
            private const string CargoManagementSurfaceCubeName = "Driller Cargo Display";
            private const string DrillGroupName = "Driller Drills";
            #endregion

            private static IMyTextSurface CargoManagementLogSurface = null;
            private static List<IMyShipDrill> Drills = new List<IMyShipDrill>();
            private static List<IMyCargoContainer> Cargos = new List<IMyCargoContainer>();

            private static bool IsInitialized = false;
            private static void Init(IMyGridTerminalSystem GridTerminalSystem, IMyProgrammableBlock Me)
            {
                if (!IsInitialized)
                {
                    IMyTerminalBlock LogSurfaceBlock = GridTerminalSystem.GetBlockWithName(CargoManagementSurfaceCubeName);
                    if (LogSurfaceBlock == null) throw new ArgumentException($"Could not find {CargoManagementSurfaceCubeName} block");
                    if (LogSurfaceBlock is IMyTextSurface)
                    {
                        CargoManagementLogSurface = (IMyTextSurface)LogSurfaceBlock;
                    }
                    else
                    {
                        throw new ArgumentException($"{CargoManagementSurfaceCubeName} block is not a {nameof(IMyTextPanel)}");
                    }

                    if (string.IsNullOrWhiteSpace(CargoManagementSurfaceCubeName)) throw new ArgumentNullException(nameof(CargoManagementSurfaceCubeName));
                    if (string.IsNullOrWhiteSpace(DrillGroupName)) throw new ArgumentNullException (nameof(DrillGroupName));

                    var drillGroup = GridTerminalSystem.GetBlockGroupWithName(DrillGroupName);
                    if (drillGroup == null) throw new ArgumentException($"Could not find group {DrillGroupName}");
                    drillGroup.GetBlocksOfType(Drills);
                    if (Drills.Count == 0) throw new ArgumentException($"There are no drills in {DrillGroupName}");

                    GridTerminalSystem.GetBlocksOfType(Cargos, c => c.CubeGrid == Me.CubeGrid);
                    if (Cargos.Count == 0) throw new ArgumentException($"Could not find any cargos");

                    IsInitialized = true;
                }
            }

            private static Dictionary<string, float> CalcOres()
            {
                var OreToKg = new Dictionary<string, float>();

                List<IMyInventory> inventories = Cargos.Select(c => c.GetInventory()).ToList();
                foreach (var inventory in inventories)
                {
                    var items = new List<MyInventoryItem>();
                    inventory.GetItems(items);
                    foreach (var item in items)
                    {
                        if (item.Type.ToString().Contains("Ore"))
                        {
                            var name = item.Type.SubtypeId;

                            if (!OreToKg.ContainsKey(name))
                            {
                                OreToKg.Add(name, (float)item.Amount);
                            }
                            else
                            {
                                OreToKg[name] += (float)item.Amount;
                            }
                        }
                    }
                }

                return OreToKg;
            }

            public static void Tick(IMyGridTerminalSystem GridTerminalSystem, string argument, IMyProgrammableBlock Me)
            {
                Init(GridTerminalSystem, Me);

                float CurrentVolume = Cargos.Sum(c => (float)c.GetInventory().CurrentVolume);
                float MaxVolume = Cargos.Sum(c => (float)c.GetInventory().MaxVolume);
                float CurrentVolumePercentage = CurrentVolume / MaxVolume * 100.0f;

                float CurrentMass = Cargos.Sum(c => (float)c.GetInventory().CurrentMass);

                float CurrentDrillVolume = Drills.Sum(d => (float)d.GetInventory().CurrentVolume);
                float MaxDrillVolume = Drills.Sum(d => (float)d.GetInventory().MaxVolume);
                float CurrentDrillVolumePercentage = CurrentDrillVolume / MaxDrillVolume * 100.0f;

                CargoManagementLogSurface.WriteText($"Cargo: {CurrentVolumePercentage:N2}%\n", true);
                CargoManagementLogSurface.WriteText($"Drill Cargo: {CurrentDrillVolumePercentage:N2}%\n", true);
                CargoManagementLogSurface.WriteText("\n", true);
                CargoManagementLogSurface.WriteText($"Mass: {CurrentMass:N2}kg\n", true);
                CargoManagementLogSurface.WriteText($"====================\n", true);

                var OreToKg = CalcOres()
                    .OrderByDescending(kvp => kvp.Value)
                    .ToDictionary(kvp => kvp.Key, kvp => kvp.Value);
                foreach (var kvp in OreToKg)
                {
                    if (kvp.Key == "Ice" || kvp.Key == "Scrap")
                    {
                        CargoManagementLogSurface.WriteText($"{kvp.Key}: {kvp.Value:N2}kg\n", true);
                    }
                    else
                    {
                        CargoManagementLogSurface.WriteText($"{kvp.Key} Ore: {kvp.Value:N2}kg\n", true);
                    }
                }
            }
        }

        public static class MagneticLanderManagement
        {
            #region Constants
            private const string LanderPistonGroupName = "Driller Landing Pistons";
            private const string LanderMagneticLockGroupName = "Driller Landing Plates";
            #endregion

            private static bool IsLowering = false;
            private static bool IsRaising = false;

            private static List<IMyPistonBase> Pistons = new List<IMyPistonBase>();
            private static List<IMyLandingGear> MagneticLocks = new List<IMyLandingGear>();

            private static bool IsInitialized = false;
            private static void Init(IMyGridTerminalSystem GridTerminalSystem)
            {
                if (!IsInitialized)
                {
                    if (string.IsNullOrWhiteSpace(LanderPistonGroupName)) throw new ArgumentNullException(nameof(LanderPistonGroupName));
                    if (string.IsNullOrWhiteSpace(LanderMagneticLockGroupName)) throw new ArgumentNullException(nameof(LanderMagneticLockGroupName));

                    var pistonGroup = GridTerminalSystem.GetBlockGroupWithName(LanderPistonGroupName);
                    if (pistonGroup == null) throw new ArgumentException($"Could not find group {LanderPistonGroupName}");
                    pistonGroup.GetBlocksOfType(Pistons);
                    if (Pistons.Count == 0) throw new ArgumentException($"There were not any pistons in group {LanderPistonGroupName}");

                    var magneticLockGroup = GridTerminalSystem.GetBlockGroupWithName(LanderMagneticLockGroupName);
                    if (magneticLockGroup == null) throw new ArgumentException($"Could not find group {LanderMagneticLockGroupName}");
                    magneticLockGroup.GetBlocksOfType(MagneticLocks);
                    if (MagneticLocks.Count == 0) throw new ArgumentException($"There were not any landing gear in group {LanderMagneticLockGroupName}");

                    IsInitialized = true;
                }
            }

            public static void Tick(IMyGridTerminalSystem GridTerminalSystem, string argument, IMyProgrammableBlock Me)
            {
                argument = argument.ToLower();

                Init(GridTerminalSystem);

                bool AnyLocksActive = MagneticLocks.Any(ml => ml.IsLocked);

                if (argument == "lowerlandinggear")
                {
                    IsLowering = true;
                    IsRaising = false;
                    Pistons.ForEach(p =>
                    {
                        p.ApplyAction("OnOff_On");
                        p.ApplyAction("Extend");
                    });

                    MagneticLocks.ForEach(ml => ml.AutoLock = true);
                }

                if (argument == "raiselandinggear")
                {
                    IsLowering = false;
                    IsRaising = true;

                    MagneticLocks.ForEach(ml =>
                    {
                        ml.AutoLock = false;
                        ml.Unlock();
                    });

                    Pistons.ForEach(p =>
                    {
                        p.ApplyAction("OnOff_On");
                        p.ApplyAction("Retract");
                    });
                }

                if (IsLowering)
                {
                    // Check if any has locked
                    if (AnyLocksActive)
                    {
                        IsLowering = false;

                        Pistons.ForEach(p => p.ApplyAction("OnOff_Off"));
                    }
                }

                if (IsRaising && Pistons.All(p => p.Status == PistonStatus.Retracted))
                {
                    IsRaising = false;
                }
            }
        }

        public static class WarningManager
        {
            private const float Speed = 0.25f;

            struct LightBlockState
            {
                public float Intensity;
                public float Radius;
                public Color Color;
                public bool On;
            }

            private static Dictionary<string, LightBlockState> InitialState = new Dictionary<string, LightBlockState>();
            private static Dictionary<string, IMyLightingBlock> NameToLight = new Dictionary<string, IMyLightingBlock>();
            private static List<IMyLightingBlock> ActiveLights = new List<IMyLightingBlock>();
            private static Dictionary<IMyLightingBlock, bool> ActiveLightToGoingUp = new Dictionary<IMyLightingBlock, bool>();

            private static Dictionary<string, IMySoundBlock> NameToSound = new Dictionary<string, IMySoundBlock>();
            private static List<IMySoundBlock> ActiveSounds = new List<IMySoundBlock>();

            public static void TurnOn(IMyGridTerminalSystem GridTerminalSystem, string LightName = null, string SoundName = null)
            {
                if (!string.IsNullOrEmpty(LightName))
                {
                    if (!NameToLight.ContainsKey(LightName))
                    {
                        var block = GridTerminalSystem.GetBlockWithName(LightName);
                        if (block is IMyLightingBlock)
                        {
                            var lightingBlock = (IMyLightingBlock)block;
                            NameToLight.Add(LightName, lightingBlock);
                            InitialState.Add(LightName, new LightBlockState { Intensity = lightingBlock.Intensity, Radius = lightingBlock.Radius, Color = lightingBlock.Color, On = lightingBlock.Enabled });
                        }
                    }

                    if (NameToLight.ContainsKey(LightName) && !ActiveLights.Contains(NameToLight[LightName]))
                    {
                        NameToLight[LightName].Color = Color.Red;
                        NameToLight[LightName].Intensity = 4.0f;
                        NameToLight[LightName].ApplyAction("OnOff_On");
                        ActiveLightToGoingUp[NameToLight[LightName]] = true;
                        ActiveLights.Add(NameToLight[LightName]);
                    }
                }

                if (!string.IsNullOrEmpty(SoundName))
                {
                    if (!NameToSound.ContainsKey(SoundName))
                    {
                        var block = GridTerminalSystem.GetBlockWithName(SoundName);
                        if (block is IMySoundBlock)
                        {
                            var soundBlock = (IMySoundBlock)block;
                            if (soundBlock.IsSoundSelected)
                            {
                                NameToSound.Add(SoundName, soundBlock);
                            }
                        }
                    }

                    if (NameToSound.ContainsKey(SoundName) && !ActiveSounds.Contains(NameToSound[SoundName]))
                    {
                        NameToSound[SoundName].LoopPeriod = 60.0f * 30.0f;
                        NameToSound[SoundName].Play();
                        ActiveSounds.Add(NameToSound[SoundName]);
                    }
                }
            }

            public static void TurnOff(string LightName = null, string SoundName = null)
            {
                if (!string.IsNullOrEmpty(LightName))
                {
                    IMyLightingBlock light;
                    if (NameToLight.TryGetValue(LightName, out light))
                    {
                        light.Intensity = InitialState[LightName].Intensity;
                        light.Radius = InitialState[LightName].Radius;
                        light.Color = InitialState[LightName].Color;
                        if (!InitialState[LightName].On)
                        {
                            light.ApplyAction("OnOff_Off");
                        }

                        ActiveLights.Remove(light);
                        ActiveLightToGoingUp.Remove(light);
                    }
                }

                if (!string.IsNullOrEmpty(SoundName))
                {
                    IMySoundBlock sound;
                    if (NameToSound.TryGetValue(SoundName, out sound))
                    {
                        sound.Stop();
                        ActiveSounds.Remove(sound);
                    }
                }
            }

            public static void Tick(IMyGridTerminalSystem GridTerminalSystem, string argument, IMyProgrammableBlock Me)
            {
                argument = argument.ToLower();

                var arguments = argument.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                if (arguments.Length >= 2)
                {
                    string command = arguments[0];
                    bool TurnOn = command == "warningon";
                    bool TurnOff = command == "warningoff";
                    if (TurnOn || TurnOff)
                    {
                        string light = "";
                        string sound = "";
                        bool parsingLight = false;
                        bool parsingSound = false;
                        foreach (var arg in arguments.Skip(1))
                        {
                            if (arg.StartsWith("light:"))
                            {
                                if (parsingSound)
                                {
                                    parsingSound = false;
                                }

                                parsingLight = true;
                                string strippedArg = arg.Substring(6);
                                light += $"{strippedArg} ";
                            }

                            if (arg.StartsWith("sound:"))
                            {
                                if (parsingLight)
                                {
                                    parsingLight = false;
                                }

                                parsingSound = true;
                                string strippedArg = arg.Substring(6);
                                sound += $"{strippedArg} ";
                            }
                        }

                        light = light.Trim();
                        sound = sound.Trim();

                        if (TurnOn)
                        {
                            WarningManager.TurnOn(GridTerminalSystem, light, sound);
                        }
                        else
                        {
                            WarningManager.TurnOff(light, sound);
                        }
                    }
                }

                foreach (var ActiveLight in ActiveLights)
                {
                    bool GoingUp = ActiveLightToGoingUp[ActiveLight];
                    float NewIntensity = GoingUp ? ActiveLight.Intensity + Speed : ActiveLight.Intensity - Speed;
                    if (NewIntensity > 10.0f) NewIntensity = 10.0f;
                    if (NewIntensity < 0.5f) NewIntensity = 0.5f;

                    float IntensityPercentage = (NewIntensity - 0.5f) / 9.5f;
                    ActiveLight.Intensity = NewIntensity;

                    if (ActiveLight.Intensity <= 0.51f)
                    {
                        ActiveLightToGoingUp[ActiveLight] = true;
                    }

                    if (ActiveLight.Intensity >= 9.9001f)
                    {
                        ActiveLightToGoingUp[ActiveLight] = false;
                    }
                }
            }
        }

        public static class AutoParkingBreak
        {
            public static void Tick(IMyGridTerminalSystem GridTerminalSystem, string argument, IMyProgrammableBlock Me)
            {
                var Controllers = new List<IMyShipController>();
                GridTerminalSystem.GetBlocksOfType(Controllers, c => c.CubeGrid == Me.CubeGrid);

                bool UnderControl = Controllers.Any(c => c.IsUnderControl);
                bool Parked = Controllers.Any(c => c.HandBrake);
                if (!UnderControl && !Parked)
                {
                    Controllers.ForEach(c => c.HandBrake = true);
                }
            }
        }
    }
}
