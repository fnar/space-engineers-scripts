﻿using Sandbox.ModAPI.Ingame;
using System;
using System.Collections.Generic;
using System.Linq;

namespace IngameScript
{
    partial class Program : MyGridProgram
    {
        private const string DefaultLogSurfaceCubeName = "[Rigger] DrillManagerDisplay";

        private const string DrillGroupName = "[Rigger] Drills";
        private const string PistonGroupPrefix = "[Rigger] DPistonSet";

        private static List<IMyShipDrill> AllDrills = new List<IMyShipDrill>();
        private static List<List<IMyPistonBase>> PistonGroups = new List<List<IMyPistonBase>>();

        private const float DrillingUpPistonGroupVelocity = -0.15f;
        private const float DrillingDownPistonGroupVelocity = 0.05f;

        private const float RetractingUpPistonGroupVelocity = 0.15f;
        private const float RetractingDownPistonGroupVelocity = -0.05f;

        private static float TotalExtendTime;
        private static float TotalRetractTime;

        private static bool IsExtending { get; set; } = false;
        private static bool IsRetracting { get; set; } = false;
        private static int LastActivePistonGroupIndex = -1;

        private static List<string> DrillerLog = new List<string>();
        private static List<IMyTextSurface> DrillerLogSurfaces = new List<IMyTextSurface>();

        public Program()
        {
            Runtime.UpdateFrequency = UpdateFrequency.Update1;
        }

        private static bool Initialized = false;
        private void Init()
        {
            if (!Initialized)
            {
                var DrillGroup = GridTerminalSystem.GetBlockGroupWithName(DrillGroupName);
                DrillGroup.GetBlocksOfType(AllDrills);

                List<IMyTerminalBlock> LogSurfaceBlocks = new List<IMyTerminalBlock>();
                GridTerminalSystem.GetBlocks(LogSurfaceBlocks);
                DrillerLogSurfaces = LogSurfaceBlocks
                    .Where(ts => ts.CustomName == DefaultLogSurfaceCubeName)
                    .Select(lsb => (IMyTextSurface)lsb)
                    .ToList();

                // Clear log surfaces
                DrillerLogSurfaces.ForEach(dls => dls.WriteText(""));

                bool keepSearching = true;
                for (int pistonGroupIdx = 1; keepSearching; ++pistonGroupIdx)
                {
                    string oddSuffix = pistonGroupIdx % 2 != 0 ? " Up" : "";
                    string pistonGroupName = $"{PistonGroupPrefix} {pistonGroupIdx}{oddSuffix}";
                    var pistonBlockGroup = GridTerminalSystem.GetBlockGroupWithName(pistonGroupName);
                    if (pistonBlockGroup != null)
                    {
                        var groupPistons = new List<IMyPistonBase>();
                        pistonBlockGroup.GetBlocksOfType(groupPistons);
                        PistonGroups.Add(groupPistons);
                    }
                    else
                    {
                        keepSearching = false;
                    }

                    Initialized = true;
                }
            }
        }

        // "[Rigger] DPistonSet N Up"   // Ups/odds - Retracted  (0.15 m/s Velocity)
        // "[Rigger] DPistonSet N"      // Evens - Extended (0.05 m/s Velocity)
        public void Main(string argument, UpdateType updateSource)
        {
            argument = argument.ToLower();

            Init();
            bool IsArgStart = argument == "startdrilling";
            bool IsArgStop = argument == "stopdrilling";

            if (IsArgStart)
            {
                HandleArgStart();
            }

            if (IsArgStop)
            {
                HandleArgStop();
            }

            if (IsExtending)
            {
                HandleExtending();
            }
            else if (IsRetracting)
            {
                HandleRetracting();
            }
            else
            {
                LogPersistent("Drill Process Inactive");
            }

            RefreshLogSurface();
        }

        private static void HandleArgStart()
        {
            //List<int> AlreadyRunningPistonGroupIndices = Enumerable.Range(0, PistonGroups.Count)
            //                                             .Where(i => PistonGroups[i][0].Status == PistonStatus.Extending || PistonGroups[i][0].Status == PistonStatus.Retracting || PistonGroups[i][0].Status == PistonStatus.Stopped)
            //                                             .ToList();
            //int NumActiveGroupIndices = AlreadyRunningPistonGroupIndices.Count;
            //
            //Log($"Detected {AllDrills.Count} drills & {PistonGroups.Count} piston groups");
            //if (NumActiveGroupIndices >= 1)
            //{
            //    string pluralSuffix = NumActiveGroupIndices > 1 ? "s" : "";
            //    Log($"Stopping {NumActiveGroupIndices} active piston group{pluralSuffix}");
            //    PistonGroups.ForEach(pg => pg.ForEach(p => p.ApplyAction("OnOff_Off")));
            //}

            Log($"Starting Drilling Process");
            AllDrills.ForEach(d => d.ApplyAction("OnOff_On"));
            for (int PistonGroupIdx = 0; PistonGroupIdx < PistonGroups.Count; ++PistonGroupIdx)
            {
                // Since we're indexing from 0, evens are the Up pistons
                bool IsUp = PistonGroupIdx % 2 == 0;
                float PistonVelocity = IsUp ? DrillingUpPistonGroupVelocity : DrillingDownPistonGroupVelocity;
                PistonGroups[PistonGroupIdx].ForEach(p =>
                {
                    p.Velocity = PistonVelocity;
                    p.ApplyAction("OnOff_Off");
                });
            }

            // Total distance / velocity
            // Separate into Odd and Even Groups
            var Odds = PistonGroups.Where((pg, index) => index % 2 == 0);
            var Evens = PistonGroups.Where((pg, index) => index % 2 == 1);

            var OddTotalExtendTime = Math.Abs(Odds.Sum(pg => pg[0].MaxLimit - pg[0].MinLimit) / DrillingUpPistonGroupVelocity);
            var EvenTotalExtendTime = Math.Abs(Evens.Sum(pg => pg[0].MaxLimit - pg[0].MinLimit) / DrillingDownPistonGroupVelocity);
            
            TotalExtendTime = OddTotalExtendTime + EvenTotalExtendTime;

            int CurrentPistonGroupIdx = 0;
            bool IsUpPistonGroup = CurrentPistonGroupIdx % 2 == 0;
            string LogActionString = IsUpPistonGroup ? "Retracting" : "Extending";
            Log($"{LogActionString} Piston Group {CurrentPistonGroupIdx + 1} - V: {PistonGroups[CurrentPistonGroupIdx][0].Velocity:N2}");
            string ActionString = IsUpPistonGroup ? "Retract" : "Extend";
            PistonGroups[CurrentPistonGroupIdx].ForEach(p =>
            {
                p.ApplyAction("OnOff_On");
                p.ApplyAction(ActionString);
            });
            IsExtending = true;
            IsRetracting = false;
            LastActivePistonGroupIndex = 0;
        }
         
        private static void HandleArgStop()
        {
            Log($"Stopping process");
            AllDrills.ForEach(d => d.ApplyAction("OnOff_Off"));
            for (int PistonGroupIdx = 0; PistonGroupIdx < PistonGroups.Count; ++PistonGroupIdx)
            {
                bool IsUp = PistonGroupIdx % 2 == 0;
                float PistonVelocity = IsUp ? RetractingUpPistonGroupVelocity : RetractingDownPistonGroupVelocity;
                Log($"Setting {PistonGroupIdx}'s Velocity to {PistonVelocity:N2} and turning on.");
                PistonGroups[PistonGroupIdx].ForEach(p =>
                {
                    p.Velocity = PistonVelocity;
                    p.ApplyAction("OnOff_On");
                });
            }

            IsExtending = false;
            IsRetracting = true;
            LastActivePistonGroupIndex = -1;
        }

        private static void HandleExtending()
        {
            int CurrentPistonGroupIdx = LastActivePistonGroupIndex;

            float CompletedPercentage = (float)CurrentPistonGroupIdx / PistonGroups.Count;
            float CurrentPistonProgressPercentage = GetPistonProgressPercentage(PistonGroups[CurrentPistonGroupIdx][0]);
            CompletedPercentage += (CurrentPistonProgressPercentage * (1.0f / PistonGroups.Count));

            float SecondsRemaining = ((1.0f - CompletedPercentage) * TotalExtendTime);

            string ActionStr = CurrentPistonGroupIdx % 2 == 0 ? "Retract" : "Extend";
            if (CompletedPercentage <= 0.99999f)
            {
                PistonStatus DestinationStatus = CurrentPistonGroupIdx % 2 == 0 ? PistonStatus.Retracted : PistonStatus.Extended;
                if (PistonGroups[CurrentPistonGroupIdx][0].Status == DestinationStatus)
                {
                    // Move onto the next index and start it (if we can)
                    CurrentPistonGroupIdx++;
                    LastActivePistonGroupIndex++;

                    if (CurrentPistonGroupIdx < PistonGroups.Count)
                    {
                        string LogActionStr = CurrentPistonGroupIdx % 2 == 0 ? "Retracting" : "Extending";
                        Log($"{LogActionStr} Piston Group {CurrentPistonGroupIdx + 1} - V: {PistonGroups[CurrentPistonGroupIdx][0].Velocity:N2}");
                        
                        PistonGroups[CurrentPistonGroupIdx].ForEach(p =>
                        {
                            p.ApplyAction("OnOff_On");
                            p.ApplyAction(ActionStr);
                        });
                    }
                }

                // For some reason that isn't clear to me, you need to constantly extend
                PistonGroups[CurrentPistonGroupIdx].ForEach(p =>
                {
                    p.ApplyAction(ActionStr);
                });
            }
            else
            {
                // Move to HandleFullyExtended
                Log("IsExtending = false");
                IsExtending = false;
                // Trigger Stop
                //HandleArgStop();
            }

            LogPersistent($"Extend Progress: {CompletedPercentage * 100.0f:N2}% ({SecondsRemaining:N1}s)");
        }

        private static void HandleRetracting()
        {
            //float LargestPercentageExtended = PistonGroups.Max(pg => (pg[0].CurrentPosition - pg[0].MinLimit) / (pg[0].MaxLimit - pg[0].MinLimit));
            //float PercentageToStart = 1.0f - LargestPercentageExtended;
            //float SecondsRemaining = TotalRetractTime - (PercentageToStart * TotalRetractTime);
            //
            //if (PercentageToStart <= 0.99999f)
            //{
            //    LogPersistent($"Retract Progress: {PercentageToStart * 100.0f:N2}% ({SecondsRemaining:N1}s)");
            //
            //    // For some reason not immediately clear to me, you need to constantly retract
            //    for (int PistonGroupIdx = 0; PistonGroupIdx < PistonGroups.Count; ++PistonGroupIdx)
            //    {
            //        string ActionStr = PistonGroupIdx % 2 == 0 ? "Extend" : "Retract";
            //        PistonGroups[PistonGroupIdx].ForEach(p => p.ApplyAction(ActionStr));
            //    }
            //}
            //else
            //{
            //    IsRetracting = false;
            //}
        }

        private static float GetPistonProgressPercentage(IMyPistonBase Piston)
        {
            // Go from Start -> End
            float Percentage = Piston.CurrentPosition / Piston.MaxLimit;
            Percentage = Piston.Status == PistonStatus.Extending ? Percentage : (1 - Percentage);
            return Percentage;
        }

        private static string PersistentLogString = "";
        private static void LogPersistent(string message)
        {
            PersistentLogString = message;
        }

        private const int LogLineLength = 15;
        private static void Log(string message)
        {
            DrillerLog.Add(message);

            if (DrillerLog.Count > LogLineLength)
            {
                DrillerLog.RemoveAt(0);
            }
        }

        private static void RefreshLogSurface()
        {
            string LogText = string.Join("\n", DrillerLog);

            if (!string.IsNullOrWhiteSpace(PersistentLogString))
            {
                LogText = $"{PersistentLogString}\n\n{LogText}";
            }

            DrillerLogSurfaces.ForEach(dls => dls.WriteText(LogText));
        }
    }
}
