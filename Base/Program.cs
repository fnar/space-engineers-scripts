﻿using System.Collections.Generic;
using System.Linq;
using System.Text;

using Sandbox.ModAPI.Ingame;
using VRage.Game.ModAPI.Ingame;

namespace IngameScript
{
    partial class Program : MyGridProgram
    {
        public Program()
        {
            Runtime.UpdateFrequency = UpdateFrequency.Update1;
        }

        private const string WritableSurfaceBlockName = "";

        public void Main(string argument, UpdateType updateSource)
        {
            var surface = GridTerminalSystem.GetBlockWithName(WritableSurfaceBlockName) as IMyTextSurface;
            BaseCargoManagement.Tick(GridTerminalSystem, argument, Me, surface);
        }

        public static class BaseCargoManagement
        {
            private static IMyTextSurface WritableSurface = null;

            private static bool IsInitialized = false;
            private static void Init(IMyTextSurface surface)
            {
                if (!IsInitialized)
                {
                    WritableSurface = surface;
                    IsInitialized = true;
                }
            }

            public static void Tick(IMyGridTerminalSystem GridTerminalSystem, string argument, IMyProgrammableBlock Me, IMyTextSurface WritableSurface)
            {
                Init(WritableSurface);

                var OreToKg = new Dictionary<string, float>();
                var IngotToKg = new Dictionary<string, float>();

                var blocks = new List<IMyTerminalBlock>();
                GridTerminalSystem.GetBlocks(blocks);
                blocks = blocks.Where(c => c.CubeGrid == Me.CubeGrid && c.InventoryCount > 0).ToList();

                var inventories = new List<IMyInventory>();
                blocks.ForEach(c =>
                {
                    for (int i = 0; i < c.InventoryCount; ++i)
                    {
                        inventories.Add(c.GetInventory(i));
                    }
                });

                foreach (var inventory in inventories)
                {
                    var items = new List<MyInventoryItem>();
                    inventory.GetItems(items);

                    foreach (var item in items)
                    {
                        if (item.Type.ToString().Contains("Ore"))
                        {
                            var name = item.Type.SubtypeId;

                            if (!OreToKg.ContainsKey(name))
                            {
                                OreToKg.Add(name, (float)item.Amount);
                            }
                            else
                            {
                                OreToKg[name] += (float)item.Amount;
                            }
                        }

                        if (item.Type.ToString().Contains("Ingot"))
                        {
                            var name = item.Type.SubtypeId;

                            if (!IngotToKg.ContainsKey(name))
                            {
                                IngotToKg.Add(name, (float)item.Amount);
                            }
                            else
                            {
                                IngotToKg[name] += (float)item.Amount;
                            }
                        }
                    }
                }

                // This isn't strictly "safe" (dictionaries are unordered) but the likelihood of this not working is low
                OreToKg = OreToKg.OrderByDescending(kvp => kvp.Value).ToDictionary(kvp => kvp.Key, kvp => kvp.Value);
                IngotToKg = IngotToKg.OrderByDescending(kvp => kvp.Value).ToDictionary(kvp => kvp.Key, kvp => kvp.Value);

                var sb = new StringBuilder();
                foreach (var kvp in OreToKg)
                {
                    if (kvp.Key == "Ice" || kvp.Key == "Scrap")
                    {
                        sb.AppendLine($"{kvp.Key}: {kvp.Value:N2}kg");
                    }
                    else
                    {
                        sb.AppendLine($"{kvp.Key} Ore: {kvp.Value:N2}kg");
                    }

                }

                if (OreToKg.Count > 0)
                {
                    sb.AppendLine("===========");
                }

                foreach (var kvp in IngotToKg)
                {
                    if (kvp.Key == "Stone")
                    {
                        sb.AppendLine($"Gravel: {kvp.Value:N2}kg");
                    }
                    else if (kvp.Key == "Silicon")
                    {
                        sb.AppendLine($"Silicon Wafer: {kvp.Value:N2}kg");
                    }
                    else
                    {
                        sb.AppendLine($"{kvp.Key} Ingot: {kvp.Value:N2}kg");
                    }
                }

                WritableSurface.WriteText(sb.ToString());
            }
        }
    }
}
